package mgrassl.at.ShoppingList;

import mgrassl.at.ShoppingList.domain.entity.Purchase;
import mgrassl.at.ShoppingList.domain.entity.User;
import mgrassl.at.ShoppingList.repository.UserRepository;
import mgrassl.at.ShoppingList.service.PurchaseService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PurchaseTest {

  @Autowired
  private UserRepository userRepository;
  @Autowired
  private PurchaseService purchaseService;

  @Test
  public void getPurchasesPerUser() {
    User michael = userRepository.findByUsername("michael");
    User zilia = userRepository.findByUsername("zilia");

    List<Purchase> purchasesMichael = new ArrayList<>();

    purchasesMichael = purchaseService.getPurchasesByUser(michael);

    Map<String, Object> stats = new HashMap<>();

    int totalAmount = 0;
    double totalPayment = 0;

    for (Purchase purchase : purchasesMichael) {
      totalAmount++;
      totalPayment += purchase.getPayment();
    }

    stats.put("Username: ", michael.getUsername());
    stats.put("Total Purchases: ", totalAmount);
    stats.put("Total Payment: € ", Math.round(totalPayment));

    stats.forEach((stat, value) -> {
      System.out.println(stat + value);
    });

  }

}
