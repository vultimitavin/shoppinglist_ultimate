package mgrassl.at.ShoppingList;

import mgrassl.at.ShoppingList.assembler.*;
import mgrassl.at.ShoppingList.domain.entity.*;
import mgrassl.at.ShoppingList.dto.UnitDto;
import mgrassl.at.ShoppingList.repository.*;
import mgrassl.at.ShoppingList.service.*;
import mgrassl.at.ShoppingList.utility.UuidConvertService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SetupShowCase {

  @Autowired
  private GroupService groupService;
  @Autowired
  private ArticleRepository articleRepository;
  @Autowired
  private UserRepository userRepository;
  @Autowired
  private UserService userService;
  @Autowired
  private UserAssembler userAssembler;
  @Autowired
  private ArticleService articleService;
  @Autowired
  private UuidConvertService uuidConvertService;
  @Autowired
  private UnitRepository unitRepository;
  @Autowired
  private UnitService unitService;
  @Autowired
  private PasswordEncoder passwordEncoder;
  @Autowired
  private GroupRepository groupRepository;
  @Autowired
  private UserGroupRepository userGroupRepository;
  @Autowired
  private RecipeRepository recipeRepository;
  @Autowired
  private IngredientRepository ingredientRepository;
  @Autowired
  private RecipeAssembler recipeAssembler;
  @Autowired
  private IngredientAssembler ingredientAssembler;
  @Autowired
  private RecipeService recipeService;
  @Autowired
  private UnitAssembler unitAssembler;
  @Autowired
  private PurchaseAssembler purchaseAssembler;
  @Autowired
  private PurchaseRepository purchaseRepository;
  @Autowired
  private PurchaseService purchaseService;


  @Test
  public void addUser() {
    User user = User.builder()
        .username("zilia")
        .isAdmin(false)
        .isActive(true)
        .password(passwordEncoder.encode("bumerang"))
        .build();
    userRepository.save(user);

    User user2 = User.builder()
        .username("michael")
        .isAdmin(true)
        .isActive(true)
        .password(passwordEncoder.encode("Nguyenr#12"))
        .build();
    userRepository.save(user2);

    addGroups();
  }

  @Test
  public void addGroups() {
    Group zweierlei = Group.builder()
        .name("Zweierlei")
        .build();
    groupRepository.save(zweierlei);

    User michael = userRepository.findByUsername("michael");
    User zilia = userRepository.findByUsername("zilia");

    UserGroup userGroup = UserGroup.builder()
        .user(michael)
        .group(zweierlei)
        .isGroupAdmin(true)
        .build();

    UserGroup userGroup1 = UserGroup.builder()
        .user(zilia)
        .group(zweierlei)
        .isGroupAdmin(false)
        .build();

    HashSet<UserGroup> groupUsers = new HashSet<>();

    groupUsers.add(userGroup);
    groupUsers.add(userGroup1);

    zweierlei.setGroupUsers(groupUsers);

    groupRepository.save(zweierlei);

    addUnits();
  }

  @Test
  public void addUnits() {
    UnitDto unitDto = UnitDto.builder()
        .name("Gramm")
        .abbreviation("g")
        .build();

    UnitDto unitDto1 = UnitDto.builder()
        .name("Packung")
        .abbreviation("pkg")
        .build();

    UnitDto unitDto2 = UnitDto.builder()
        .name("Stück")
        .abbreviation("stk")
        .build();

    unitService.addUnit(unitDto);
    unitService.addUnit(unitDto1);
    unitService.addUnit(unitDto2);

    addRecipe();
    addArticles();
  }

  @Test
  public void addRecipe() {
    Set<Ingredient> ingredients = new HashSet<>();

    Ingredient zwiebel = Ingredient.builder()
        .name("Zwiebel")
        .unit(unitRepository.findByName("Stück"))
        .amount(5)
        .build();

    Ingredient fleisch = Ingredient.builder()
        .name("Rindfleisch")
        .unit(unitRepository.findByName("Gramm"))
        .amount(350)
        .build();

    Ingredient paprikapulver = Ingredient.builder()
        .name("Paprikapulver")
        .unit(unitRepository.findByName("Gramm"))
        .amount(10)
        .build();

    Ingredient essig = Ingredient.builder()
        .name("Essig")
        .unit(unitRepository.findByName("Gramm"))
        .amount(5)
        .build();

    ingredients.add(zwiebel);
    ingredients.add(fleisch);
    ingredients.add(paprikapulver);
    ingredients.add(essig);

    Recipe recipe = Recipe.builder()
        .name("Gulasch")
        .ingredients(ingredients)
        .user(userRepository.findByUsername("michael"))
        .build();

    recipeRepository.save(recipe);
  }

  @Test
  public void addArticles() {
    List<Article> articles = new ArrayList<>();
    User michael = userRepository.findByUsername("michael");
    Unit stk = unitRepository.findByName("Stück");

    Date today = new Date(System.currentTimeMillis());

    articles.add(Article.builder().articleName("Aal").amount(1).price(2.54).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Absinth (45% Vol.)").amount(1).price(0.4).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Acai").amount(1).price(2.49).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Acerola").amount(1).price(3.95).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Ackerbohnen").amount(1).price(3.65).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Afa Alge frisch").amount(1).price(4.89).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Agavendicksaft").amount(1).price(2.72).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Ajvar").amount(1).price(3.98).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Aloe Vera Saft").amount(1).price(0.57).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Altbier").amount(1).price(0.2).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Amaranth").amount(1).price(4.9).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Ananassaft").amount(1).price(0.44).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Anis").amount(1).price(3.12).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Aperol Likör").amount(1).price(0.07).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Apfel").amount(1).price(1.87).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Apfeldicksaft").amount(1).price(4.31).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Apfelkuchen").amount(1).price(2.63).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Apfelmus").amount(1).price(0.76).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Apfelringe getrocknet").amount(1).price(2.55).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Apfelsaft").amount(1).price(4.16).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Apfelschorle").amount(1).price(3.45).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Apfelwein").amount(1).price(2.71).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Appenzeller").amount(1).price(1.52).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Aprikose").amount(1).price(4.83).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Aronia Beeren").amount(1).price(4.74).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Artischocke").amount(1).price(4.86).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Aubergine").amount(1).price(3.35).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Auster").amount(1).price(1.98).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Austernpilze").amount(1).price(4.5).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Avocado").amount(1).price(0.57).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Ayran").amount(1).price(3.67).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Backfisch").amount(1).price(0.56).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Backpulver").amount(1).price(3.06).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Bacon").amount(1).price(2.12).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Baguette").amount(1).price(0.69).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Baiser").amount(1).price(1.61).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Balsamico Creme").amount(1).price(0.09).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Balsamico Essig").amount(1).price(2.11).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Bambussprossen").amount(1).price(1.67).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Banane").amount(1).price(3.29).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Barsch").amount(1).price(2.33).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Basilikum").amount(1).price(1.29).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Basilikum Pesto").amount(1).price(2.05).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Basmati Reis roh").amount(1).price(3.21).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Bauchspeck").amount(1).price(2.97).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Beifuß getrocknet").amount(1).price(3.02).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Berberitzen, getrocknet").amount(1).price(3.69).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Bergkäse 50% i.Tr.").amount(1).price(3.82).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Berliner").amount(1).price(0.36).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Bier").amount(1).price(4.27).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Bier, alkoholfrei").amount(1).price(1.93).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Bierschinken").amount(1).price(3.28).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Birne").amount(1).price(4.71).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Bismarckhering").amount(1).price(2.04).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Blattsalat").amount(1).price(0.75).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Blaukraut").amount(1).price(2.98).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Blauschimmelkäse").amount(1).price(1.68).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Blumenkohl").amount(1).price(3.49).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Blutwurst").amount(1).price(1.01).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Blätterteig").amount(1).price(3.58).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Bockshornklee").amount(1).price(0.35).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Bockwurst").amount(1).price(2.05).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Bohnenkraut getrocknet").amount(1).price(1.67).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Bonbons").amount(1).price(1.92).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Borretsch").amount(1).price(4.37).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Brandteig").amount(1).price(1.08).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Bratensauce").amount(1).price(2.13).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Brathering").amount(1).price(0.06).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Bratkartoffeln").amount(1).price(2.27).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Bratwurst").amount(1).price(0.61).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Brennnessel").amount(1).price(0.92).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Brezel").amount(1).price(4.49).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Brie").amount(1).price(3.49).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Brokkoli").amount(1).price(4.38).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Brombeeren").amount(1).price(1.93).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Brötchen Weizen").amount(1).price(3.65).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Buchweizen").amount(1).price(2.05).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Buchweizenmehl").amount(1).price(4.88).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Bulgur").amount(1).price(2.08).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Butter").amount(1).price(3.36).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Butterbrezel").amount(1).price(1.42).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Butterkekse").amount(1).price(2.92).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Butterkäse").amount(1).price(0.72).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Buttermilch").amount(1).price(3.58).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Butterpilze").amount(1).price(0.03).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Bärlauch").amount(1).price(0.09).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Büffelmozzarella").amount(1).price(0.56).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Bündnerfleisch").amount(1).price(2.86).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Cabanossi").amount(1).price(3.65).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Camembert").amount(1).price(2.36).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Camu-Camu Pulver").amount(1).price(1.36).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Cappuccino").amount(1).price(3.82).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Cashewmus").amount(1).price(1.47).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Cashewnüsse").amount(1).price(3.77).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Cervelatwurst").amount(1).price(0.82).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Cevapcici").amount(1).price(4.66).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Champagner").amount(1).price(4.35).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Champignons").amount(1).price(0.64).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Cheddar Käse").amount(1).price(1.06).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Cheeseburger").amount(1).price(4.57).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Cherrytomaten").amount(1).price(2.86).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Chia").amount(1).price(2.93).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Chia Brot").amount(1).price(3.86).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Chia Brötchen").amount(1).price(4.78).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Chicken Nuggets").amount(1).price(0.02).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Chicken Wings").amount(1).price(0.76).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Chicorée").amount(1).price(4.06).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Chili con carne").amount(1).price(1.22).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Chilischote").amount(1).price(0.09).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Chinakohl").amount(1).price(3.39).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Chlorella").amount(1).price(3.78).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Ciabatta Brot").amount(1).price(1.3).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Cidre").amount(1).price(2.28).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Clementine").amount(1).price(4.84).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Cola").amount(1).price(3.73).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Cola light").amount(1).price(1.11).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Cola Zero").amount(1).price(0.01).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Cordon bleu v. Schwein").amount(1).price(1.67).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Corned Beef").amount(1).price(4.27).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Cornflakes, natur").amount(1).price(4.66).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Couscous gekocht").amount(1).price(4.37).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Couscous roh").amount(1).price(2.93).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Cranberries getrocknet").amount(1).price(1.01).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Cranberry").amount(1).price(1.34).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Creme fraiche 15%").amount(1).price(1).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Creme fraiche 30%").amount(1).price(3.92).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Crevetten").amount(1).price(1.5).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Croissant").amount(1).price(1.3).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Curry").amount(1).price(0.45).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Currywurst mit Sauce").amount(1).price(2.52).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Datteln").amount(1).price(2.19).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Dicke Bohnen").amount(1).price(3.83).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Diesel").amount(1).price(3.94).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Dill").amount(1).price(3.8).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Dinkelbrot").amount(1).price(1.41).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Dinkelbrötchen").amount(1).price(1.62).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Dinkelflocken").amount(1).price(3.76).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Dinkelkleie").amount(1).price(1.4).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Dinkelmehl, Vollkorn").amount(1).price(4.2).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Dinkelnudeln roh").amount(1).price(3.23).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Dinkelvollkornbrot").amount(1).price(2.16).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Diätbier").amount(1).price(0.29).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Dominosteine").amount(1).price(2.05).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Donut").amount(1).price(0.16).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Dorade").amount(1).price(2.9).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Drachenfrucht").amount(1).price(2.16).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Döner").amount(1).price(2.1).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Dönerfleisch").amount(1).price(2.45).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Edamer Käse").amount(1).price(4.9).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Ei - Vollei").amount(1).price(2.79).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Cherrytomaten").amount(1).price(4.92).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Chia").amount(1).price(4.75).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Chia Brot").amount(1).price(1.65).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Chia Brötchen").amount(1).price(0.48).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Chicken Nuggets").amount(1).price(0.6).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Chicken Wings").amount(1).price(2.92).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Chicorée").amount(1).price(4.88).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Chili con carne").amount(1).price(2.63).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Chilischote").amount(1).price(3.83).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Chinakohl").amount(1).price(3.32).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Chlorella").amount(1).price(4.32).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Ciabatta Brot").amount(1).price(0.45).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Cidre").amount(1).price(3.61).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Clementine").amount(1).price(4.06).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Cola").amount(1).price(0.79).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Cola light").amount(1).price(4.46).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Cola Zero").amount(1).price(0.17).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Cordon bleu v. Schwein").amount(1).price(3.17).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Corned Beef").amount(1).price(0.08).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Cornflakes, natur").amount(1).price(3).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Couscous gekocht").amount(1).price(3.48).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Couscous roh").amount(1).price(3.89).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Cranberries getrocknet").amount(1).price(1.67).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Cranberry").amount(1).price(3.25).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Creme fraiche 15%").amount(1).price(2.31).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Creme fraiche 30%").amount(1).price(1.66).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Crevetten").amount(1).price(0.58).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Croissant").amount(1).price(4.78).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Curry").amount(1).price(3.95).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Currywurst mit Sauce").amount(1).price(4.75).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Datteln").amount(1).price(2.67).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Dicke Bohnen").amount(1).price(4.16).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Diesel").amount(1).price(0.15).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Dill").amount(1).price(0.83).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Dinkelbrot").amount(1).price(4.84).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Dinkelbrötchen").amount(1).price(2.57).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Dinkelflocken").amount(1).price(2.38).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Dinkelkleie").amount(1).price(3.93).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Dinkelmehl, Vollkorn").amount(1).price(3.99).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Dinkelnudeln roh").amount(1).price(3.39).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Dinkelvollkornbrot").amount(1).price(0.47).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Diätbier").amount(1).price(1.64).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Dominosteine").amount(1).price(2.49).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Donut").amount(1).price(1.31).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Dorade").amount(1).price(1.03).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Drachenfrucht").amount(1).price(2.22).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Döner").amount(1).price(4.92).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Dönerfleisch").amount(1).price(3.97).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Edamer Käse").amount(1).price(1.28).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Ei - Vollei").amount(1).price(0.77).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Eierkuchen").amount(1).price(0.98).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Eierlikör (20% Vol.)").amount(1).price(1.49).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Eierpfannkuchen").amount(1).price(4.83).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Eiersalat mit Mayonnaise").amount(1).price(4.36).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Eierschecke").amount(1).price(4.27).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Eigelb").amount(1).price(2.56).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Eisbein roh").amount(1).price(1.62).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Eisbergsalat").amount(1).price(1.21).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Eiscreme").amount(1).price(2.6).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Eiscreme").amount(1).price(2.43).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Eiskaffee").amount(1).price(0.99).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Eistee").amount(1).price(0.78).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Eiweiss / Eiklar").amount(1).price(3.52).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Eiweißbrot").amount(1).price(4.3).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Eiweißbrötchen").amount(1).price(3.69).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Eiweißpulver").amount(1).price(2.52).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Eiweißshake mit Milch").amount(1).price(2.37).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Emmentaler Käse").amount(1).price(2.18).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Endiviensalat").amount(1).price(0.57).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Energydrink").amount(1).price(2.78).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Ente").amount(1).price(4.15).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Entenbrust").amount(1).price(4.91).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Entenleber").amount(1).price(2.88).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Erbsensuppe").amount(1).price(0.03).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Erdbeeren").amount(1).price(0.46).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Erdbeerjoghurt 1.5%").amount(1).price(0.33).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Erdbeerkuchen Biskuitteig").amount(1).price(4.25).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Erdnussbutter").amount(1).price(1.95).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Erdnussflips").amount(1).price(2.28).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Erdnussmus").amount(1).price(3.4).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Erdnussöl").amount(1).price(3.09).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Erdnüsse").amount(1).price(1.38).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Erythrit").amount(1).price(1.56).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Espresso").amount(1).price(1.67).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Essig").amount(1).price(3.36).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Essig Öl Dressing").amount(1).price(3.9).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Estragon frisch").amount(1).price(3.64).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Estragon getrocknet").amount(1).price(1.01).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Falafel").amount(1).price(3.26).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Fanta Orange").amount(1).price(1.95).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Federweißer").amount(1).price(3.91).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Feige").amount(1).price(3.14).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Feldsalat").amount(1).price(2.1).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Fenchel").amount(1).price(1.61).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Feta Käse").amount(1).price(0.16).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Fischstäbchen").amount(1).price(1.7).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Fladenbrot").amount(1).price(1.49).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Flammkuchen").amount(1).price(0.83).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Fleischbrühe").amount(1).price(1.41).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Fleischkäse").amount(1).price(0.4).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Fleischsalat").amount(1).price(2.48).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Fleischwurst").amount(1).price(1.9).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Flohsamenschalen").amount(1).price(2.55).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Flusskrebs").amount(1).price(2.19).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Forelle").amount(1).price(1.44).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Forelle geräuchert").amount(1).price(2.74).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Franzbrötchen").amount(1).price(2.95).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Frikadelle").amount(1).price(3.14).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Frischkäse fettreduziert").amount(1).price(3.91).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Frischkäse körnig").amount(1).price(1.83).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Frischkäse, Doppelrahm").amount(1).price(4.13).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Fruchteis / Sorbet").amount(1).price(2.01).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Fruchtjoghurt 1.5%").amount(1).price(4.44).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Fruchtsaft").amount(1).price(3.32).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Fruchtzucker").amount(1).price(3.66).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Früchtemüsli").amount(1).price(2.61).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Früchtetee").amount(1).price(2.68).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Frühlingsrolle").amount(1).price(3.93).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Frühlingszwiebel").amount(1).price(1.57).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Frühlingszwiebeln").amount(1).price(1.5).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Galiamelone").amount(1).price(2.09).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Gambas").amount(1).price(1.48).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Gans").amount(1).price(4.96).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Garnelen").amount(1).price(3.47).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Geflügelfleischwurst").amount(1).price(1.73).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Geflügelwiener").amount(1).price(1.15).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Geflügelwurst").amount(1).price(1.92).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Gelatine").amount(1).price(3.17).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Gemüsebrühe").amount(1).price(2.07).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Gemüsesaft").amount(1).price(1.98).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Germknödel").amount(1).price(4.95).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Gerstengras Pulver").amount(1).price(0.17).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Gerstenmehl, Vollkorn").amount(1).price(1.05).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Gewürzgurken").amount(1).price(4.63).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Gheebutter").amount(1).price(2.82).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Gin (40% Vol.)").amount(1).price(2.1).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Glasnudeln roh").amount(1).price(3.69).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Gluten, Weizenkleber").amount(1).price(4.73).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Glühwein").amount(1).price(4.08).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Gnocchi").amount(1).price(3.2).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Goji Beeren getrocknet").amount(1).price(1.97).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Gorgonzola").amount(1).price(4.88).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Gouda Käse").amount(1).price(1.6).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Granatapfel").amount(1).price(3.51).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Grapefruit").amount(1).price(3.35).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Grapefruitsaft").amount(1).price(0.03).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Grappa (40% Vol.)").amount(1).price(1.17).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Graubrot").amount(1).price(1.18).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Graupen").amount(1).price(4.7).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("griechischer Joghurt 10%").amount(1).price(3.26).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Grießbrei").amount(1).price(3.46).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Grissinis").amount(1).price(0.42).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Grüne Bohnen").amount(1).price(2.41).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Grüne Erbsen").amount(1).price(3.21).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Grüner Spargel").amount(1).price(3.87).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Grüner tee").amount(1).price(3.94).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Grünkohl").amount(1).price(3.81).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Grützwurst").amount(1).price(3.09).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Guacamole").amount(1).price(2.34).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Guarkernmehl").amount(1).price(0.85).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Guave").amount(1).price(2.04).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Gummibärchen").amount(1).price(3.08).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Gurke grün").amount(1).price(0.63).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Gyros").amount(1).price(3).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Gänsebrust").amount(1).price(3.74).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Gänsekeule").amount(1).price(1.29).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Gänseleberpastete").amount(1).price(0.53).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Gänseschmalz").amount(1).price(1.21).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Götterspeise gekocht").amount(1).price(0.11).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Hackfleisch gemischt").amount(1).price(2.04).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Haferflocken").amount(1).price(0.88).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Haferkleie").amount(1).price(4.08).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Hafermilch").amount(1).price(2.46).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Hagebutten").amount(1).price(2.56).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Hagebuttentee").amount(1).price(3.16).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Halbfettmargarine").amount(1).price(2.87).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Halloumi").amount(1).price(0.28).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Hamburger").amount(1).price(1.72).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Handkäse").amount(1).price(4.06).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Hanfmehl").amount(1).price(4.85).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Hanfsamen").amount(1).price(0.18).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Harzer Käse").amount(1).price(4.03).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Hase").amount(1).price(2.86).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Haselnussmilch").amount(1).price(0.01).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Haselnussmus").amount(1).price(2.1).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Haselnüsse").amount(1).price(2.3).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Hefeflocken").amount(1).price(1.82).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Hefeteig").amount(1).price(2.82).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Hefezopf").amount(1).price(2.89).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Heidelbeeren").amount(1).price(3.32).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Heilbutt").amount(1).price(1.82).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Heiße Schokolade").amount(1).price(0.45).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Heringsfilet").amount(1).price(2.08).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Himbeeren").amount(1).price(2.93).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Hirsch").amount(1).price(0.51).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Hirse").amount(1).price(4.4).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Hirseflocken").amount(1).price(2.25).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Holunder").amount(1).price(3.48).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Holundersaft").amount(1).price(2.93).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Honig").amount(1).price(3.81).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Honigmelone").amount(1).price(4.6).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Hugo Aperitif Cocktail").amount(1).price(0.03).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Hummer").amount(1).price(3.04).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Hummus").amount(1).price(1.86).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Hähnchen").amount(1).price(4.37).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Hähnchen gebraten").amount(1).price(0.47).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Hähnchenbrust").amount(1).price(1.32).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Hähnchenleber").amount(1).price(0.02).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Hähnchenschenkel").amount(1).price(2.39).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Hähnchenschnitzel paniert gebraten").amount(1).price(0.32).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Hühnerfrikassee").amount(1).price(1.5).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Hühnersuppe").amount(1).price(4.61).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Hüttenkäse").amount(1).price(3.28).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Ingwer").amount(1).price(1.13).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Isomalt").amount(1).price(4.28).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Jagdwurst").amount(1).price(3.75).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Jakobsmuschel").amount(1).price(3.5).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Jasminreis roh").amount(1).price(4.15).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Joghurt, 0,1%").amount(1).price(3.06).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Joghurt, 1,5%").amount(1).price(2.25).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Joghurt, 3.5%").amount(1).price(2.54).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Joghurtdressing").amount(1).price(4.24).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Joghurteis").amount(1).price(4.88).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Johannisbrotkernmehl").amount(1).price(0.59).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Jägermeister Kräuterlikör").amount(1).price(2.24).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kabeljau / Dorsch").amount(1).price(0.62).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kaffee komplett").amount(1).price(2.21).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kaffee mit Milch").amount(1).price(4.24).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kaffee schwarz").amount(1).price(3.15).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kaffeesahne 10%").amount(1).price(1.83).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kaffeesahne 4%").amount(1).price(4.22).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kaffeesahne 7,5%").amount(1).price(2.72).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kaisergemüse").amount(1).price(1.34).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kaiserschmarrn").amount(1).price(4).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kakao, stark entölt").amount(1).price(0.01).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kakaobohnen").amount(1).price(4.51).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kakaobutter").amount(1).price(3.83).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kakaofasern").amount(1).price(3.13).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kakaomilch 1,5%").amount(1).price(2.19).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kakaomilch 3,5%").amount(1).price(0.37).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kakaopulver gezuckert").amount(1).price(3.47).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kaki").amount(1).price(3.24).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kalbsbries").amount(1).price(4.01).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kalbsfilet").amount(1).price(4.82).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kalbsfleisch").amount(1).price(3.56).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kalbsleber").amount(1).price(4.2).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kalbsleberwurst").amount(1).price(0).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kalbsrücken").amount(1).price(1.91).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kalbsschnitzel").amount(1).price(4.61).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kalorien Ahornsirup").amount(1).price(4.12).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kalorien Ananas").amount(1).price(0.12).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kalorien Apfelessig").amount(1).price(4.63).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kamut").amount(1).price(0.96).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kandiszucker").amount(1).price(1.32).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kaninchen").amount(1).price(4.67).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kapern, Glas").amount(1).price(3.31).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Karambole").amount(1).price(3.52).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Karamel").amount(1).price(1.8).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kardamom gemahlen").amount(1).price(0.62).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Karottensaft").amount(1).price(4.25).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Karpfen").amount(1).price(2.84).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kartoffelbrot").amount(1).price(2.9).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kartoffelchips").amount(1).price(2.42).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kartoffelfasern").amount(1).price(0.89).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kartoffelgratin").amount(1).price(3.88).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kartoffeln gekocht").amount(1).price(0.92).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kartoffeln roh").amount(1).price(1.69).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kartoffelpuffer").amount(1).price(2.2).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kartoffelpüree").amount(1).price(0.73).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kartoffelrösti").amount(1).price(4.35).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kartoffelsalat").amount(1).price(4.59).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kartoffelspalten").amount(1).price(1.41).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kartoffelstärke").amount(1).price(2).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kartoffelsuppe").amount(1).price(4.69).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kasseler").amount(1).price(0.89).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Katenschinken").amount(1).price(2.65).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kaugummi").amount(1).price(3.08).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kaviar").amount(1).price(0.68).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kefir").amount(1).price(0.34).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kekse").amount(1).price(4.31).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kerbel").amount(1).price(0.85).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Ketchup").amount(1).price(1.51).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kichererbsen").amount(1).price(1.82).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kichererbsenmehl").amount(1).price(4.72).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kidneybohnen").amount(1).price(1.72).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kiwi").amount(1).price(3.17).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Klöße gekocht").amount(1).price(1.02).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Knoblauch").amount(1).price(1.32).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Knollensellerie").amount(1).price(4.65).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Knuspermüsli").amount(1).price(4).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kochschinken").amount(1).price(4.12).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kohlrabi").amount(1).price(2.3).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kohlrübe").amount(1).price(3.95).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kokosblütenzucker").amount(1).price(1.12).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kokosfett").amount(1).price(4.19).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kokosmehl").amount(1).price(0.81).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kokosmilch").amount(1).price(5).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kokosnuss").amount(1).price(0.72).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kokosraspel").amount(1).price(2.53).isBought(true).user(michael).unit(stk).dateToBuy(today).build());
    articles.add(Article.builder().articleName("Kokoswasser").amount(1).price(3.52).isBought(true).user(michael).unit(stk).dateToBuy(today).build());

    articleRepository.saveAll(articles);
    addPurchases();
  }

  @Test
  public void addPurchases() {
    User michael = userRepository.findByUsername("michael");
    User zilia = userRepository.findByUsername("zilia");

    List<Purchase> purchases = new ArrayList<>();

    purchases.add(Purchase.builder().user(michael).payment(21.52).build());
    purchases.add(Purchase.builder().user(michael).payment(22.5).build());
    purchases.add(Purchase.builder().user(michael).payment(8.52).build());
    purchases.add(Purchase.builder().user(michael).payment(24.37).build());
    purchases.add(Purchase.builder().user(michael).payment(9.28).build());
    purchases.add(Purchase.builder().user(michael).payment(16.05).build());
    purchases.add(Purchase.builder().user(michael).payment(14.54).build());
    purchases.add(Purchase.builder().user(michael).payment(20.6).build());
    purchases.add(Purchase.builder().user(michael).payment(19.61).build());
    purchases.add(Purchase.builder().user(michael).payment(10.48).build());
    purchases.add(Purchase.builder().user(michael).payment(14.45).build());
    purchases.add(Purchase.builder().user(michael).payment(23.46).build());
    purchases.add(Purchase.builder().user(michael).payment(23.2).build());
    purchases.add(Purchase.builder().user(michael).payment(7.92).build());
    purchases.add(Purchase.builder().user(michael).payment(13.02).build());
    purchases.add(Purchase.builder().user(michael).payment(17.63).build());
    purchases.add(Purchase.builder().user(michael).payment(13.95).build());
    purchases.add(Purchase.builder().user(michael).payment(11.98).build());
    purchases.add(Purchase.builder().user(michael).payment(10.69).build());
    purchases.add(Purchase.builder().user(michael).payment(24.03).build());
    purchases.add(Purchase.builder().user(michael).payment(21.32).build());
    purchases.add(Purchase.builder().user(michael).payment(9.12).build());
    purchases.add(Purchase.builder().user(michael).payment(21.06).build());
    purchases.add(Purchase.builder().user(michael).payment(26.39).build());
    purchases.add(Purchase.builder().user(michael).payment(18.95).build());
    purchases.add(Purchase.builder().user(michael).payment(13.01).build());
    purchases.add(Purchase.builder().user(michael).payment(7.64).build());
    purchases.add(Purchase.builder().user(michael).payment(12.82).build());
    purchases.add(Purchase.builder().user(michael).payment(12.43).build());
    purchases.add(Purchase.builder().user(michael).payment(13.2).build());
    purchases.add(Purchase.builder().user(michael).payment(11.86).build());
    purchases.add(Purchase.builder().user(michael).payment(22.03).build());
    purchases.add(Purchase.builder().user(michael).payment(16.88).build());
    purchases.add(Purchase.builder().user(michael).payment(20.83).build());
    purchases.add(Purchase.builder().user(michael).payment(23.58).build());
    purchases.add(Purchase.builder().user(michael).payment(8.97).build());
    purchases.add(Purchase.builder().user(michael).payment(21.5).build());
    purchases.add(Purchase.builder().user(michael).payment(7.59).build());
    purchases.add(Purchase.builder().user(michael).payment(7.37).build());
    purchases.add(Purchase.builder().user(michael).payment(11.91).build());
    purchases.add(Purchase.builder().user(michael).payment(8).build());
    purchases.add(Purchase.builder().user(michael).payment(11.42).build());
    purchases.add(Purchase.builder().user(michael).payment(12.21).build());
    purchases.add(Purchase.builder().user(michael).payment(8.89).build());
    purchases.add(Purchase.builder().user(michael).payment(15).build());
    purchases.add(Purchase.builder().user(michael).payment(26.91).build());
    purchases.add(Purchase.builder().user(michael).payment(7.26).build());
    purchases.add(Purchase.builder().user(michael).payment(8.53).build());
    purchases.add(Purchase.builder().user(michael).payment(21.74).build());
    purchases.add(Purchase.builder().user(michael).payment(18.01).build());
    purchases.add(Purchase.builder().user(michael).payment(10.56).build());
    purchases.add(Purchase.builder().user(michael).payment(26.73).build());
    purchases.add(Purchase.builder().user(michael).payment(14.52).build());
    purchases.add(Purchase.builder().user(michael).payment(25.86).build());
    purchases.add(Purchase.builder().user(michael).payment(20.68).build());
    purchases.add(Purchase.builder().user(michael).payment(8.71).build());
    purchases.add(Purchase.builder().user(michael).payment(7.8).build());
    purchases.add(Purchase.builder().user(michael).payment(9.23).build());
    purchases.add(Purchase.builder().user(michael).payment(21.62).build());
    purchases.add(Purchase.builder().user(michael).payment(13.64).build());
    purchases.add(Purchase.builder().user(michael).payment(22.4).build());
    purchases.add(Purchase.builder().user(michael).payment(10.95).build());
    purchases.add(Purchase.builder().user(michael).payment(16.37).build());
    purchases.add(Purchase.builder().user(michael).payment(16.74).build());
    purchases.add(Purchase.builder().user(michael).payment(22.04).build());
    purchases.add(Purchase.builder().user(michael).payment(7.81).build());
    purchases.add(Purchase.builder().user(michael).payment(12.6).build());
    purchases.add(Purchase.builder().user(michael).payment(9.04).build());
    purchases.add(Purchase.builder().user(michael).payment(25.15).build());
    purchases.add(Purchase.builder().user(michael).payment(18.24).build());
    purchases.add(Purchase.builder().user(michael).payment(8.39).build());
    purchases.add(Purchase.builder().user(michael).payment(7.64).build());
    purchases.add(Purchase.builder().user(michael).payment(19.9).build());
    purchases.add(Purchase.builder().user(michael).payment(17.26).build());
    purchases.add(Purchase.builder().user(michael).payment(18.42).build());
    purchases.add(Purchase.builder().user(michael).payment(15.83).build());
    purchases.add(Purchase.builder().user(michael).payment(26.58).build());
    purchases.add(Purchase.builder().user(michael).payment(12.36).build());
    purchases.add(Purchase.builder().user(michael).payment(15.38).build());
    purchases.add(Purchase.builder().user(michael).payment(11.31).build());
    purchases.add(Purchase.builder().user(michael).payment(24.65).build());
    purchases.add(Purchase.builder().user(michael).payment(13.77).build());
    purchases.add(Purchase.builder().user(michael).payment(18.06).build());
    purchases.add(Purchase.builder().user(michael).payment(25.91).build());
    purchases.add(Purchase.builder().user(michael).payment(13.18).build());
    purchases.add(Purchase.builder().user(michael).payment(25.41).build());
    purchases.add(Purchase.builder().user(michael).payment(18.3).build());
    purchases.add(Purchase.builder().user(michael).payment(13.49).build());
    purchases.add(Purchase.builder().user(michael).payment(23.78).build());
    purchases.add(Purchase.builder().user(michael).payment(25.3).build());
    purchases.add(Purchase.builder().user(michael).payment(10.57).build());
    purchases.add(Purchase.builder().user(michael).payment(21.29).build());
    purchases.add(Purchase.builder().user(michael).payment(21.16).build());
    purchases.add(Purchase.builder().user(michael).payment(22.35).build());
    purchases.add(Purchase.builder().user(michael).payment(19.77).build());
    purchases.add(Purchase.builder().user(michael).payment(25.75).build());
    purchases.add(Purchase.builder().user(michael).payment(24.04).build());
    purchases.add(Purchase.builder().user(michael).payment(10.28).build());
    purchases.add(Purchase.builder().user(michael).payment(26.48).build());
    purchases.add(Purchase.builder().user(michael).payment(24.38).build());
    purchases.add(Purchase.builder().user(michael).payment(22.54).build());
    purchases.add(Purchase.builder().user(michael).payment(17.24).build());
    purchases.add(Purchase.builder().user(michael).payment(17.64).build());
    purchases.add(Purchase.builder().user(michael).payment(20.18).build());
    purchases.add(Purchase.builder().user(michael).payment(10.87).build());
    purchases.add(Purchase.builder().user(michael).payment(20.75).build());
    purchases.add(Purchase.builder().user(michael).payment(14.32).build());
    purchases.add(Purchase.builder().user(michael).payment(11.16).build());
    purchases.add(Purchase.builder().user(michael).payment(26.53).build());
    purchases.add(Purchase.builder().user(michael).payment(12.86).build());
    purchases.add(Purchase.builder().user(michael).payment(12.56).build());
    purchases.add(Purchase.builder().user(michael).payment(7.66).build());
    purchases.add(Purchase.builder().user(michael).payment(25.69).build());
    purchases.add(Purchase.builder().user(michael).payment(10.18).build());
    purchases.add(Purchase.builder().user(michael).payment(23.93).build());
    purchases.add(Purchase.builder().user(michael).payment(13.11).build());
    purchases.add(Purchase.builder().user(michael).payment(11.23).build());
    purchases.add(Purchase.builder().user(michael).payment(18.91).build());
    purchases.add(Purchase.builder().user(michael).payment(12.19).build());
    purchases.add(Purchase.builder().user(michael).payment(13.18).build());
    purchases.add(Purchase.builder().user(michael).payment(25.14).build());
    purchases.add(Purchase.builder().user(michael).payment(26.17).build());
    purchases.add(Purchase.builder().user(michael).payment(26.71).build());
    purchases.add(Purchase.builder().user(michael).payment(9.16).build());
    purchases.add(Purchase.builder().user(michael).payment(25.53).build());
    purchases.add(Purchase.builder().user(michael).payment(8.46).build());
    purchases.add(Purchase.builder().user(michael).payment(16.94).build());
    purchases.add(Purchase.builder().user(michael).payment(8.97).build());
    purchases.add(Purchase.builder().user(michael).payment(19.39).build());
    purchases.add(Purchase.builder().user(michael).payment(12.76).build());
    purchases.add(Purchase.builder().user(michael).payment(20.22).build());
    purchases.add(Purchase.builder().user(michael).payment(16.36).build());
    purchases.add(Purchase.builder().user(michael).payment(23.27).build());
    purchases.add(Purchase.builder().user(michael).payment(18.92).build());
    purchases.add(Purchase.builder().user(michael).payment(7.81).build());
    purchases.add(Purchase.builder().user(michael).payment(18.84).build());
    purchases.add(Purchase.builder().user(michael).payment(25.63).build());
    purchases.add(Purchase.builder().user(michael).payment(8.27).build());
    purchases.add(Purchase.builder().user(michael).payment(15.67).build());
    purchases.add(Purchase.builder().user(michael).payment(13.28).build());
    purchases.add(Purchase.builder().user(michael).payment(12.59).build());
    purchases.add(Purchase.builder().user(michael).payment(23.53).build());
    purchases.add(Purchase.builder().user(michael).payment(7.62).build());
    purchases.add(Purchase.builder().user(michael).payment(25.42).build());
    purchases.add(Purchase.builder().user(michael).payment(11.3).build());
    purchases.add(Purchase.builder().user(michael).payment(26.16).build());
    purchases.add(Purchase.builder().user(michael).payment(14.72).build());
    purchases.add(Purchase.builder().user(michael).payment(18.85).build());
    purchases.add(Purchase.builder().user(michael).payment(24.18).build());
    purchases.add(Purchase.builder().user(michael).payment(8.98).build());
    purchases.add(Purchase.builder().user(michael).payment(8.83).build());
    purchases.add(Purchase.builder().user(michael).payment(26.84).build());
    purchases.add(Purchase.builder().user(michael).payment(22.84).build());
    purchases.add(Purchase.builder().user(michael).payment(22.82).build());
    purchases.add(Purchase.builder().user(michael).payment(8.78).build());
    purchases.add(Purchase.builder().user(michael).payment(15.98).build());
    purchases.add(Purchase.builder().user(michael).payment(12.09).build());
    purchases.add(Purchase.builder().user(michael).payment(26.92).build());
    purchases.add(Purchase.builder().user(michael).payment(7.34).build());
    purchases.add(Purchase.builder().user(michael).payment(13.99).build());
    purchases.add(Purchase.builder().user(michael).payment(11.11).build());
    purchases.add(Purchase.builder().user(michael).payment(15.41).build());
    purchases.add(Purchase.builder().user(michael).payment(7.88).build());
    purchases.add(Purchase.builder().user(michael).payment(11.4).build());
    purchases.add(Purchase.builder().user(michael).payment(24.95).build());
    purchases.add(Purchase.builder().user(michael).payment(24.25).build());
    purchases.add(Purchase.builder().user(michael).payment(10.77).build());
    purchases.add(Purchase.builder().user(michael).payment(12.1).build());
    purchases.add(Purchase.builder().user(michael).payment(12.79).build());
    purchases.add(Purchase.builder().user(michael).payment(15.88).build());
    purchases.add(Purchase.builder().user(michael).payment(26.35).build());
    purchases.add(Purchase.builder().user(michael).payment(22.34).build());
    purchases.add(Purchase.builder().user(michael).payment(26.68).build());
    purchases.add(Purchase.builder().user(michael).payment(12.66).build());
    purchases.add(Purchase.builder().user(michael).payment(24.91).build());
    purchases.add(Purchase.builder().user(michael).payment(23.15).build());
    purchases.add(Purchase.builder().user(michael).payment(8.77).build());
    purchases.add(Purchase.builder().user(michael).payment(21.83).build());
    purchases.add(Purchase.builder().user(michael).payment(25.45).build());
    purchases.add(Purchase.builder().user(michael).payment(15.1).build());
    purchases.add(Purchase.builder().user(michael).payment(10.93).build());
    purchases.add(Purchase.builder().user(michael).payment(18.67).build());
    purchases.add(Purchase.builder().user(michael).payment(10.97).build());
    purchases.add(Purchase.builder().user(michael).payment(18.72).build());
    purchases.add(Purchase.builder().user(michael).payment(9.15).build());
    purchases.add(Purchase.builder().user(michael).payment(20.06).build());
    purchases.add(Purchase.builder().user(michael).payment(26.62).build());
    purchases.add(Purchase.builder().user(michael).payment(10.61).build());
    purchases.add(Purchase.builder().user(michael).payment(20.94).build());
    purchases.add(Purchase.builder().user(michael).payment(14.11).build());
    purchases.add(Purchase.builder().user(michael).payment(21.21).build());
    purchases.add(Purchase.builder().user(michael).payment(17.42).build());
    purchases.add(Purchase.builder().user(michael).payment(15.83).build());
    purchases.add(Purchase.builder().user(michael).payment(17.97).build());
    purchases.add(Purchase.builder().user(michael).payment(13.65).build());
    purchases.add(Purchase.builder().user(michael).payment(20.19).build());
    purchases.add(Purchase.builder().user(michael).payment(11.77).build());
    purchases.add(Purchase.builder().user(michael).payment(15.62).build());
    purchases.add(Purchase.builder().user(michael).payment(19.91).build());
    purchases.add(Purchase.builder().user(michael).payment(26.69).build());
    purchases.add(Purchase.builder().user(michael).payment(16.65).build());
    purchases.add(Purchase.builder().user(michael).payment(15.63).build());
    purchases.add(Purchase.builder().user(michael).payment(7.52).build());
    purchases.add(Purchase.builder().user(michael).payment(13.77).build());
    purchases.add(Purchase.builder().user(michael).payment(18.93).build());
    purchases.add(Purchase.builder().user(michael).payment(23.12).build());
    purchases.add(Purchase.builder().user(michael).payment(13.76).build());
    purchases.add(Purchase.builder().user(michael).payment(18.63).build());
    purchases.add(Purchase.builder().user(michael).payment(26.61).build());
    purchases.add(Purchase.builder().user(michael).payment(19.66).build());
    purchases.add(Purchase.builder().user(michael).payment(11.87).build());
    purchases.add(Purchase.builder().user(michael).payment(19.48).build());
    purchases.add(Purchase.builder().user(michael).payment(13.69).build());
    purchases.add(Purchase.builder().user(michael).payment(7.69).build());
    purchases.add(Purchase.builder().user(michael).payment(9.64).build());
    purchases.add(Purchase.builder().user(michael).payment(25.31).build());
    purchases.add(Purchase.builder().user(michael).payment(18.52).build());
    purchases.add(Purchase.builder().user(michael).payment(17.56).build());
    purchases.add(Purchase.builder().user(michael).payment(12.96).build());
    purchases.add(Purchase.builder().user(michael).payment(19.92).build());
    purchases.add(Purchase.builder().user(michael).payment(23.17).build());
    purchases.add(Purchase.builder().user(michael).payment(22.96).build());
    purchases.add(Purchase.builder().user(michael).payment(21.44).build());
    purchases.add(Purchase.builder().user(michael).payment(20.84).build());
    purchases.add(Purchase.builder().user(michael).payment(16.05).build());
    purchases.add(Purchase.builder().user(michael).payment(23.15).build());
    purchases.add(Purchase.builder().user(michael).payment(26.47).build());
    purchases.add(Purchase.builder().user(michael).payment(26.57).build());
    purchases.add(Purchase.builder().user(michael).payment(21.04).build());
    purchases.add(Purchase.builder().user(michael).payment(13.52).build());
    purchases.add(Purchase.builder().user(michael).payment(23.94).build());
    purchases.add(Purchase.builder().user(michael).payment(10.79).build());
    purchases.add(Purchase.builder().user(michael).payment(16.41).build());
    purchases.add(Purchase.builder().user(michael).payment(14.66).build());
    purchases.add(Purchase.builder().user(michael).payment(10.43).build());
    purchases.add(Purchase.builder().user(michael).payment(19.81).build());
    purchases.add(Purchase.builder().user(michael).payment(23.27).build());
    purchases.add(Purchase.builder().user(michael).payment(22.36).build());
    purchases.add(Purchase.builder().user(michael).payment(22.28).build());
    purchases.add(Purchase.builder().user(michael).payment(15.5).build());
    purchases.add(Purchase.builder().user(michael).payment(20.74).build());
    purchases.add(Purchase.builder().user(michael).payment(9.48).build());
    purchases.add(Purchase.builder().user(michael).payment(19.12).build());
    purchases.add(Purchase.builder().user(michael).payment(19.16).build());
    purchases.add(Purchase.builder().user(michael).payment(13.91).build());
    purchases.add(Purchase.builder().user(michael).payment(19.7).build());
    purchases.add(Purchase.builder().user(michael).payment(17.88).build());
    purchases.add(Purchase.builder().user(michael).payment(23.43).build());
    purchases.add(Purchase.builder().user(michael).payment(22.89).build());
    purchases.add(Purchase.builder().user(michael).payment(10.4).build());
    purchases.add(Purchase.builder().user(zilia).payment(13.74).build());
    purchases.add(Purchase.builder().user(zilia).payment(8.59).build());
    purchases.add(Purchase.builder().user(zilia).payment(10.87).build());
    purchases.add(Purchase.builder().user(zilia).payment(21.87).build());
    purchases.add(Purchase.builder().user(zilia).payment(24.96).build());
    purchases.add(Purchase.builder().user(zilia).payment(14.1).build());
    purchases.add(Purchase.builder().user(zilia).payment(8.93).build());
    purchases.add(Purchase.builder().user(zilia).payment(15.99).build());
    purchases.add(Purchase.builder().user(zilia).payment(8.81).build());
    purchases.add(Purchase.builder().user(zilia).payment(10.31).build());
    purchases.add(Purchase.builder().user(zilia).payment(12.64).build());
    purchases.add(Purchase.builder().user(zilia).payment(9.19).build());
    purchases.add(Purchase.builder().user(zilia).payment(20.88).build());
    purchases.add(Purchase.builder().user(zilia).payment(26.76).build());
    purchases.add(Purchase.builder().user(zilia).payment(18.26).build());
    purchases.add(Purchase.builder().user(zilia).payment(17.05).build());
    purchases.add(Purchase.builder().user(zilia).payment(23.34).build());
    purchases.add(Purchase.builder().user(zilia).payment(25.33).build());
    purchases.add(Purchase.builder().user(zilia).payment(7.06).build());
    purchases.add(Purchase.builder().user(zilia).payment(17.99).build());
    purchases.add(Purchase.builder().user(zilia).payment(19.09).build());
    purchases.add(Purchase.builder().user(zilia).payment(13.17).build());
    purchases.add(Purchase.builder().user(zilia).payment(22.8).build());
    purchases.add(Purchase.builder().user(zilia).payment(20.3).build());
    purchases.add(Purchase.builder().user(zilia).payment(23.3).build());
    purchases.add(Purchase.builder().user(zilia).payment(7.63).build());
    purchases.add(Purchase.builder().user(zilia).payment(13.25).build());
    purchases.add(Purchase.builder().user(zilia).payment(17.38).build());
    purchases.add(Purchase.builder().user(zilia).payment(20.2).build());
    purchases.add(Purchase.builder().user(zilia).payment(21.99).build());
    purchases.add(Purchase.builder().user(zilia).payment(16.92).build());
    purchases.add(Purchase.builder().user(zilia).payment(13.91).build());
    purchases.add(Purchase.builder().user(zilia).payment(21.19).build());
    purchases.add(Purchase.builder().user(zilia).payment(9.02).build());
    purchases.add(Purchase.builder().user(zilia).payment(12.57).build());
    purchases.add(Purchase.builder().user(zilia).payment(10.23).build());
    purchases.add(Purchase.builder().user(zilia).payment(7.53).build());
    purchases.add(Purchase.builder().user(zilia).payment(9.94).build());
    purchases.add(Purchase.builder().user(zilia).payment(20.97).build());
    purchases.add(Purchase.builder().user(zilia).payment(19.89).build());
    purchases.add(Purchase.builder().user(zilia).payment(14.66).build());
    purchases.add(Purchase.builder().user(zilia).payment(22.64).build());
    purchases.add(Purchase.builder().user(zilia).payment(14.94).build());
    purchases.add(Purchase.builder().user(zilia).payment(23.18).build());
    purchases.add(Purchase.builder().user(zilia).payment(7.04).build());
    purchases.add(Purchase.builder().user(zilia).payment(22.85).build());
    purchases.add(Purchase.builder().user(zilia).payment(15.34).build());
    purchases.add(Purchase.builder().user(zilia).payment(21.99).build());
    purchases.add(Purchase.builder().user(zilia).payment(25.55).build());
    purchases.add(Purchase.builder().user(zilia).payment(19.46).build());
    purchases.add(Purchase.builder().user(zilia).payment(8.16).build());
    purchases.add(Purchase.builder().user(zilia).payment(17.86).build());
    purchases.add(Purchase.builder().user(zilia).payment(7.77).build());
    purchases.add(Purchase.builder().user(zilia).payment(26.03).build());
    purchases.add(Purchase.builder().user(zilia).payment(14.61).build());
    purchases.add(Purchase.builder().user(zilia).payment(12.97).build());
    purchases.add(Purchase.builder().user(zilia).payment(11.67).build());
    purchases.add(Purchase.builder().user(zilia).payment(24.72).build());
    purchases.add(Purchase.builder().user(zilia).payment(17.67).build());
    purchases.add(Purchase.builder().user(zilia).payment(12.58).build());
    purchases.add(Purchase.builder().user(zilia).payment(18.12).build());
    purchases.add(Purchase.builder().user(zilia).payment(11.55).build());
    purchases.add(Purchase.builder().user(zilia).payment(18.97).build());
    purchases.add(Purchase.builder().user(zilia).payment(17.17).build());
    purchases.add(Purchase.builder().user(zilia).payment(11.87).build());
    purchases.add(Purchase.builder().user(zilia).payment(20.95).build());
    purchases.add(Purchase.builder().user(zilia).payment(26.61).build());
    purchases.add(Purchase.builder().user(zilia).payment(15.52).build());
    purchases.add(Purchase.builder().user(zilia).payment(26.87).build());
    purchases.add(Purchase.builder().user(zilia).payment(8.28).build());
    purchases.add(Purchase.builder().user(zilia).payment(20.97).build());
    purchases.add(Purchase.builder().user(zilia).payment(21.13).build());
    purchases.add(Purchase.builder().user(zilia).payment(12.3).build());
    purchases.add(Purchase.builder().user(zilia).payment(8.19).build());
    purchases.add(Purchase.builder().user(zilia).payment(7.72).build());
    purchases.add(Purchase.builder().user(zilia).payment(22.32).build());
    purchases.add(Purchase.builder().user(zilia).payment(9.68).build());
    purchases.add(Purchase.builder().user(zilia).payment(24.41).build());
    purchases.add(Purchase.builder().user(zilia).payment(25.5).build());
    purchases.add(Purchase.builder().user(zilia).payment(20.83).build());
    purchases.add(Purchase.builder().user(zilia).payment(25.74).build());
    purchases.add(Purchase.builder().user(zilia).payment(14.85).build());
    purchases.add(Purchase.builder().user(zilia).payment(10.91).build());
    purchases.add(Purchase.builder().user(zilia).payment(10.26).build());
    purchases.add(Purchase.builder().user(zilia).payment(12.64).build());
    purchases.add(Purchase.builder().user(zilia).payment(20.11).build());
    purchases.add(Purchase.builder().user(zilia).payment(11.81).build());
    purchases.add(Purchase.builder().user(zilia).payment(26.6).build());
    purchases.add(Purchase.builder().user(zilia).payment(18.57).build());
    purchases.add(Purchase.builder().user(zilia).payment(7.4).build());
    purchases.add(Purchase.builder().user(zilia).payment(14.57).build());
    purchases.add(Purchase.builder().user(zilia).payment(21.99).build());
    purchases.add(Purchase.builder().user(zilia).payment(18.71).build());
    purchases.add(Purchase.builder().user(zilia).payment(20.32).build());
    purchases.add(Purchase.builder().user(zilia).payment(22.38).build());
    purchases.add(Purchase.builder().user(zilia).payment(13.97).build());
    purchases.add(Purchase.builder().user(zilia).payment(19.94).build());
    purchases.add(Purchase.builder().user(zilia).payment(15.57).build());
    purchases.add(Purchase.builder().user(zilia).payment(15.76).build());
    purchases.add(Purchase.builder().user(zilia).payment(19.12).build());
    purchases.add(Purchase.builder().user(zilia).payment(21.56).build());
    purchases.add(Purchase.builder().user(zilia).payment(18.96).build());
    purchases.add(Purchase.builder().user(zilia).payment(16.97).build());
    purchases.add(Purchase.builder().user(zilia).payment(10.18).build());
    purchases.add(Purchase.builder().user(zilia).payment(15.12).build());
    purchases.add(Purchase.builder().user(zilia).payment(7.24).build());
    purchases.add(Purchase.builder().user(zilia).payment(21.32).build());
    purchases.add(Purchase.builder().user(zilia).payment(18.83).build());
    purchases.add(Purchase.builder().user(zilia).payment(7.08).build());
    purchases.add(Purchase.builder().user(zilia).payment(18.01).build());
    purchases.add(Purchase.builder().user(zilia).payment(23.7).build());
    purchases.add(Purchase.builder().user(zilia).payment(19.84).build());
    purchases.add(Purchase.builder().user(zilia).payment(20.51).build());
    purchases.add(Purchase.builder().user(zilia).payment(21.57).build());
    purchases.add(Purchase.builder().user(zilia).payment(18.71).build());
    purchases.add(Purchase.builder().user(zilia).payment(14.68).build());
    purchases.add(Purchase.builder().user(zilia).payment(8.93).build());
    purchases.add(Purchase.builder().user(zilia).payment(24.24).build());
    purchases.add(Purchase.builder().user(zilia).payment(23.84).build());
    purchases.add(Purchase.builder().user(zilia).payment(13.28).build());
    purchases.add(Purchase.builder().user(zilia).payment(23.71).build());
    purchases.add(Purchase.builder().user(zilia).payment(21.69).build());
    purchases.add(Purchase.builder().user(zilia).payment(21.58).build());
    purchases.add(Purchase.builder().user(zilia).payment(7.16).build());
    purchases.add(Purchase.builder().user(zilia).payment(26.66).build());
    purchases.add(Purchase.builder().user(zilia).payment(25.63).build());
    purchases.add(Purchase.builder().user(zilia).payment(24.93).build());
    purchases.add(Purchase.builder().user(zilia).payment(24.34).build());
    purchases.add(Purchase.builder().user(zilia).payment(21.55).build());
    purchases.add(Purchase.builder().user(zilia).payment(14.94).build());
    purchases.add(Purchase.builder().user(zilia).payment(22.2).build());
    purchases.add(Purchase.builder().user(zilia).payment(15.82).build());
    purchases.add(Purchase.builder().user(zilia).payment(19.32).build());
    purchases.add(Purchase.builder().user(zilia).payment(23.01).build());
    purchases.add(Purchase.builder().user(zilia).payment(18.33).build());
    purchases.add(Purchase.builder().user(zilia).payment(12.82).build());
    purchases.add(Purchase.builder().user(zilia).payment(23.56).build());
    purchases.add(Purchase.builder().user(zilia).payment(20.35).build());
    purchases.add(Purchase.builder().user(zilia).payment(23.96).build());
    purchases.add(Purchase.builder().user(zilia).payment(16.46).build());
    purchases.add(Purchase.builder().user(zilia).payment(10.24).build());
    purchases.add(Purchase.builder().user(zilia).payment(17.34).build());
    purchases.add(Purchase.builder().user(zilia).payment(20.08).build());
    purchases.add(Purchase.builder().user(zilia).payment(23.31).build());
    purchases.add(Purchase.builder().user(zilia).payment(22.34).build());
    purchases.add(Purchase.builder().user(zilia).payment(10.77).build());
    purchases.add(Purchase.builder().user(zilia).payment(7.46).build());
    purchases.add(Purchase.builder().user(zilia).payment(18.82).build());
    purchases.add(Purchase.builder().user(zilia).payment(19.89).build());
    purchases.add(Purchase.builder().user(zilia).payment(14.66).build());
    purchases.add(Purchase.builder().user(zilia).payment(9.17).build());
    purchases.add(Purchase.builder().user(zilia).payment(23.7).build());
    purchases.add(Purchase.builder().user(zilia).payment(23.33).build());
    purchases.add(Purchase.builder().user(zilia).payment(20.51).build());
    purchases.add(Purchase.builder().user(zilia).payment(8.72).build());
    purchases.add(Purchase.builder().user(zilia).payment(8.77).build());
    purchases.add(Purchase.builder().user(zilia).payment(21.56).build());
    purchases.add(Purchase.builder().user(zilia).payment(25.64).build());
    purchases.add(Purchase.builder().user(zilia).payment(8.61).build());
    purchases.add(Purchase.builder().user(zilia).payment(19.66).build());
    purchases.add(Purchase.builder().user(zilia).payment(12.32).build());
    purchases.add(Purchase.builder().user(zilia).payment(19.57).build());
    purchases.add(Purchase.builder().user(zilia).payment(24.21).build());
    purchases.add(Purchase.builder().user(zilia).payment(11.86).build());
    purchases.add(Purchase.builder().user(zilia).payment(7.07).build());
    purchases.add(Purchase.builder().user(zilia).payment(15.37).build());
    purchases.add(Purchase.builder().user(zilia).payment(14.69).build());
    purchases.add(Purchase.builder().user(zilia).payment(8.68).build());
    purchases.add(Purchase.builder().user(zilia).payment(26.52).build());
    purchases.add(Purchase.builder().user(zilia).payment(25.17).build());
    purchases.add(Purchase.builder().user(zilia).payment(13.53).build());
    purchases.add(Purchase.builder().user(zilia).payment(17.39).build());
    purchases.add(Purchase.builder().user(zilia).payment(13.11).build());
    purchases.add(Purchase.builder().user(zilia).payment(8.45).build());
    purchases.add(Purchase.builder().user(zilia).payment(9.69).build());
    purchases.add(Purchase.builder().user(zilia).payment(22.81).build());
    purchases.add(Purchase.builder().user(zilia).payment(14.14).build());
    purchases.add(Purchase.builder().user(zilia).payment(18.39).build());
    purchases.add(Purchase.builder().user(zilia).payment(21.25).build());
    purchases.add(Purchase.builder().user(zilia).payment(9.57).build());
    purchases.add(Purchase.builder().user(zilia).payment(18.63).build());
    purchases.add(Purchase.builder().user(zilia).payment(21.66).build());
    purchases.add(Purchase.builder().user(zilia).payment(8.91).build());
    purchases.add(Purchase.builder().user(zilia).payment(18.66).build());
    purchases.add(Purchase.builder().user(zilia).payment(21.22).build());
    purchases.add(Purchase.builder().user(zilia).payment(7.24).build());
    purchases.add(Purchase.builder().user(zilia).payment(18.04).build());
    purchases.add(Purchase.builder().user(zilia).payment(20.21).build());
    purchases.add(Purchase.builder().user(zilia).payment(14.92).build());
    purchases.add(Purchase.builder().user(zilia).payment(11.57).build());
    purchases.add(Purchase.builder().user(zilia).payment(13.32).build());
    purchases.add(Purchase.builder().user(zilia).payment(12.75).build());
    purchases.add(Purchase.builder().user(zilia).payment(8.52).build());
    purchases.add(Purchase.builder().user(zilia).payment(10.88).build());
    purchases.add(Purchase.builder().user(zilia).payment(11.41).build());
    purchases.add(Purchase.builder().user(zilia).payment(11.67).build());
    purchases.add(Purchase.builder().user(zilia).payment(9.77).build());
    purchases.add(Purchase.builder().user(zilia).payment(23.97).build());
    purchases.add(Purchase.builder().user(zilia).payment(16.5).build());
    purchases.add(Purchase.builder().user(zilia).payment(25.82).build());
    purchases.add(Purchase.builder().user(zilia).payment(13.02).build());
    purchases.add(Purchase.builder().user(zilia).payment(20.78).build());
    purchases.add(Purchase.builder().user(zilia).payment(7.28).build());
    purchases.add(Purchase.builder().user(zilia).payment(9.12).build());
    purchases.add(Purchase.builder().user(zilia).payment(17.72).build());
    purchases.add(Purchase.builder().user(zilia).payment(13.41).build());
    purchases.add(Purchase.builder().user(zilia).payment(21.89).build());
    purchases.add(Purchase.builder().user(zilia).payment(17.69).build());
    purchases.add(Purchase.builder().user(zilia).payment(19.17).build());
    purchases.add(Purchase.builder().user(zilia).payment(21.28).build());
    purchases.add(Purchase.builder().user(zilia).payment(11.3).build());
    purchases.add(Purchase.builder().user(zilia).payment(16.14).build());
    purchases.add(Purchase.builder().user(zilia).payment(23.41).build());
    purchases.add(Purchase.builder().user(zilia).payment(20.28).build());
    purchases.add(Purchase.builder().user(zilia).payment(18.18).build());
    purchases.add(Purchase.builder().user(zilia).payment(13.03).build());
    purchases.add(Purchase.builder().user(zilia).payment(18.18).build());
    purchases.add(Purchase.builder().user(zilia).payment(20.52).build());
    purchases.add(Purchase.builder().user(zilia).payment(7).build());
    purchases.add(Purchase.builder().user(zilia).payment(13.15).build());
    purchases.add(Purchase.builder().user(zilia).payment(20.4).build());
    purchases.add(Purchase.builder().user(zilia).payment(9.56).build());
    purchases.add(Purchase.builder().user(zilia).payment(24.64).build());
    purchases.add(Purchase.builder().user(zilia).payment(12.45).build());
    purchases.add(Purchase.builder().user(zilia).payment(10.29).build());
    purchases.add(Purchase.builder().user(zilia).payment(9.12).build());
    purchases.add(Purchase.builder().user(zilia).payment(11.31).build());
    purchases.add(Purchase.builder().user(zilia).payment(19.49).build());
    purchases.add(Purchase.builder().user(zilia).payment(20.07).build());
    purchases.add(Purchase.builder().user(zilia).payment(9.75).build());
    purchases.add(Purchase.builder().user(zilia).payment(10.5).build());
    purchases.add(Purchase.builder().user(zilia).payment(22.91).build());
    purchases.add(Purchase.builder().user(zilia).payment(12.36).build());
    purchases.add(Purchase.builder().user(zilia).payment(21.65).build());
    purchases.add(Purchase.builder().user(zilia).payment(15.2).build());
    purchases.add(Purchase.builder().user(zilia).payment(17.63).build());
    purchases.add(Purchase.builder().user(zilia).payment(24.69).build());
    purchases.add(Purchase.builder().user(zilia).payment(15.89).build());
    purchases.add(Purchase.builder().user(zilia).payment(13.75).build());
    purchases.add(Purchase.builder().user(zilia).payment(8.01).build());
    purchases.add(Purchase.builder().user(zilia).payment(20.81).build());
    purchases.add(Purchase.builder().user(zilia).payment(23.04).build());
    purchases.add(Purchase.builder().user(zilia).payment(26.16).build());
    purchases.add(Purchase.builder().user(zilia).payment(12.04).build());
    purchases.add(Purchase.builder().user(zilia).payment(18.43).build());
    purchases.add(Purchase.builder().user(zilia).payment(26.71).build());
    purchases.add(Purchase.builder().user(zilia).payment(26.86).build());
    purchases.add(Purchase.builder().user(zilia).payment(19.72).build());
    purchases.add(Purchase.builder().user(zilia).payment(9.51).build());
    purchases.add(Purchase.builder().user(zilia).payment(24.09).build());

    purchaseService.addPurchases(purchases);
  }



/*  @Test
  public void getRecipe() {
    Map<Recipe, Set<Ingredient>> recipes = new HashMap<>();
    recipeRepository.findAllByName("Gulasch").forEach(recipe -> {
      recipes.put(recipe, recipe.getIngredients());
    });

    System.out.println(recipes);
  }

  @Test
  public void updateRecipeRemoveIngredient() {
    Recipe recipe = recipeRepository.findById(uuidConvertService.getUuid("f0aed973-b576-4cf0-ab8f-ce2d76588251")).get();
    RecipeDto recipeDto = recipeAssembler.assemble(recipe);

    Set<IngredientDto> ingredientDtos = new HashSet<>();
    ingredientRepository.findAllByRecipes(recipe).forEach(ingredient -> {
      ingredientDtos.add(ingredientAssembler.assemble(ingredient));
    });

    ingredientDtos.removeIf(ingredientDto -> ingredientDto.getName().equals("Zwiebel"));

    recipeDto.setIngredientDtos(ingredientDtos);

    System.out.println(recipeService.updateRecipe("f0aed973-b576-4cf0-ab8f-ce2d76588251", recipeDto));
  }

  @Test
  public void updateRecipeAddIngredientExisting() {
    Recipe recipe = recipeRepository.findById(uuidConvertService.getUuid("f0aed973-b576-4cf0-ab8f-ce2d76588251")).get();
    RecipeDto recipeDto = recipeAssembler.assemble(recipe);

    Set<IngredientDto> ingredientDtos = new HashSet<>();
    ingredientRepository.findAllByRecipes(recipe).forEach(ingredient -> {
      ingredientDtos.add(ingredientAssembler.assemble(ingredient));
    });

    ingredientDtos.removeIf(ingredientDto -> ingredientDto.getName().equals("Zwiebel"));

    Unit unit = unitRepository.findByName("Gramm");

    ingredientDtos.add(IngredientDto.builder().name("Kümmel").amount(1).unit(unitAssembler.assemble(unit)).build());

    recipeDto.setIngredientDtos(ingredientDtos);

    System.out.println(recipeService.updateRecipe("f0aed973-b576-4cf0-ab8f-ce2d76588251", recipeDto));
  }

  @Test
  public void updateRecipeAddIngredientNonExistingRemoveIngredient() {
    Unit unit = unitRepository.findByName("Gramm");
    IngredientDto ingredientDto = IngredientDto.builder()
        .name("Kümmel")
        .unit(unitAssembler.assemble(unit))
        .amount(1)
        .build();

    Ingredient ingredient = Ingredient.builder()
        .name(ingredientDto.getName())
        .amount(ingredientDto.getAmount())
        .unit(unitRepository.findByName(ingredientDto.getUnitDto().getName()))
        .build();

    ingredientRepository.save(ingredient);
  }

  @Test
  public void testAddRecipeService() {
    RecipeDto recipeDto = RecipeDto.builder()
        .name("Chefsalat")
        .user(userAssembler.assemble(userRepository.findByUsername("michael")))
        .build();

    recipeService.addRecipe(recipeDto);
  }*/

}
