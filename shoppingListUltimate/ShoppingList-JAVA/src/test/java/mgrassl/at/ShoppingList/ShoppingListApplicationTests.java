package mgrassl.at.ShoppingList;

import mgrassl.at.ShoppingList.assembler.GroupAssembler;
import mgrassl.at.ShoppingList.assembler.UserAssembler;
import mgrassl.at.ShoppingList.domain.entity.*;
import mgrassl.at.ShoppingList.dto.ArticleDto;
import mgrassl.at.ShoppingList.dto.GroupDto;
import mgrassl.at.ShoppingList.dto.UserDto;
import mgrassl.at.ShoppingList.dto.UserGroupDto;
import mgrassl.at.ShoppingList.repository.*;
import mgrassl.at.ShoppingList.service.ArticleService;
import mgrassl.at.ShoppingList.service.GroupService;
import mgrassl.at.ShoppingList.service.UserGroupService;
import mgrassl.at.ShoppingList.service.UserService;
import mgrassl.at.ShoppingList.utility.UuidConvertService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.*;


@RunWith(SpringRunner.class)
@SpringBootTest
public class ShoppingListApplicationTests {

  @Autowired
  private ArticleRepository articleRepository;
  @Autowired
  private UserRepository userRepository;
  @Autowired
  private UserService userService;
  @Autowired
  private ArticleService articleService;
  @Autowired
  private UuidConvertService uuidConvertService;
  @Autowired
  private PasswordEncoder passwordEncoder;
  @Autowired
  private UnitRepository unitRepository;
  @Autowired
  private UserGroupRepository userGroupRepository;
  @Autowired
  private GroupRepository groupRepository;
  @Autowired
  private GroupAssembler groupAssembler;
  @Autowired
  private UserAssembler userAssembler;
  @Autowired
  private GroupService groupService;
  @Autowired
  private UserGroupService userGroupService;
  @Autowired
  private RecipeRepository recipeRepository;

  @Test
  public void contextLoads() {
  }

  @Test
  public void testArticle() {
    Article article1 = Article.builder().articleName("mitGUID_Identity").amount(2).build();
    articleRepository.save(article1);

    articleRepository.findAll().forEach(article -> System.out.println(article));

  }

  @Test
  public void testUser() {
    User user = User.builder()
        .username("michael")
        .password(passwordEncoder.encode("Nguyenr#12"))
        .isAdmin(true)
        .isActive(true)
        .build();

    userRepository.save(user);
  }

/*  @Test
  public void updateUser() {
    User user2 = User.builder()
        .username("zilia")
        .isAdmin(false)
        .isActive(true)
        .build();

    userService.updateUser(44, user2);
  }*/

  @Test
  public void testTimeStamp() {

    System.out.println(java.time.Instant.now());
  }

  @Test
  public void testDeleteDB() {

    articleRepository.deleteAll();

    articleRepository.findAll().forEach(article -> System.out.println(article));
  }

  @Test
  public void passwordEncode() {
    BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
    System.out.println(bCryptPasswordEncoder.encode("abcdefghijklmnopqrstuvwxyz"));
  }

  @Test
  public void addArticle() {
    ArticleDto articleDto = ArticleDto.builder()
        .name("Haselnüsse")
        .amount(10)
        .isBought(false)
        .build();

    this.articleService.addArticle(articleDto);
  }

  @Test
  public void uuid() {
    UUID uuid = UUID.randomUUID();
    String string = uuidConvertService.getStringFromUuid(uuid);
    System.out.println("String: " + string);
    System.out.println(uuidConvertService.getUuid(string));
  }

  @Test
  public void articleWithUser() {
    User user = User.builder()
        .username("michael")
        .isActive(true)
        .isAdmin(true)
        .build();

    Article article = Article.builder()
        .user(userRepository.findByUsername("michael"))
        .articleName("Banane")
        .amount(10)
        .isBought(false)
        .unit(unitRepository.findByName("Packung"))
        .build();
    Article article2 = Article.builder()
        .user(user)
        .articleName("Banane")
        .amount(10)
        .isBought(false)
        .build();

    //userRepository.save(user);
    articleRepository.save(article);
    //articleRepository.save(article2);
  }

  @Test
  public void testDeleteArticle() {
    articleRepository.deleteAll();
  }

  @Test
  public void testArticleNameList() {

/*    LocalDate from = LocalDate.parse("2015-05-01");
    LocalDate to = LocalDate.now();

    long days = ChronoUnit.DAYS.between(from, to);    // 6 days
    long weeks = ChronoUnit.WEEKS.between(from, to);  // 0 weeks

*/

    articleRepository.findAll().forEach(article -> {

      LocalDate from = LocalDate.parse(article.getCreateDate().toString().substring(0, 10));
      LocalDate to = LocalDate.now().plusDays(7);

      System.out.println(ChronoUnit.DAYS.between(from, to));
    });
  }

  @Test
  public void testDeleteUser() {
    userRepository.delete(userRepository.findByUsername("zilia"));
  }

  @Test
  public void testNewDateToBuy() {
    User michael = userRepository.findByUsername("michael");
    Unit stk = unitRepository.findByName("Stück");
    Date today = new Date(System.currentTimeMillis());

    Article article = Article.builder()
        .user(michael)
        .unit(stk)
        .isBought(false)
        .amount(5)
        .articleName("Zucchini")
        .dateToBuy(today)
        .build();

    articleRepository.save(article);
  }

  @Test
  public void getGroupsFromUser() {
    User user = userRepository.findByUsername("michael");

    Set<UserGroup> userGroups = userGroupRepository.findAllByUser(user);

    userGroups.forEach(userGroup -> {
      System.out.println(userGroup.getGroup());
    });
  }

  @Test
  public void getUsersFromGroup() {
    Group group = groupRepository.findAll().get(0);

    Set<UserGroup> userGroups = userGroupRepository.findAllByGroup(group);

    userGroups.forEach(userGroup -> {
      System.out.println(userGroup.getUser());
    });
  }

  @Test
  public void testAddDuplicateEntry() {
    Group zweierlei = groupRepository.findAll().get(0);

    User michael = userRepository.findByUsername("michael");

    UserGroup userGroup = UserGroup.builder()
        .group(zweierlei)
        .user(michael)
        .isGroupAdmin(true)
        .build();

    zweierlei.getGroupUsers().add(userGroup);

    groupRepository.save(zweierlei);
  }

  @Test
  public void removeUserFromGroup() {
    GroupDto zweierlei = groupAssembler.assemble(groupRepository.findAll().get(0));
    UserDto michael = userAssembler.assemble(userRepository.findByUsername("michael"));

    userGroupService.removeUserFromGroup(michael, zweierlei);
  }

  @Test
  public void addUserToGroup() {
    GroupDto zweierlei = groupAssembler.assemble(groupRepository.findAll().get(0));
    UserDto michael = userAssembler.assemble(userRepository.findByUsername("michael"));

    userGroupService.addUserToGroup(michael, zweierlei, true);
  }

  @Test
  public void getUserGroups() {
    UserDto michael = userAssembler.assemble(userRepository.findByUsername("michael"));
    System.out.println(userGroupService.getUserGroups(michael));
  }

  @Test
  public void getGroupUsers() {
    GroupDto zweierlei = groupAssembler.assemble(groupRepository.findAll().get(0));
    System.out.println(userGroupService.getGroupUsers(zweierlei));
  }

  @Test
  public void setUserGroupAdmin() {
    UserDto michael = userAssembler.assemble(userRepository.findByUsername("michael"));
    GroupDto zweierlei = groupAssembler.assemble(groupRepository.findAll().get(0));

    UserGroupDto userGroupDto = UserGroupDto.builder()
        .userDto(michael)
        .groupDto(zweierlei)
        .isGroupAdmin(false)
        .build();

    userGroupService.updateUserInGroup(userGroupDto);
  }

  @Test
  public void addIngredientsToArticle() {
    List<Recipe> recipes = recipeRepository.findAll();


  }
}
