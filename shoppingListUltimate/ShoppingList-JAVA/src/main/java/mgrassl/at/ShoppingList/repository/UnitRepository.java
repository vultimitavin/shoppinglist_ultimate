package mgrassl.at.ShoppingList.repository;

import mgrassl.at.ShoppingList.domain.entity.Unit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface UnitRepository extends JpaRepository<Unit, UUID> {

  Unit findByName(String name);
}
