package mgrassl.at.ShoppingList.dto;

import mgrassl.at.ShoppingList.domain.entity.UserGroup;
import java.util.Objects;
import java.util.StringJoiner;

public class UserGroupDto {

  private UserDto userDto;
  private GroupDto groupDto;
  private boolean isGroupAdmin;


  public UserGroup asUserGroupEntity() {
    return UserGroup.builder()
        .user(getUserDto().asUserEntity())
        .group(getGroupDto().asGroupEntity())
        .isGroupAdmin(isGroupAdmin)
        .build();
  }

  public static UserGroupDtoBuilder builder() {
    return new UserGroupDtoBuilder();
  }

  public static class UserGroupDtoBuilder {
    private final UserGroupDto userGroupDto = new UserGroupDto();

    public UserGroupDtoBuilder userDto(UserDto userDto) {
      userGroupDto.userDto = userDto;
      return this;
    }

    public UserGroupDtoBuilder groupDto(GroupDto groupDto) {
      userGroupDto.groupDto = groupDto;
      return this;
    }

    public UserGroupDtoBuilder isGroupAdmin(boolean isGroupAdmin) {
      userGroupDto.isGroupAdmin = isGroupAdmin;
      return this;
    }

    public UserGroupDto build() {
      return userGroupDto;
    }
  }

  public UserDto getUserDto() {
    return userDto;
  }

  public void setUserDto(final UserDto userDto) {
    this.userDto = userDto;
  }

  public GroupDto getGroupDto() {
    return groupDto;
  }

  public void setGroupDto(final GroupDto groupDto) {
    this.groupDto = groupDto;
  }

  public boolean isGroupAdmin() {
    return isGroupAdmin;
  }

  public void setGroupAdmin(final boolean groupAdmin) {
    isGroupAdmin = groupAdmin;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UserGroupDto that = (UserGroupDto) o;
    return userDto.equals(that.userDto) &&
        groupDto.equals(that.groupDto);
  }

  @Override
  public int hashCode() {
    return Objects.hash(userDto, groupDto);
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", "[", "]")
        .add("userDto=" + userDto)
        .add("groupDto=" + groupDto)
        .add("isGroupAdmin=" + isGroupAdmin)
        .toString();
  }
}
