package mgrassl.at.ShoppingList.service;

import mgrassl.at.ShoppingList.assembler.ArticleAssembler;
import mgrassl.at.ShoppingList.domain.entity.Article;
import mgrassl.at.ShoppingList.domain.entity.Unit;
import mgrassl.at.ShoppingList.domain.entity.User;
import mgrassl.at.ShoppingList.dto.ArticleDto;
import mgrassl.at.ShoppingList.repository.ArticleRepository;
import mgrassl.at.ShoppingList.repository.UnitRepository;
import mgrassl.at.ShoppingList.repository.UserRepository;
import mgrassl.at.ShoppingList.utility.UuidConvertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.*;

@Service
public class ArticleService {

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private ArticleRepository articleRepository;

  @Autowired
  private ArticleAssembler articleAssembler;

  @Autowired
  private UuidConvertService uuidConvertService;

  @Autowired
  private UnitRepository unitRepository;

  public ArticleDto getArticleById(String uuid) {

    return articleAssembler.assemble(
        articleRepository.findById(uuidConvertService.getUuid(uuid)).get()
    );
  }

  public List<ArticleDto> getArticleList() {
    // Get System Date
    LocalDate to = LocalDate.now();


    List<ArticleDto> articleDtos = new ArrayList<>();

    articleRepository.findAll().forEach(article -> {

      // Get the createDate in format for LocalDate
      LocalDate from = LocalDate.parse(article.getCreateDate().toString().substring(0, 10));

      //System.out.println(from + " " + to);

      // When SystemDate - CreateDate > 7 then dont pass it along to the view
      if (ChronoUnit.DAYS.between(from, to) < 8) {
        articleDtos.add(articleAssembler.assemble(article));
      }

    });

    return articleDtos;
  }

  public HashSet<String> getArticleNameList() {

    List<Article> articleList = articleRepository.findAll();

    HashSet<String> articleNameList = new HashSet<>();

    articleList.forEach(article -> {
      articleNameList.add(article.getName());
    });

    return articleNameList;
  }

  public ArticleDto addArticle(ArticleDto articleDto) {

    User user = userRepository.findById(uuidConvertService.getUuid(articleDto.getUserUuid())).get();
    Unit unit = unitRepository.findById(uuidConvertService.getUuid(articleDto.getUnitUuid())).get();


    return articleAssembler.assemble(
        articleRepository.save(articleDto.asArticleEntity(user, unit))
    );
  }

  public List<ArticleDto> addArticleList(Iterable<ArticleDto> articleDtos) {

    List<Article> articleList = new ArrayList<>();

    Map<String, User> userMap = new HashMap<>();
    Map<String, Unit> unitMap = new HashMap<>();

    // Map all Users to their id as String
    userRepository.findAll().forEach(user -> {
      userMap.put(uuidConvertService.getStringFromUuid(user.getId()), user);
    });

    // Map all Units to their id as String
    unitRepository.findAll().forEach(unit -> {
      unitMap.put(uuidConvertService.getStringFromUuid(unit.getId()), unit);
    });

    // Get all ArticleDtos als Articles
    articleDtos.forEach(articleDto -> {
      User user = userMap.get(articleDto.getUserUuid());
      Unit unit = unitMap.get(articleDto.getUnitUuid());

      articleList.add(articleDto.asArticleEntity(user, unit));
    });

    // Save all Articles
    return articleAssembler.assemble(
        articleRepository.saveAll(articleList)
    );
  }

  public ArticleDto updateArticle(String uuid, ArticleDto articleDto) {
    Article articleToUpdate = articleRepository.findById(uuidConvertService.getUuid(uuid)).get();

    articleToUpdate.setName(articleDto.getName());
    articleToUpdate.setAmount(articleDto.getAmount());
    articleToUpdate.setBought(articleDto.isBought());
    articleToUpdate.setDateToBuy(articleDto.getDateToBuy());

    return articleAssembler.assemble(
        articleRepository.save(articleToUpdate)
    );
  }

  public void removeArticle(String  uuid) {
    articleRepository.delete(articleRepository.findById(uuidConvertService.getUuid(uuid)).get());
  }
}
