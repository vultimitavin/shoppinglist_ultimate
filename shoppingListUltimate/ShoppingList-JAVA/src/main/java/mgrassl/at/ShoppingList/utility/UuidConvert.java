package mgrassl.at.ShoppingList.utility;

import java.util.UUID;

public interface UuidConvert {

  UUID getUuid(final String string);

  String getStringFromUuid(final UUID uuid);
}
