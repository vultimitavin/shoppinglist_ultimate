package mgrassl.at.ShoppingList.dto;

import mgrassl.at.ShoppingList.domain.entity.Group;
import mgrassl.at.ShoppingList.domain.entity.User;
import mgrassl.at.ShoppingList.domain.entity.UserGroup;
import mgrassl.at.ShoppingList.repository.UserGroupRepository;
import mgrassl.at.ShoppingList.service.UserGroupService;
import mgrassl.at.ShoppingList.service.UserService;
import mgrassl.at.ShoppingList.utility.UuidConvert;
import mgrassl.at.ShoppingList.utility.UuidConvertService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;

public class GroupDto {

  @Autowired
  private UserGroupRepository userGroupRepository;
  @Autowired
  private UuidConvertService uuidConvertService;

  private String uuid;
  private String name;
  private Set<UserDto> userDtos;
  private Date createDate;
  private Date modifyDate;

  public Group asGroupEntity() {
    return Group.builder()
        .name(getName())
        .groupUsers(userGroupRepository.findById_GroupId(uuidConvertService.getUuid(getUuid())))
        .build();
  }

  public static GroupDtoBuilder builder() {
    return new GroupDtoBuilder();
  }

  public static class GroupDtoBuilder {

    private final GroupDto groupDto = new GroupDto();

    public GroupDtoBuilder name(String name) {
      groupDto.name = name;
      return this;
    }

    public GroupDtoBuilder uuid(String uuid) {
      groupDto.uuid = uuid;
      return this;
    }

    public GroupDtoBuilder users(Set<UserDto> userDtos) {
      groupDto.userDtos = userDtos;
      return this;
    }

    public GroupDtoBuilder createDate(Date createDate) {
      groupDto.createDate = createDate;
      return this;
    }

    public GroupDtoBuilder modifyDate(Date modifyDate) {
      groupDto.modifyDate = modifyDate;
      return this;
    }

    public GroupDto build() {
      return groupDto;
    }

  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public String getUuid() {
    return uuid;
  }

  public Date getCreateDate() {
    return createDate;
  }

  public Date getModifyDate() {
    return modifyDate;
  }

  public void setUuid(final String uuid) {
    this.uuid = uuid;
  }

  public Set<UserDto> getUserDtos() {
    return userDtos;
  }

  public void setUserDtos(final Set<UserDto> userDtos) {
    this.userDtos = userDtos;
  }

  public void setCreateDate(final Date createDate) {
    this.createDate = createDate;
  }

  public void setModifyDate(final Date modifyDate) {
    this.modifyDate = modifyDate;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GroupDto groupDto = (GroupDto) o;
    return name.equals(groupDto.name) &&
        Objects.equals(userDtos, groupDto.userDtos);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, userDtos);
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", "[", "]")
        .add("uuid=" + uuid)
        .add("name=" + name)
        .add("userDtos=" + userDtos)
        .add("createDate=" + createDate)
        .add("modifyDate=" + modifyDate)
        .toString();
  }
}
