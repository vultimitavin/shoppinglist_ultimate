package mgrassl.at.ShoppingList.api;

import mgrassl.at.ShoppingList.dto.UnitDto;
import mgrassl.at.ShoppingList.service.UnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/unit")
public class UnitRestApi {

  @Autowired
  private UnitService unitService;

  @GetMapping
  public List<UnitDto> getUnitList() {
    return unitService.getUnitList();
  }

  @GetMapping("{unitname}")
  public UnitDto getUnitByUnitname(@PathVariable("unitname") String unitname) {
    return unitService.getUnitbyName(unitname);
  }

  // TODO: User shouldnt be able to add Units because of ingredients
  @PostMapping
  public UnitDto addUnit(@Valid @RequestBody UnitDto unitDto) {
    return unitService.addUnit(unitDto);
  }

  @PostMapping("/list")
  public List<UnitDto> addUnitList(@Valid @RequestBody Iterable<UnitDto> unitDtos) {
    return unitService.addUnitList(unitDtos);
  }
}
