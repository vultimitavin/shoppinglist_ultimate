package mgrassl.at.ShoppingList.model;

import java.io.Serializable;

public class JwtRequest implements Serializable {

  private static final long SERIAL_VERSION_UID = 5926468583005150707L;

  private String username;
  private String password;

  //need default constructor for JSON Parsing
  public JwtRequest() {

  }

  public JwtRequest(final String username, final String password) {
    this.username = username;
    this.password = password;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(final String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(final String password) {
    this.password = password;
  }


}
