package mgrassl.at.ShoppingList.assembler;

import mgrassl.at.ShoppingList.domain.entity.User;
import mgrassl.at.ShoppingList.dto.UserDto;
import mgrassl.at.ShoppingList.utility.UuidConvertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserAssembler extends AbstractAssembler<User, UserDto> {

  @Autowired
  private UuidConvertService uuidConvertService;

  @Override
  public UserDto assemble(final User user) {
    if (user == null) {
      return null;
    }
    return UserDto
        .builder()
        .uuid(uuidConvertService.getStringFromUuid(user.getId()))
        .username(user.getUsername())
        .isActive(user.isActive())
        .isAdmin(user.isAdmin())
        .createDate(user.getCreateDate())
        .modifyDate(user.getModifyDate())
        .build();
  }

}
