package mgrassl.at.ShoppingList.domain.entity;

import com.sun.org.apache.xml.internal.dtm.ref.sax2dtm.SAX2DTM2;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "ingredients")
public class Ingredient {

  @Id
  @GeneratedValue
  @Column(name = "Id", columnDefinition = "uniqueidentifier")
  private UUID id;

  @Column(name = "Name", columnDefinition = "nvarchar(50)", nullable = false)
  private String name;

  @Column(name = "Amount")
  private double amount;

  @ManyToOne
  @JoinColumn(name = "unit_id", nullable = false)
  private Unit unit;

  @ManyToMany(mappedBy = "ingredients")
  private Set<Recipe> recipes;

  @CreationTimestamp
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "create_date")
  private Date createDate;

  @UpdateTimestamp
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "modify_date")
  private Date modifyDate;

  public static IngredientBuilder builder() {
    return new IngredientBuilder();
  }

  public static class IngredientBuilder {
    private final Ingredient ingredient = new Ingredient();

    public IngredientBuilder name(String name) {
      ingredient.name = name;
      return this;
    }

    public IngredientBuilder amount(double amount) {
      ingredient.amount = amount;
      return this;
    }

    public IngredientBuilder unit(Unit unit) {
      ingredient.unit = unit;
      return this;
    }

    public Ingredient build() {
      return ingredient;
    }
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public double getAmount() {
    return amount;
  }

  public void setAmount(final double amount) {
    this.amount = amount;
  }

  public Unit getUnit() {
    return unit;
  }

  public void setUnit(final Unit unit) {
    this.unit = unit;
  }

  public UUID getId() {
    return id;
  }

  public Date getCreateDate() {
    return createDate;
  }

  public Date getModifyDate() {
    return modifyDate;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Ingredient that = (Ingredient) o;
    return amount == that.amount &&
        name.equals(that.name) &&
        unit.equals(that.unit);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, amount, unit);
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", "[", "]")
        .add("id=" + id)
        .add("name=" + name)
        .add("amount=" + amount)
        .add("unit=" + unit)
        .add("createDate=" + createDate)
        .add("modifyDate=" + modifyDate)
        .toString();
  }
}
