package mgrassl.at.ShoppingList.dto;

import mgrassl.at.ShoppingList.domain.entity.Ingredient;
import mgrassl.at.ShoppingList.domain.entity.Unit;
import mgrassl.at.ShoppingList.repository.UnitRepository;
import mgrassl.at.ShoppingList.utility.UuidConvertService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;

public class IngredientDto {

  @Autowired
  private UnitRepository unitRepository;
  @Autowired
  private UuidConvertService uuidConvertService;

  private String uuid;
  private String name;
  private double amount;
  private UnitDto unitDto;
  private Date modifyDate;
  private Date createDate;

  public Ingredient asIngredientEntity() {
    return Ingredient.builder()
        .name(getName())
        .amount(getAmount())
        .unit(getUnitDto().asUnitEntity())
        .build();
  }

  public static IngredientDtoBuilder builder() {
    return new IngredientDtoBuilder();
  }

  public static class IngredientDtoBuilder {
    private final IngredientDto ingredientDto = new IngredientDto();

    public IngredientDtoBuilder uuid(String uuid) {
      ingredientDto.uuid = uuid;
      return this;
    }

    public IngredientDtoBuilder name(String name) {
      ingredientDto.name = name;
      return this;
    }

    public IngredientDtoBuilder amount(double amount) {
      ingredientDto.amount = amount;
      return this;
    }

    public IngredientDtoBuilder unit(UnitDto unitDto) {
      ingredientDto.unitDto = unitDto;
      return this;
    }

    public IngredientDtoBuilder modifyDate(Date modifyDate) {
      ingredientDto.modifyDate = modifyDate;
      return this;
    }

    public IngredientDtoBuilder createDate(Date createDate) {
      ingredientDto.createDate = createDate;
      return this;
    }

    public IngredientDto build() {
      return ingredientDto;
    }
  }

  public String getUuid() {
    return uuid;
  }

  public void setUuid(final String uuid) {
    this.uuid = uuid;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public double getAmount() {
    return amount;
  }

  public void setAmount(final double amount) {
    this.amount = amount;
  }

  public UnitDto getUnitDto() {
    return unitDto;
  }

  public void setUnitDto(final UnitDto unitDto) {
    this.unitDto = unitDto;
  }

  public Date getModifyDate() {
    return modifyDate;
  }

  public void setModifyDate(final Date modifyDate) {
    this.modifyDate = modifyDate;
  }

  public Date getCreateDate() {
    return createDate;
  }

  public void setCreateDate(final Date createDate) {
    this.createDate = createDate;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    IngredientDto that = (IngredientDto) o;
    return Double.compare(that.amount, amount) == 0 &&
        name.equals(that.name) &&
        unitDto.equals(that.unitDto);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, amount, unitDto);
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", "[", "]")
        .add("uuid=" + uuid)
        .add("name=" + name)
        .add("amount=" + amount)
        .add("unitDto=" + unitDto)
        .add("modifyDate=" + modifyDate)
        .add("createDate=" + createDate)
        .toString();
  }
}
