package mgrassl.at.ShoppingList.assembler;

import mgrassl.at.ShoppingList.domain.entity.Purchase;
import mgrassl.at.ShoppingList.dto.PurchaseDto;
import mgrassl.at.ShoppingList.utility.UuidConvertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PurchaseAssembler extends AbstractAssembler<Purchase, PurchaseDto> {

  @Autowired
  private UuidConvertService uuidConvertService;

  @Override
  public PurchaseDto assemble(final Purchase purchase) {
    return PurchaseDto.builder()
        .uuid(uuidConvertService.getStringFromUuid(purchase.getId()))
        .userUuid(uuidConvertService.getStringFromUuid(purchase.getUser().getId()))
        .payment(purchase.getPayment())
        .purchaseDate(purchase.getPurchaseDate())
        .build();
  }
}
