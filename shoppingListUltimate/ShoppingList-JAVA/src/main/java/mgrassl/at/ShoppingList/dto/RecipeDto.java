package mgrassl.at.ShoppingList.dto;

import mgrassl.at.ShoppingList.assembler.UserAssembler;
import mgrassl.at.ShoppingList.domain.entity.Ingredient;
import mgrassl.at.ShoppingList.domain.entity.Recipe;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;

public class RecipeDto {

  private String uuid;
  private String name;
  private UserDto userDto;
  private Set<IngredientDto> ingredientDtos;
  private Date modifyDate;
  private Date createDate;

  public Recipe asRecipeEntity() {
    Set<Ingredient> ingredients = new HashSet<>();

    if (getIngredientDtos() != null) {
      getIngredientDtos().forEach(ingredientDto -> {
        ingredients.add(ingredientDto.asIngredientEntity());
      });
    }

    return Recipe.builder()
        .name(getName())
        .user(getUserDto().asUserEntity())
        .ingredients(ingredients)
        .build();
  }

  public static RecipeDtoBuilder builder() {
    return new RecipeDtoBuilder();
  }

  public static class RecipeDtoBuilder {
    private final RecipeDto recipeDto = new RecipeDto();

    public RecipeDtoBuilder uuid(String uuid) {
      recipeDto.uuid = uuid;
      return this;
    }

    public RecipeDtoBuilder name(String name) {
      recipeDto.name = name;
      return this;
    }

    public RecipeDtoBuilder user(UserDto userDto) {
      recipeDto.userDto = userDto;
      return this;
    }

    public RecipeDtoBuilder ingredients(Set<IngredientDto> ingredientDtos) {
      recipeDto.ingredientDtos = ingredientDtos;
      return this;
    }

    public RecipeDtoBuilder createDate(Date createDate) {
      recipeDto.createDate = createDate;
      return this;
    }

    public RecipeDtoBuilder modifyDate(Date modifyDate) {
      recipeDto.modifyDate = modifyDate;
      return this;
    }

    public RecipeDto build() {
      return recipeDto;
    }
  }

  public String getUuid() {
    return uuid;
  }

  public void setUuid(final String uuid) {
    this.uuid = uuid;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public UserDto getUserDto() {
    return userDto;
  }

  public void setUserDto(final UserDto userDto) {
    this.userDto = userDto;
  }

  public Set<IngredientDto> getIngredientDtos() {
    return ingredientDtos;
  }

  public void setIngredientDtos(final Set<IngredientDto> ingredientDtos) {
    this.ingredientDtos = ingredientDtos;
  }

  public Date getModifyDate() {
    return modifyDate;
  }

  public void setModifyDate(final Date modifyDate) {
    this.modifyDate = modifyDate;
  }

  public Date getCreateDate() {
    return createDate;
  }

  public void setCreateDate(final Date createDate) {
    this.createDate = createDate;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    RecipeDto recipeDto = (RecipeDto) o;
    return uuid.equals(recipeDto.uuid) &&
        name.equals(recipeDto.name) &&
        userDto.equals(recipeDto.userDto);
  }

  @Override
  public int hashCode() {
    return Objects.hash(uuid, name, userDto);
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", "[", "]")
        .add("uuid=" + uuid)
        .add("name=" + name)
        .add("userDto=" + userDto)
        .add("ingredientDtos=" + ingredientDtos)
        .add("modifyDate=" + modifyDate)
        .add("createDate=" + createDate)
        .toString();
  }
}
