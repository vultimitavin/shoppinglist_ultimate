package mgrassl.at.ShoppingList.dto;

import mgrassl.at.ShoppingList.domain.entity.User;
import java.util.Date;
import java.util.Objects;
import java.util.StringJoiner;

public class UserDto {

  private String uuid;
  private String username;
  private boolean isActive;
  private boolean isAdmin;
  private Date createDate;
  private Date modifyDate;

  public User asUserEntity() {
    return User.builder()
        .username(getUsername())
        .isActive(isActive())
        .isAdmin(isAdmin())
        .build();
  }

  public UserDto() {
  }

  public static UserDtoBuilder builder() { return new UserDtoBuilder();}

  public static class UserDtoBuilder {
    private final UserDto userDto = new UserDto();

    public UserDtoBuilder uuid( final String uuid) {
      userDto.uuid = uuid;
      return this;
    }

    public UserDtoBuilder username(final String username) {
      userDto.username = username;
      return this;
    }

    public UserDtoBuilder isActive(final boolean isActive) {
      userDto.isActive = isActive;
      return this;
    }

    public UserDtoBuilder isAdmin(final boolean isAdmin) {
      userDto.isAdmin = isAdmin;
      return this;
    }

    public UserDtoBuilder createDate(final Date createDate) {
      userDto.createDate = createDate;
      return this;
    }

    public UserDtoBuilder modifyDate(final Date modifyDate) {
      userDto.modifyDate = modifyDate;
      return this;
    }

    public UserDto build() {
      return userDto;
    }
  }

  public String getUuid() {
    return uuid;
  }

  public void setUuid(final String uuid) {
    this.uuid = uuid;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(final String username) {
    this.username = username;
  }

  public boolean isActive() {
    return isActive;
  }

  public void setActive(final boolean active) {
    isActive = active;
  }

  public boolean isAdmin() {
    return isAdmin;
  }

  public void setAdmin(final boolean admin) {
    isAdmin = admin;
  }

  public Date getCreateDate() {
    return createDate;
  }

  public void setCreateDate(final Date createDate) {
    this.createDate = createDate;
  }

  public Date getModifyDate() {
    return modifyDate;
  }

  public void setModifyDate(final Date modifyDate) {
    this.modifyDate = modifyDate;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UserDto userDto = (UserDto) o;
    return uuid.equals(userDto.uuid);
  }

  @Override
  public int hashCode() {
    return Objects.hash(uuid);
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", "[", "]")
        .add("uuid=" + uuid)
        .add("username=" + username)
        .add("isActive=" + isActive)
        .add("isAdmin=" + isAdmin)
        .add("createDate=" + createDate)
        .add("modifyDate=" + modifyDate)
        .toString();
  }
}
