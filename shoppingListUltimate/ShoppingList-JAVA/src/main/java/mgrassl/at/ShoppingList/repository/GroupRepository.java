package mgrassl.at.ShoppingList.repository;

import mgrassl.at.ShoppingList.domain.entity.Group;
import mgrassl.at.ShoppingList.domain.entity.UserGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;
import java.util.UUID;

@Repository
public interface GroupRepository extends JpaRepository<Group, UUID> {
}
