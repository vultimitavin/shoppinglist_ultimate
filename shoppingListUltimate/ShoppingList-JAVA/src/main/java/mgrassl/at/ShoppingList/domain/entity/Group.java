package mgrassl.at.ShoppingList.domain.entity;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "Groups")
public class Group {

  @Id
  @GeneratedValue
  @Column(name = "Id", columnDefinition = "uniqueidentifier")
  private UUID id;

  @Column(name = "Name", nullable = false)
  private String name;

  @OneToMany(mappedBy = "group", cascade = CascadeType.ALL,
      orphanRemoval = true)
  private Set<UserGroup> groupUsers;

  @CreationTimestamp
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "Create_date")
  private Date createDate;

  @UpdateTimestamp
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "Modify_date")
  private Date modifyDate;

  public static GroupBuilder builder() {
    return new GroupBuilder();
  }

  public static class GroupBuilder {

    private final Group group = new Group();

    public GroupBuilder name(String name) {
      group.name = name;
      return this;
    }

    public GroupBuilder groupUsers(Set<UserGroup> groupUsers) {
      group.groupUsers = groupUsers;
      return this;
    }

    public Group build() {
      return group;
    }
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public Set<UserGroup> getGroupUsers() {
    return groupUsers;
  }

  public void setGroupUsers(final Set<UserGroup> groupUsers) {
    this.groupUsers = groupUsers;
  }

  public UUID getId() {
    return id;
  }

  public Date getCreateDate() {
    return createDate;
  }

  public Date getModifyDate() {
    return modifyDate;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Group group = (Group) o;
    return id.equals(group.id); /*&&
        Objects.equals(groupUsers, group.groupUsers);*/
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", "[", "]")
        .add("id=" + id)
        .add("name=" + name)
        .add("groupUsers=" + groupUsers)
        .add("createDate=" + createDate)
        .add("modifyDate=" + modifyDate)
        .toString();
  }
}
