package mgrassl.at.ShoppingList.utility;

import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class UuidConvertService implements UuidConvert {

  @Override
  public UUID getUuid(final String string) {
    try {
      return UUID.fromString(string);
    } catch (IllegalArgumentException e) {
      throw new IllegalArgumentException("String '" + string + "' ist not a valid UUID:\n"
          + e.fillInStackTrace());
    }
  }

  @Override
  public String getStringFromUuid(final UUID uuid) {
    return uuid.toString();
  }
}
