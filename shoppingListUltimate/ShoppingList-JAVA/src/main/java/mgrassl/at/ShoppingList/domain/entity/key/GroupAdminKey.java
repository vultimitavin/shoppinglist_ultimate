package mgrassl.at.ShoppingList.domain.entity.key;

import mgrassl.at.ShoppingList.domain.entity.Group;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

@Embeddable
public class GroupAdminKey implements Serializable {

  @Column(name = "userId", columnDefinition = "uniqueidentifier")
  private UUID userId;

  @Column(name = "groupId", columnDefinition = "uniqueidentifier")
  private UUID groupId;

  public GroupAdminKey() {
  }

  public static GroupAdminKeyBuilder builder() {
    return new GroupAdminKeyBuilder();
  }

  public static class GroupAdminKeyBuilder {
    GroupAdminKey groupAdminKey = new GroupAdminKey();

    public GroupAdminKeyBuilder userId(UUID userId) {
      groupAdminKey.userId = userId;
      return this;
    }

    public GroupAdminKeyBuilder groupId(UUID groupId) {
      groupAdminKey.groupId = groupId;
      return this;
    }

    public GroupAdminKey build() {
      return groupAdminKey;
    }
  }

  public UUID getUserId() {
    return userId;
  }

  public void setUserId(final UUID userId) {
    this.userId = userId;
  }

  public UUID getGroupId() {
    return groupId;
  }

  public void setGroupId(final UUID groupId) {
    this.groupId = groupId;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GroupAdminKey that = (GroupAdminKey) o;
    return userId.equals(that.userId) &&
        groupId.equals(that.groupId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(userId, groupId);
  }
}
