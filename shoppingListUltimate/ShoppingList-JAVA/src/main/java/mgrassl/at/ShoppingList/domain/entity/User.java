package mgrassl.at.ShoppingList.domain.entity;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "Userdata")
public class User {

  @Id
  @GeneratedValue
  @Column(name = "Id", columnDefinition = "uniqueidentifier")
  private UUID id;

  @Column(name = "Username", columnDefinition = "nvarchar(30)", nullable = false, unique = true)
  private String username;

  @Column(name = "Password", nullable = false)
  private String password;

  @Column(name = "Admin", nullable = false)
  private boolean isAdmin;

  @Column(name = "Active", nullable = false)
  private boolean isActive;

  @OneToMany(mappedBy = "user", cascade = CascadeType.ALL,
      orphanRemoval = true)
  private Set<UserGroup> userGroups;

  @CreationTimestamp
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "create_date")
  private Date createDate;

  @UpdateTimestamp
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "modify_date")
  private Date modifyDate;

  @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
  private Set<Article> articles;

  @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
  private Set<Recipe> recipes;

  @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
  private Set<Purchase> purchases;

  public User() {
  }

  public static UserBuilder builder() { return new UserBuilder(); }

  public static class UserBuilder {
    private final User user = new User();

    public UserBuilder username(String username) {
      user.username = username;
      return this;
    }

    public UserBuilder isAdmin(boolean isAdmin) {
      user.isAdmin = isAdmin;
      return this;
    }

    public UserBuilder isActive(boolean isActive) {
      user.isActive = isActive;
      return this;
    }

    public UserBuilder password(String password) {
      user.password = password;
      return this;
    }

    public User build() {
      return user;
    }
  }

  public boolean isActive() {
    return isActive;
  }

  public void setActive(final boolean active) {
    isActive = active;
  }

  public void setUsername(final String username) {
    this.username = username;
  }

  public void setAdmin(final boolean admin) {
    isAdmin = admin;
  }

  public UUID getId() {
    return id;
  }

  public String getUsername() {
    return username;
  }

  public boolean isAdmin() {
    return isAdmin;
  }

  public Date getCreateDate() {
    return createDate;
  }

  public Date getModifyDate() {
    return modifyDate;
  }

  public String getPassword() {
    return password;
  }

  @Override
  public boolean equals(Object other) {
    if (this == other) {
      return true;
    }
    if (!(other instanceof User)) {
      return false;
    }
    if (other == null) {
      return false;
    }
    User user = (User) other;
    return username.equals(user.username);
  }

  @Override
  public int hashCode() {
    return Objects.hash(username);
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", "[", "]")
        .add("id=" + id)
        .add("username=" + username)
        .add("isAdmin=" + isAdmin)
        .add("createDate=" + createDate)
        .add("modifyDate=" + modifyDate)
        .toString();
  }
}
