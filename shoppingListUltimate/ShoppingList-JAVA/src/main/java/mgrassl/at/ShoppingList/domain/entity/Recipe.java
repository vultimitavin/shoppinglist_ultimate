package mgrassl.at.ShoppingList.domain.entity;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "recipes")
public class Recipe {

  @Id
  @GeneratedValue
  @Column(name = "Id", columnDefinition = "uniqueidentifier")
  private UUID id;

  @Column(name = "name", nullable = false)
  private String name;

  @ManyToOne
  @JoinColumn(name = "user_id", nullable = false)
  private User user;

  @ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
  @JoinTable(
      name = "recipe_ingredient",
      joinColumns = @JoinColumn(name = "recipe_id"),
      inverseJoinColumns = @JoinColumn(name = "ingredient_id"))
  private Set<Ingredient> ingredients;

  @CreationTimestamp
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "create_date")
  private Date createDate;

  @UpdateTimestamp
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "modify_date")
  private Date modifyDate;

  public static RecipeBuilder builder() {
    return new RecipeBuilder();
  }

  public static class RecipeBuilder {
    private final Recipe recipe = new Recipe();

    public RecipeBuilder name(String name) {
      recipe.name = name;
      return this;
    }

    public RecipeBuilder user(User user) {
      recipe.user = user;
      return this;
    }

    public RecipeBuilder ingredients(Set<Ingredient> ingredients) {
      recipe.ingredients = ingredients;
      return this;
    }

    public Recipe build() {
      return recipe;
    }
  }

  public User getUser() {
    return user;
  }

  public void setUser(final User user) {
    this.user = user;
  }

  public Set<Ingredient> getIngredients() {
    return ingredients;
  }

  public void setIngredients(final Set<Ingredient> ingredients) {
    this.ingredients = ingredients;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public UUID getId() {
    return id;
  }

  public Date getCreateDate() {
    return createDate;
  }

  public Date getModifyDate() {
    return modifyDate;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Recipe recipe = (Recipe) o;
    return user.equals(recipe.user) &&
        name.equals(recipe.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(user, name);
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", "[", "]")
        .add("id=" + id)
        .add("user=" + user)
        .add("name=" + name)
        .add("ingredients" + ingredients)
        .add("createDate=" + createDate)
        .add("modifyDate=" + modifyDate)
        .toString();
  }
}
