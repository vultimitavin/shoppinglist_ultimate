package mgrassl.at.ShoppingList.service;

import mgrassl.at.ShoppingList.domain.entity.Purchase;
import mgrassl.at.ShoppingList.domain.entity.User;
import mgrassl.at.ShoppingList.dto.PurchaseDto;
import mgrassl.at.ShoppingList.repository.PurchaseRepository;
import mgrassl.at.ShoppingList.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class PurchaseService {

  @Autowired
  private PurchaseRepository purchaseRepository;

  public List<Purchase> getPurchasesByUser(User user) {
    List<Purchase> purchases = new ArrayList<>();

    purchaseRepository.findAllByUser_Id(user.getId()).forEach(purchase -> {
      purchases.add(purchase);
    });

    return purchases;
  }

  public Purchase addPurchase(Purchase purchase) {
    return purchaseRepository.save(purchase);
  }

  public List<Purchase> addPurchases(List<Purchase> purchases) {
    return purchaseRepository.saveAll(purchases);
  }

  public void removePurchase(UUID uuid) {
    purchaseRepository.delete(purchaseRepository.findById(uuid).get());
  }
}
