package mgrassl.at.ShoppingList.service;

import mgrassl.at.ShoppingList.assembler.GroupAssembler;
import mgrassl.at.ShoppingList.assembler.UserAssembler;
import mgrassl.at.ShoppingList.domain.entity.Group;
import mgrassl.at.ShoppingList.domain.entity.User;
import mgrassl.at.ShoppingList.domain.entity.UserGroup;
import mgrassl.at.ShoppingList.dto.GroupDto;
import mgrassl.at.ShoppingList.dto.UserDto;
import mgrassl.at.ShoppingList.repository.GroupRepository;
import mgrassl.at.ShoppingList.repository.UserGroupRepository;
import mgrassl.at.ShoppingList.repository.UserRepository;
import mgrassl.at.ShoppingList.utility.UuidConvertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GroupService {

  @Autowired
  private UuidConvertService uuidConvertService;

  @Autowired
  private GroupAssembler groupAssembler;

  @Autowired
  private GroupRepository groupRepository;

  public GroupDto addGroup(GroupDto groupDto) {
    return groupAssembler.assemble(groupRepository.save(groupDto.asGroupEntity()));
  }

  public void removeGroup(GroupDto groupDto) {
    groupRepository.delete(groupDto.asGroupEntity());
  }

  public GroupDto updateGroup(GroupDto groupDto) {
    // Get group entity
    Group groupToUpdate = groupRepository.findById(uuidConvertService.getUuid(groupDto.getUuid())).get();

    groupToUpdate.setName(groupDto.getName());

    return groupAssembler.assemble(groupRepository.save(groupToUpdate));
  }

}
