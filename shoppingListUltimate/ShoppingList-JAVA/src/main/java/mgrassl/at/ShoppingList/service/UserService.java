package mgrassl.at.ShoppingList.service;

import mgrassl.at.ShoppingList.assembler.UserAssembler;
import mgrassl.at.ShoppingList.domain.entity.User;
import mgrassl.at.ShoppingList.dto.UserDto;
import mgrassl.at.ShoppingList.repository.UserRepository;
import mgrassl.at.ShoppingList.utility.UuidConvertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private UserAssembler userAssembler;


  private String hallo;

  @Autowired
  private UuidConvertService uuidConvertService;

  public UserDto getUserById(String uuid) {
    return userAssembler.assemble(userRepository.findById(uuidConvertService.getUuid(uuid)).get());
  }

  public UserDto getUserByUsername(String username) {
    return userAssembler.assemble(userRepository.findByUsername(username));
  }

  public User getUserByUsernameInternal(String username) {
    return userRepository.findByUsername(username);
  }

  //getAdmins

  public List<UserDto> getAdminUserList(boolean isAdmin) {
    return userAssembler.assemble(userRepository.findByIsAdmin(isAdmin));
  }

  //getActives

  public List<UserDto> getActiveUserList(boolean isActive) {
    return userAssembler.assemble(userRepository.findByIsActive(isActive));
  }

  public List<UserDto> getUserList() {
    return userAssembler.assemble(userRepository.findAll());
  }

  public UserDto addUser(UserDto userDto) {
    return userAssembler.assemble(userRepository.save(userDto.asUserEntity()));
  }

  public UserDto updateUser(String uuid, UserDto userDto) {
    User userToUpdate = userRepository.findById(uuidConvertService.getUuid(uuid)).get();

    userToUpdate.setUsername(userDto.getUsername());
    userToUpdate.setAdmin(userDto.isAdmin());
    userToUpdate.setActive(userDto.isActive());

    return userAssembler.assemble(userRepository.save(userToUpdate));
  }

  public void removeUser(String uuid) {
    userRepository.delete(userRepository.findById(uuidConvertService.getUuid(uuid)).get());
  }


}

