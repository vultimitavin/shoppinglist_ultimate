package mgrassl.at.ShoppingList.domain.entity;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "articles")
public class Article {

  @Id
  @GeneratedValue
  @Column(name = "Id", columnDefinition = "uniqueidentifier")
  private UUID id;

  @ManyToOne
  @JoinColumn(name = "user_id", nullable = false)
  private User user;

  @ManyToOne
  @JoinColumn(name = "unit_id", nullable = false)
  private Unit unit;

  @Column(name = "name", nullable = false)
  private String name;

  @Column(name = "amount", nullable = false)
  private int amount;

  @Column(name = "price", nullable = false)
  private double price;

  @Column(name = "isBought", nullable = false)
  private boolean isBought;

  @Column(name = "date_to_buy", nullable = false)
  private Date dateToBuy;

  @CreationTimestamp
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "create_date")
  private Date createDate;

  @UpdateTimestamp
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "modify_date")
  private Date modifyDate;

  public static ArticleBuilder builder() {
    return new ArticleBuilder();
  }

  public static class ArticleBuilder {

    private final Article article = new Article();

    public ArticleBuilder articleName(String articleName) {
      article.name = articleName;
      return this;
    }

    public ArticleBuilder amount(int amount) {
      article.amount = amount;
      return this;
    }

    public ArticleBuilder price(double price) {
      article.price = price;
      return this;
    }

    public ArticleBuilder isBought(boolean isBought) {
      article.isBought = isBought;
      return this;
    }

    public ArticleBuilder user(User user) {
      article.user = user;
      return this;
    }

    public ArticleBuilder unit(Unit unit) {
      article.unit = unit;
      return this;
    }

    public ArticleBuilder dateToBuy(Date dateToBuy) {
      article.dateToBuy = dateToBuy;
      return this;
    }

    public Article build() {
      return article;
    }
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public int getAmount() {
    return amount;
  }

  public void setAmount(final int amount) {
    this.amount = amount;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(final double price) {
    this.price = price;
  }

  public boolean isBought() {
    return isBought;
  }

  public void setBought(final boolean bought) {
    isBought = bought;
  }

  public Date getCreateDate() {
    return createDate;
  }

  public Date getModifyDate() {
    return modifyDate;
  }

  public UUID getId() {
    return id;
  }

  public User getUser() {
    return user;
  }

  public Unit getUnit() {
    return unit;
  }

  public Date getDateToBuy() {
    return dateToBuy;
  }

  public void setDateToBuy(final Date dateToBuy) {
    this.dateToBuy = dateToBuy;
  }
}
