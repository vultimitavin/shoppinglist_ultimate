package mgrassl.at.ShoppingList.repository;

import mgrassl.at.ShoppingList.domain.entity.Recipe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;
import java.util.UUID;

@Repository
public interface RecipeRepository extends JpaRepository<Recipe, UUID> {

  Set<Recipe> findAllByName(String name);
}
