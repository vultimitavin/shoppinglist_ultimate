package mgrassl.at.ShoppingList.dto;

import mgrassl.at.ShoppingList.domain.entity.Unit;

import java.util.Date;
import java.util.StringJoiner;

public class UnitDto {

  private String uuid;
  private String name;
  private String abbreviation;
  private Date createDate;
  private Date modifyDate;

  public UnitDto() {
  }

  public Unit asUnitEntity() {
    return Unit.builder()
        .name(getName())
        .abbreviation(getAbbreviation())
        .build();
  }

  public static UnitDtoBuilder builder() {
    return new UnitDtoBuilder();
  }

  public static class UnitDtoBuilder {
    private final UnitDto unitDto = new UnitDto();

    public UnitDtoBuilder uuid(String uuid) {
      unitDto.uuid = uuid;
      return this;
    }

    public UnitDtoBuilder name(String name) {
      unitDto.name = name;
      return this;
    }

    public UnitDtoBuilder abbreviation(String abbreviation) {
      unitDto.abbreviation = abbreviation;
      return this;
    }

    public UnitDtoBuilder createDate(Date createDate) {
      unitDto.createDate = createDate;
      return this;
    }

    public UnitDtoBuilder modifyDate(Date modifyDate) {
      unitDto.modifyDate = modifyDate;
      return this;
    }

    public UnitDto build() {
      return unitDto;
    }
  }

  public String getUuid() {
    return uuid;
  }

  public void setUuid(final String uuid) {
    this.uuid = uuid;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public String getAbbreviation() {
    return abbreviation;
  }

  public Date getCreateDate() {
    return createDate;
  }

  public Date getModifyDate() {
    return modifyDate;
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", "[", "]")
        .add("uuid=" + uuid)
        .add("name=" + name)
        .add("abbreviation=" + abbreviation)
        .toString();
  }
}
