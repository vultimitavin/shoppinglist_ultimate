package mgrassl.at.ShoppingList.repository;

import mgrassl.at.ShoppingList.domain.entity.Group;
import mgrassl.at.ShoppingList.domain.entity.User;
import mgrassl.at.ShoppingList.domain.entity.UserGroup;
import mgrassl.at.ShoppingList.domain.entity.key.GroupAdminKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;
import java.util.UUID;

@Repository
public interface UserGroupRepository extends JpaRepository<UserGroup, GroupAdminKey> {

  Set<UserGroup> findAllByUser(User user);

  Set<UserGroup> findAllByGroup(Group group);

  UserGroup findByUserAndGroup(User user, Group group);

  UserGroup findById_UserIdAndId_GroupId(UUID userId, UUID groupId);

  Set<UserGroup> findById_GroupId(UUID groupId);

  void removeByUserAndGroup(User user, Group group);

}
