package mgrassl.at.ShoppingList.domain.entity;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.*;

@Entity
@Table
public class Purchase {

  @Id
  @GeneratedValue
  @Column(name = "Id", columnDefinition = "uniqueidentifier")
  private UUID id;

  @ManyToOne
  @JoinColumn(name = "user_id", nullable = false)
  private User user;

  @Column(name = "payment", nullable = false)
  private double payment;

  @CreationTimestamp
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "date")
  private Date purchaseDate;

  public Purchase() {

  }

  public static PurchaseBuilder builder() {
    return new PurchaseBuilder();
  }

  public static class PurchaseBuilder {
    private final Purchase purchase = new Purchase();

    public PurchaseBuilder user(User user) {
      purchase.user = user;
      return this;
    }

    public PurchaseBuilder payment(double payment) {
      purchase.payment = payment;
      return this;
    }

    public Purchase build() {
      return purchase;
    }
  }

  public UUID getId() {
    return id;
  }

  public User getUser() {
    return user;
  }

  public double getPayment() {
    return payment;
  }

  public Date getPurchaseDate() {
    return purchaseDate;
  }

  public void setPayment(final double payment) {
    this.payment = payment;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Purchase purchase = (Purchase) o;
    return id.equals(purchase.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", "[", "]")
        .add("id=" + id)
        .add("user=" + user)
        .add("payment=" + payment)
        .add("purchaseDate=" + purchaseDate)
        .toString();
  }
}
