package mgrassl.at.ShoppingList.assembler;

import mgrassl.at.ShoppingList.domain.entity.Unit;
import mgrassl.at.ShoppingList.dto.UnitDto;
import mgrassl.at.ShoppingList.utility.UuidConvertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UnitAssembler extends AbstractAssembler<Unit, UnitDto> {

  @Autowired
  private UuidConvertService uuidConvertService;

  @Override
  public UnitDto assemble(final Unit unit) {
    if (unit == null) {
      return null;
    }
    return UnitDto
        .builder()
        .uuid(uuidConvertService.getStringFromUuid(unit.getId()))
        .name(unit.getName())
        .abbreviation(unit.getAbbreviation())
        .createDate(unit.getCreateDate())
        .modifyDate(unit.getModifyDate())
        .build();
  }
}
