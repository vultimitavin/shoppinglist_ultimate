package mgrassl.at.ShoppingList.domain.entity;

import mgrassl.at.ShoppingList.domain.entity.key.GroupAdminKey;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;
import java.util.StringJoiner;

@Entity
@Table(name = "user_group")
public class UserGroup {

  @PrePersist
  private void prePersist() {
    if (getId() == null) {
      GroupAdminKey pk = new GroupAdminKey();
      pk.setUserId(user.getId());
      pk.setGroupId(group.getId());
      id = pk;
    }
  }

  @EmbeddedId
  GroupAdminKey id;

  @ManyToOne
  @MapsId("userId")
  private User user;

  @ManyToOne
  @MapsId("groupId")
  private Group group;

  @CreationTimestamp
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "Create_date")
  private Date createDate;

  @UpdateTimestamp
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "Modify_date")
  private Date modifyDate;

  private boolean isGroupAdmin;

  @PreRemove
  private void synchronizeRelations() {
    group.getGroupUsers().remove(this);
    //user.getUserGroups().remove(this);
  }

  public UserGroup() {
  }

  public static UserGroupBuilder builder() {
    return new UserGroupBuilder();
  }

  public static class UserGroupBuilder {
    UserGroup userGroup = new UserGroup();

    public UserGroupBuilder user(User user) {
      userGroup.user = user;
      return this;
    }

    public UserGroupBuilder group(Group group) {
      userGroup.group = group;
      return this;
    }

    public UserGroupBuilder isGroupAdmin(boolean isGroupAdmin) {
      userGroup.isGroupAdmin = isGroupAdmin;
      return this;
    }

    public UserGroup build() {
      return userGroup;
    }
  }

  public User getUser() {
    return user;
  }

  public void setUser(final User user) {
    this.user = user;
  }

  public Group getGroup() {
    return group;
  }

  public void setGroup(final Group group) {
    this.group = group;
  }

  public boolean isGroupAdmin() {
    return isGroupAdmin;
  }

  public void setGroupAdmin(final boolean groupAdmin) {
    isGroupAdmin = groupAdmin;
  }

  public GroupAdminKey getId() {
    return id;
  }

  public Date getCreateDate() {
    return createDate;
  }

  public Date getModifyDate() {
    return modifyDate;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UserGroup userGroup = (UserGroup) o;
    return user.getId().equals(userGroup.user.getId()) &&
        group.getId().equals(userGroup.group.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(user.getId(), group.getId());
  }
}
