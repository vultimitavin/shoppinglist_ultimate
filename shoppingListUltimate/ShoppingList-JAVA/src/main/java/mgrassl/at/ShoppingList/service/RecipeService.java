package mgrassl.at.ShoppingList.service;

import javassist.tools.web.BadHttpRequest;
import mgrassl.at.ShoppingList.assembler.IngredientAssembler;
import mgrassl.at.ShoppingList.assembler.RecipeAssembler;
import mgrassl.at.ShoppingList.assembler.UserAssembler;
import mgrassl.at.ShoppingList.domain.entity.Ingredient;
import mgrassl.at.ShoppingList.domain.entity.Recipe;
import mgrassl.at.ShoppingList.domain.entity.Unit;
import mgrassl.at.ShoppingList.domain.entity.User;
import mgrassl.at.ShoppingList.dto.RecipeDto;
import mgrassl.at.ShoppingList.repository.IngredientRepository;
import mgrassl.at.ShoppingList.repository.RecipeRepository;
import mgrassl.at.ShoppingList.repository.UnitRepository;
import mgrassl.at.ShoppingList.repository.UserRepository;
import mgrassl.at.ShoppingList.utility.UuidConvertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class RecipeService {

  @Autowired
  private RecipeRepository recipeRepository;
  @Autowired
  private IngredientRepository ingredientRepository;
  @Autowired
  private RecipeAssembler recipeAssembler;
  @Autowired
  private UuidConvertService uuidConvertService;
  @Autowired
  private UnitRepository unitRepository;
  @Autowired
  private UserRepository userRepository;

  public ResponseEntity<RecipeDto> addRecipe(RecipeDto recipeDto) {
    User user = userRepository.findById(uuidConvertService.getUuid(
        recipeDto.getUserDto().getUuid())).get();

    Recipe recipe = Recipe.builder()
        .name(recipeDto.getName())
        .user(user)
        .ingredients(new HashSet<>())
        .build();

     if (!recipeRepository.findAll().contains(recipe)) {
       recipe = recipeRepository.save(recipe);
     }

    if (recipeDto.getIngredientDtos() != null && !recipeDto.getIngredientDtos().isEmpty()) {
      return ResponseEntity.ok(updateRecipe(uuidConvertService.getStringFromUuid(recipe.getId()), recipeDto));
    }

    try {
      return ResponseEntity.ok(recipeAssembler.assemble(recipe));
    } catch (NullPointerException e) {
      System.out.println("Couldnt persist Recipe, already exists from user");
      return ResponseEntity.badRequest().body(recipeDto);
    }
  }

  // TODO: Define return value for deleting recipe
  public void removeRecipe(String uuid) {
    // Get recipe entity
    Recipe recipe = recipeRepository.findById(
        uuidConvertService.getUuid(uuid)).get();

    recipeRepository.delete(recipe);
  }

  public RecipeDto getRecipeById(String uuid) {
    return recipeAssembler.assemble(
        recipeRepository.findById(uuidConvertService.getUuid(uuid)).get()
    );
  }

  public List<RecipeDto> getRecipeList() {
    return recipeAssembler.assemble(recipeRepository.findAll());
  }

  // TODO: Is there a better solution to this?
  public RecipeDto updateRecipe(String uuid, RecipeDto recipeDto) {
    // Get all Unit entities
    List<Unit> allUnits = unitRepository.findAll();
    // Get all Ingredient entities
    List<Ingredient> allIngredients = ingredientRepository.findAll();

    // Get recipe entity
    Recipe recipeToUpdate = recipeRepository.findById(
        uuidConvertService.getUuid(uuid)).get();

    Set<Ingredient> ingredientsUpdate = new HashSet<>();
    Set<Ingredient> oldIngredients = recipeToUpdate.getIngredients();
    Set<Ingredient> newIngredients = new HashSet<>();

    // Get the ingredients from the new recipe
    recipeDto.getIngredientDtos().forEach(ingredientDto -> {
      // Check if the ingredient already exists in the old recipe
      if (oldIngredients.contains(ingredientDto.asIngredientEntity())) {
        // If yes, add the ingredient entity to the new Ingredients
        oldIngredients.forEach(ingredient -> {
          if (ingredientDto.asIngredientEntity().equals(ingredient)) {
            ingredientsUpdate.add(ingredient);
          }
        });
      } else {
        // If no, check if there exists a similar ingredient entity
        if (allIngredients.contains(ingredientDto.asIngredientEntity())) {
          // If yes, add the ingredient entity to the new Ingredients
          allIngredients.forEach(ingredient -> {
            if (ingredientDto.asIngredientEntity().equals(ingredient)) {
              ingredientsUpdate.add(ingredient);
            }
          });
        } else {
          // If no, add it as a new ingredient entity to the new ingredients
          Ingredient ingredient = Ingredient.builder()
              .name(ingredientDto.getName())
              .amount(ingredientDto.getAmount())
              .unit(allUnits.get(allUnits.indexOf(ingredientDto.getUnitDto().asUnitEntity())))
              .build();
          newIngredients.add(ingredient);
        }

      }
    });

    System.out.println(newIngredients);

    // Check if new ingredients is empty, if it is not empty, save them to the db and add
    // them to the ingredientsUpdate
    if (!newIngredients.isEmpty()) {
      ingredientsUpdate.addAll(ingredientRepository.saveAll(newIngredients));
    }

    recipeToUpdate.setName(recipeDto.getName());
    recipeToUpdate.setIngredients(ingredientsUpdate);

    return recipeAssembler.assemble(recipeRepository.save(recipeToUpdate));
  }
}
