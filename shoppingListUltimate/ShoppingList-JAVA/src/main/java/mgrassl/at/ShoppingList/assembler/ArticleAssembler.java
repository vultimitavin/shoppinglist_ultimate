package mgrassl.at.ShoppingList.assembler;

import mgrassl.at.ShoppingList.domain.entity.Article;
import mgrassl.at.ShoppingList.dto.ArticleDto;
import mgrassl.at.ShoppingList.utility.UuidConvertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ArticleAssembler extends AbstractAssembler<Article, ArticleDto> {

  @Autowired
  private UuidConvertService uuidConvertService;

  @Override
  public ArticleDto assemble(final Article article) {
    if (article == null) {
      return null;
    }
    return ArticleDto.builder()
        .uuid(uuidConvertService.getStringFromUuid(article.getId()))
        .name(article.getName())
        .isBought(article.isBought())
        .amount(article.getAmount())
        .price(article.getPrice())
        .createDate(article.getCreateDate())
        .modifyDate(article.getModifyDate())
        .userUuid(uuidConvertService.getStringFromUuid(article.getUser().getId()))
        .unitUuid(uuidConvertService.getStringFromUuid(article.getUnit().getId()))
        .dateToBuy(article.getDateToBuy())
        .build();
  }
}
