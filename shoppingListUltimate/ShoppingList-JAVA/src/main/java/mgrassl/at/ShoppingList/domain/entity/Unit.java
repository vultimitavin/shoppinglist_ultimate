package mgrassl.at.ShoppingList.domain.entity;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "Unit")
public class Unit {

  @Id
  @GeneratedValue
  @Column(name = "Id", columnDefinition = "uniqueidentifier")
  private UUID id;

  @Column(name = "name", columnDefinition = "nvarchar(20)", nullable = false, unique = true)
  private String name;

  @Column(name = "abbreviation", columnDefinition = "nvarchar(10)", nullable = false, unique = true)
  private String abbreviation;

  @OneToMany(mappedBy = "unit", cascade = CascadeType.PERSIST)
  private Set<Article> articles;

  @OneToMany(mappedBy = "unit", cascade = CascadeType.PERSIST)
  private Set<Ingredient> ingredients;

  @CreationTimestamp
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "create_date")
  private Date createDate;

  @UpdateTimestamp
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "modify_date")
  private Date modifyDate;

  public static UnitBuilder builder() {
    return new UnitBuilder();
  }

  public static class UnitBuilder {
    private final Unit unit = new Unit();

    public UnitBuilder name(String name) {
      unit.name = name;
      return this;
    }

    public UnitBuilder abbreviation(String abbreviation) {
      unit.abbreviation = abbreviation;
      return this;
    }

    public Unit build() {
      return unit;
    }
  }

  public UUID getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public String getAbbreviation() {
    return abbreviation;
  }

  public void setAbbreviation(final String abbreviation) {
    this.abbreviation = abbreviation;
  }

  public Date getCreateDate() {
    return createDate;
  }

  public Date getModifyDate() {
    return modifyDate;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Unit unit = (Unit) o;
    return name.equals(unit.name) &&
        abbreviation.equals(unit.abbreviation);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, abbreviation);
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", "[", "]")
        .add("id=" + id)
        .add("name=" + name)
        .add("abbreviation=" + abbreviation)
        .toString();
  }
}
