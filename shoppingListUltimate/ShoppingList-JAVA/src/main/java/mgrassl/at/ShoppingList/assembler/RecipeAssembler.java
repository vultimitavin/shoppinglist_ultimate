package mgrassl.at.ShoppingList.assembler;

import mgrassl.at.ShoppingList.domain.entity.Recipe;
import mgrassl.at.ShoppingList.dto.IngredientDto;
import mgrassl.at.ShoppingList.dto.RecipeDto;
import mgrassl.at.ShoppingList.repository.IngredientRepository;
import mgrassl.at.ShoppingList.utility.UuidConvertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class RecipeAssembler extends AbstractAssembler<Recipe, RecipeDto> {

  @Autowired
  private UserAssembler userAssembler;
  @Autowired
  private IngredientAssembler ingredientAssembler;
  @Autowired
  private UuidConvertService uuidConvertService;
  @Autowired
  private IngredientRepository ingredientRepository;

  @Override
  public RecipeDto assemble(final Recipe recipe) {
    if (recipe == null) {
      return null;
    }

    Set<IngredientDto> ingredientDtos = new HashSet<>();

    recipe.getIngredients().forEach(ingredient -> {
      ingredientDtos.add(ingredientAssembler.assemble(ingredient));
    });

    return RecipeDto.builder()
        .uuid(uuidConvertService.getStringFromUuid(recipe.getId()))
        .name(recipe.getName())
        .user(userAssembler.assemble(recipe.getUser()))
        .ingredients(ingredientDtos)
        .modifyDate(recipe.getModifyDate())
        .createDate(recipe.getCreateDate())
        .build();
  }
}
