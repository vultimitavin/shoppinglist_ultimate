package mgrassl.at.ShoppingList.api;

import mgrassl.at.ShoppingList.dto.UserDto;
import mgrassl.at.ShoppingList.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/user")
public class UserRestApi {

  @Autowired
  private UserService userService;

  @GetMapping
  public List<UserDto> getUserList() {
    return userService.getUserList();
  }

  @GetMapping("{username}")
  public UserDto getUserByUsername(@PathVariable("username") String username) {
    return userService.getUserByUsername(username);
  }
}
