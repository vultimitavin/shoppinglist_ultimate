package mgrassl.at.ShoppingList.assembler;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class AbstractAssembler<S, T> implements Assembler<S, T> {

  @Override
  public final List<T> assemble(final Collection<S> source) {
    List<T> targets = new ArrayList<>();
    source.forEach(s -> targets.add(assemble(s)));
    return applyOrdering(targets);
  }

  protected List<T> applyOrdering(List<T> list) {
    return list;
  }
}
