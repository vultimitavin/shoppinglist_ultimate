package mgrassl.at.ShoppingList.service;

import mgrassl.at.ShoppingList.assembler.UserGroupAssembler;
import mgrassl.at.ShoppingList.domain.entity.Group;
import mgrassl.at.ShoppingList.domain.entity.User;
import mgrassl.at.ShoppingList.domain.entity.UserGroup;
import mgrassl.at.ShoppingList.dto.GroupDto;
import mgrassl.at.ShoppingList.dto.UserDto;
import mgrassl.at.ShoppingList.dto.UserGroupDto;
import mgrassl.at.ShoppingList.repository.GroupRepository;
import mgrassl.at.ShoppingList.repository.UserGroupRepository;
import mgrassl.at.ShoppingList.repository.UserRepository;
import mgrassl.at.ShoppingList.utility.UuidConvertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserGroupService {

  @Autowired
  private UserGroupRepository userGroupRepository;
  @Autowired
  private UserGroupAssembler userGroupAssembler;
  @Autowired
  private GroupRepository groupRepository;
  @Autowired
  private UserRepository userRepository;
  @Autowired
  private UuidConvertService uuidConvertService;

  public UserGroupDto addUserToGroup(UserDto userDto, GroupDto groupDto, boolean isGroupAdmin) {
    // Get User and Group Entities
    Group group = groupRepository.findById(uuidConvertService.getUuid(groupDto.getUuid())).get();
    User user = userRepository.findById(uuidConvertService.getUuid(userDto.getUuid())).get();

    // Create the new relation
    UserGroup userGroup = UserGroup.builder()
        .user(user)
        .group(group)
        .isGroupAdmin(isGroupAdmin)
        .build();

    // Save the new relation to the database
    group.getGroupUsers().add(userGroup);
    groupRepository.save(group);

    return userGroupAssembler.assemble(userGroup);
  }

  // TODO: Define returntype
  public void removeUserFromGroup(UserDto userDto, GroupDto groupDto) {
    // Get User and Group Entities
    Group group = groupRepository.findById(uuidConvertService.getUuid(groupDto.getUuid())).get();
    User user = userRepository.findById(uuidConvertService.getUuid(userDto.getUuid())).get();

    // Get the relation
    UserGroup userGroup = userGroupRepository.findByUserAndGroup(user, group);

    // Remove the relation
    userGroupRepository.delete(userGroup);
  }

  public UserGroupDto updateUserInGroup(UserGroupDto userGroupDto) {
    // Get the UserGroup entitiy
    UserGroup userGroupToUpdate = userGroupRepository.findById_UserIdAndId_GroupId(
        uuidConvertService.getUuid(userGroupDto.getUserDto().getUuid()),
        uuidConvertService.getUuid(userGroupDto.getGroupDto().getUuid())
    );

    userGroupToUpdate.setGroupAdmin(userGroupDto.isGroupAdmin());

    return userGroupAssembler.assemble(userGroupRepository.save(userGroupToUpdate));
  }

  public List<UserGroupDto> getUserGroups(UserDto userDto) {
    User user = userRepository.findById(uuidConvertService.getUuid(userDto.getUuid())).get();
    return userGroupAssembler.assemble(
        userGroupRepository.findAllByUser(user)
    );
  }

  public List<UserGroupDto> getGroupUsers(GroupDto groupDto) {
    Group group = groupRepository.findById(uuidConvertService.getUuid(groupDto.getUuid())).get();
    return userGroupAssembler.assemble(
        userGroupRepository.findAllByGroup(group)
    );
  }

}
