package mgrassl.at.ShoppingList.service;

import mgrassl.at.ShoppingList.assembler.UnitAssembler;
import mgrassl.at.ShoppingList.domain.entity.Unit;
import mgrassl.at.ShoppingList.dto.UnitDto;
import mgrassl.at.ShoppingList.repository.UnitRepository;
import mgrassl.at.ShoppingList.utility.UuidConvertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UnitService {

  @Autowired
  private UnitRepository unitRepository;

  @Autowired
  private UnitAssembler unitAssembler;

  @Autowired
  private UuidConvertService uuidConvertService;

  public List<UnitDto> getUnitList() {
    return unitAssembler.assemble(
        unitRepository.findAll()
    );
  }

  public UnitDto getUnitbyName(String name) {
    return unitAssembler.assemble(
        unitRepository.findByName(name)
    );
  }

  public UnitDto addUnit(UnitDto unitDto) {
    return unitAssembler.assemble(
        unitRepository.save(unitDto.asUnitEntity())
    );
  }

  public List<UnitDto> addUnitList(Iterable<UnitDto> unitDtos) {
    List<Unit> units = new ArrayList<>();

    unitDtos.forEach(unitDto -> {
      units.add(unitDto.asUnitEntity());
    });

    return unitAssembler.assemble(
        unitRepository.saveAll(units)
    );
  }

  public UnitDto updateUnit(UnitDto unitDto) {
    Unit unitToUpdate = unitRepository.findById(uuidConvertService.getUuid(unitDto.getUuid())).get();

    unitToUpdate.setAbbreviation(unitDto.getAbbreviation());
    unitToUpdate.setName(unitDto.getName());

    return unitAssembler.assemble(
        unitRepository.save(unitToUpdate)
    );
  }
}
