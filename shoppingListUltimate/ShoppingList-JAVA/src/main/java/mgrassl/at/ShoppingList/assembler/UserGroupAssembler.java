package mgrassl.at.ShoppingList.assembler;

import mgrassl.at.ShoppingList.domain.entity.UserGroup;
import mgrassl.at.ShoppingList.dto.UserGroupDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserGroupAssembler extends AbstractAssembler<UserGroup, UserGroupDto> {

  @Autowired
  private UserAssembler userAssembler;
  @Autowired
  private GroupAssembler groupAssembler;

  @Override
  public UserGroupDto assemble(final UserGroup userGroup) {
    if (userGroup == null) {
      return null;
    }
    return UserGroupDto.builder()
        .userDto(userAssembler.assemble(userGroup.getUser()))
        .groupDto(groupAssembler.assemble(userGroup.getGroup()))
        .isGroupAdmin(userGroup.isGroupAdmin())
        .build();
  }
}
