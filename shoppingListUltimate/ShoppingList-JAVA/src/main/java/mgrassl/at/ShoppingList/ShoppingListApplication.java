package mgrassl.at.ShoppingList;

import mgrassl.at.ShoppingList.domain.entity.Article;
import mgrassl.at.ShoppingList.repository.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class ShoppingListApplication {

	@Autowired
	private ArticleRepository articleRepository;

	public static void main(String[] args) {
		SpringApplication.run(ShoppingListApplication.class, args);
	}

	@PostConstruct
	public void initDB() {


		//articleRepository.findAll().forEach(article -> System.out.println(article));
	}
}
