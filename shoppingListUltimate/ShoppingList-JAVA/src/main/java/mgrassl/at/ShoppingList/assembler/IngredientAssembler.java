package mgrassl.at.ShoppingList.assembler;

import mgrassl.at.ShoppingList.domain.entity.Ingredient;
import mgrassl.at.ShoppingList.dto.IngredientDto;
import mgrassl.at.ShoppingList.dto.RecipeDto;
import mgrassl.at.ShoppingList.utility.UuidConvertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class IngredientAssembler extends AbstractAssembler<Ingredient, IngredientDto> {

  @Autowired
  private RecipeAssembler recipeAssembler;
  @Autowired
  private UnitAssembler unitAssembler;
  @Autowired
  private UuidConvertService uuidConvertService;

  @Override
  public IngredientDto assemble(final Ingredient ingredient) {
    if (ingredient == null) {
      return null;
    }

    return IngredientDto.builder()
        .uuid(uuidConvertService.getStringFromUuid(ingredient.getId()))
        .name(ingredient.getName())
        .amount(ingredient.getAmount())
        .unit(unitAssembler.assemble(ingredient.getUnit()))
        .modifyDate(ingredient.getModifyDate())
        .createDate(ingredient.getCreateDate())
        .build();
  }
}
