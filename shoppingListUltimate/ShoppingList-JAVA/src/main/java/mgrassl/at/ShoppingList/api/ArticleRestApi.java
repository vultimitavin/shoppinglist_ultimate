package mgrassl.at.ShoppingList.api;

import mgrassl.at.ShoppingList.domain.entity.Article;
import mgrassl.at.ShoppingList.dto.ArticleDto;
import mgrassl.at.ShoppingList.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/Artikel")
public class ArticleRestApi {

  @Autowired
  private ArticleService articleService;

  @GetMapping("/{id}")
  public ArticleDto getArticleById(@PathVariable("id") String uuid) {
    return articleService.getArticleById(uuid);
  }

  @GetMapping
  public List<ArticleDto> getArticleList() {
    return articleService.getArticleList();
  }

  @RequestMapping(value = "/names", method = RequestMethod.GET)
  public HashSet<String> getArticleNameList() {
    return articleService.getArticleNameList();
  }

  @PostMapping
  public ArticleDto addArticle(@Valid @RequestBody ArticleDto articleDto) {
    return articleService.addArticle(articleDto);
  }

  @PostMapping("/list")
  public List<ArticleDto> addArticleList(@Valid @RequestBody Iterable<ArticleDto> articleDtos) {
    return articleService.addArticleList(articleDtos);
  }

  @PutMapping("/{id}")
  public ResponseEntity<ArticleDto> updateArticle(@PathVariable(value = "id") String uuid,
                                      @Valid @RequestBody ArticleDto articleDto) {
    return ResponseEntity.ok(articleService.updateArticle(uuid, articleDto));
  }


  @DeleteMapping("/{id}")
  public Map<String, Boolean> removeArticle(@PathVariable(value = "id") String uuid) {
    articleService.removeArticle(uuid);

    Map<String, Boolean> res = new HashMap<>();
    res.put("Deleted", true);
    return res;
  }
}
