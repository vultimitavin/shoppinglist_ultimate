package mgrassl.at.ShoppingList.dto;

import mgrassl.at.ShoppingList.domain.entity.Purchase;
import mgrassl.at.ShoppingList.repository.UserRepository;
import mgrassl.at.ShoppingList.utility.UuidConvertService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.Objects;
import java.util.StringJoiner;

public class PurchaseDto {

  @Autowired
  private UserRepository userRepository;
  @Autowired
  private UuidConvertService uuidConvertService;

  private String uuid;
  private String userUuid;
  private double payment;
  private Date purchaseDate;

  public Purchase asPurchaseEntity() {
    return Purchase.builder()
        .user(userRepository.findById(uuidConvertService.getUuid(getUserUuid())).get())
        .payment(getPayment())
        .build();
  }

  public PurchaseDto() {
  }

  public static PurchaseDtoBuilder builder() { return new PurchaseDtoBuilder(); }

  public static class PurchaseDtoBuilder {
    private final PurchaseDto purchaseDto = new PurchaseDto();

    public PurchaseDtoBuilder uuid(String uuid) {
      purchaseDto.uuid = uuid;
      return this;
    }

    public PurchaseDtoBuilder userUuid(String userUuid) {
      purchaseDto.userUuid = userUuid;
      return this;
    }

    public PurchaseDtoBuilder payment(double payment) {
      purchaseDto.payment = payment;
      return this;
    }

    public PurchaseDtoBuilder purchaseDate(Date purchaseDate) {
      purchaseDto.purchaseDate = purchaseDate;
      return this;
    }

    public PurchaseDto build() { return purchaseDto; }
  }

  public String getUuid() {
    return uuid;
  }

  public void setUuid(final String uuid) {
    this.uuid = uuid;
  }

  public String getUserUuid() {
    return userUuid;
  }

  public void setUserUuid(final String userUuid) {
    this.userUuid = userUuid;
  }

  public double getPayment() {
    return payment;
  }

  public void setPayment(final double payment) {
    this.payment = payment;
  }

  public Date getPurchaseDate() {
    return purchaseDate;
  }

  public void setPurchaseDate(final Date purchaseDate) {
    this.purchaseDate = purchaseDate;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PurchaseDto that = (PurchaseDto) o;
    return uuid.equals(that.uuid);
  }

  @Override
  public int hashCode() {
    return Objects.hash(uuid);
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", "[", "]")
        .add("uuid=" + uuid)
        .add("userUuid=" + userUuid)
        .add("payment=" + payment)
        .add("purchaseDate=" + purchaseDate)
        .toString();
  }
}
