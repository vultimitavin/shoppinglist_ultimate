package mgrassl.at.ShoppingList.config;

import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class DataSourceConfig {
  @Bean
  public DataSource datasource() {
    return DataSourceBuilder.create()
        .driverClassName("com.microsoft.sqlserver.jdbc.SQLServerDriver")
        .url("jdbc:sqlserver://localhost;databaseName=shoppingList")
        .username("sa")
        .password("fhbfiwien")
        .build();
  }
}