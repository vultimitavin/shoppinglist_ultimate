package mgrassl.at.ShoppingList.dto;

import mgrassl.at.ShoppingList.domain.entity.Article;
import mgrassl.at.ShoppingList.domain.entity.Unit;
import mgrassl.at.ShoppingList.domain.entity.User;

import java.util.Date;
import java.util.Objects;
import java.util.StringJoiner;

public class ArticleDto {

  private String uuid;
  private String name;
  private String userUuid;
  private String unitUuid;
  private int amount;
  private double price;
  private boolean isBought;
  private Date dateToBuy;
  private Date createDate;
  private Date modifyDate;

  public Article asArticleEntity(User user, Unit unit) {
    return Article.builder()
        .articleName(getName())
        .amount(getAmount())
        .price(getPrice())
        .isBought(isBought())
        .user(user)
        .unit(unit)
        .dateToBuy(getDateToBuy())
        .build();
  }

  public ArticleDto() {
  }

  public static ArticleDtoBuilder builder() {
    return new ArticleDtoBuilder();
  }

  public static class ArticleDtoBuilder {

    private final ArticleDto articleDto = new ArticleDto();

    public ArticleDtoBuilder uuid(String uuid) {
      articleDto.uuid = uuid;
      return this;
    }

    public ArticleDtoBuilder name(String name) {
      articleDto.name = name;
      return this;
    }

    public ArticleDtoBuilder userUuid(String userUuid) {
      articleDto.userUuid = userUuid;
      return this;
    }

    public ArticleDtoBuilder unitUuid(String unitUuid) {
      articleDto.unitUuid = unitUuid;
      return this;
    }

    public ArticleDtoBuilder amount(int amount) {
      articleDto.amount = amount;
      return this;
    }

    public ArticleDtoBuilder price(double price) {
      articleDto.price = price;
      return this;
    }

    public ArticleDtoBuilder isBought(boolean isBought) {
      articleDto.isBought = isBought;
      return this;
    }

    public ArticleDtoBuilder createDate(Date createDate) {
      articleDto.createDate = createDate;
      return this;
    }

    public ArticleDtoBuilder modifyDate(Date modifyDate) {
      articleDto.modifyDate = modifyDate;
      return this;
    }

    public ArticleDtoBuilder dateToBuy(Date dateToBuy) {
      articleDto.dateToBuy = dateToBuy;
      return this;
    }

    public ArticleDto build() {
      return articleDto;
    }
  }

  public String getUuid() {
    return uuid;
  }

  public void setUuid(final String uuid) {
    this.uuid = uuid;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public int getAmount() {
    return amount;
  }

  public void setAmount(final int amount) {
    this.amount = amount;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(final double price) {
    this.price = price;
  }

  public boolean isBought() {
    return isBought;
  }

  public void setBought(final boolean bought) {
    isBought = bought;
  }

  public Date getCreateDate() {
    return createDate;
  }

  public void setCreateDate(final Date createDate) {
    this.createDate = createDate;
  }

  public Date getModifyDate() {
    return modifyDate;
  }

  public void setModifyDate(final Date modifyDate) {
    this.modifyDate = modifyDate;
  }

  public String getUserUuid() {
    return userUuid;
  }

  public void setUserUuid(final String userUuid) {
    this.userUuid = userUuid;
  }

  public String getUnitUuid() {
    return unitUuid;
  }

  public void setUnitUuid(final String unitUuid) {
    this.unitUuid = unitUuid;
  }

  public Date getDateToBuy() {
    return dateToBuy;
  }

  public void setDateToBuy(final Date dateToBuy) {
    this.dateToBuy = dateToBuy;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ArticleDto that = (ArticleDto) o;
    return uuid.equals(that.uuid);
  }

  @Override
  public int hashCode() {
    return Objects.hash(uuid);
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", "[", "]")
        .add("uuid=" + uuid)
        .add("name=" + name)
        .add("userUuid=" + userUuid)
        .add("unitUuid=" + unitUuid)
        .add("amount=" + amount)
        .add("price=" + price)
        .add("isBought=" + isBought)
        .add("dateToBuy=" + dateToBuy)
        .add("createDate=" + createDate)
        .add("modifyDate=" + modifyDate)
        .toString();
  }
}
