package mgrassl.at.ShoppingList.api;

import mgrassl.at.ShoppingList.dto.RecipeDto;
import mgrassl.at.ShoppingList.service.RecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.sql.SQLOutput;
import java.util.*;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/recipe")
public class RecipeRestApi {

  @Autowired
  private RecipeService recipeService;

  @GetMapping("/{id}")
  public RecipeDto getRecipeById(@PathVariable("id") String uuid) {
    return recipeService.getRecipeById(uuid);
  }

  @GetMapping
  public List<RecipeDto> getRecipeList() {
    return recipeService.getRecipeList();
  }

  @PostMapping
  public ResponseEntity<RecipeDto> addRecipe(@Valid @RequestBody RecipeDto recipeDto) {
    return recipeService.addRecipe(recipeDto);
  }

  @PutMapping("/{id}")
  public ResponseEntity<RecipeDto> updateRecipe(@PathVariable(value = "id") String uuid,
                                                @Valid @RequestBody RecipeDto recipeDto) {
    return ResponseEntity.ok(recipeService.updateRecipe(uuid, recipeDto));
  }

  @DeleteMapping("/{id}")
  public Map<String, Boolean> removeRecipe(@PathVariable(value = "id") String uuid) {
    recipeService.removeRecipe(uuid);

    Map<String, Boolean> res = new HashMap<>();
    res.put("Deleted", true);
    return res;
  }

}
