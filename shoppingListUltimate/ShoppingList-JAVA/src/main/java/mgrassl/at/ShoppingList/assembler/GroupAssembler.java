package mgrassl.at.ShoppingList.assembler;

import mgrassl.at.ShoppingList.domain.entity.Group;
import mgrassl.at.ShoppingList.domain.entity.UserGroup;
import mgrassl.at.ShoppingList.dto.GroupDto;
import mgrassl.at.ShoppingList.dto.UserDto;
import mgrassl.at.ShoppingList.repository.UserGroupRepository;
import mgrassl.at.ShoppingList.service.UserGroupService;
import mgrassl.at.ShoppingList.utility.UuidConvertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class GroupAssembler extends AbstractAssembler<Group, GroupDto> {

  @Autowired
  private UuidConvertService uuidConvertService;
  @Autowired
  private UserAssembler userAssembler;
  @Autowired
  private UserGroupRepository userGroupRepository;

  @Override
  public GroupDto assemble(final Group group) {
    if (group == null) {
      return null;
    }

    Set<UserDto> groupUserDtos = new HashSet<>();

    // Get UserGroups
    userGroupRepository.findAllByGroup(group).forEach(userGroup -> {
      groupUserDtos.add(userAssembler.assemble(userGroup.getUser()));
    });

    return GroupDto
        .builder()
        .uuid(uuidConvertService.getStringFromUuid(group.getId()))
        .name(group.getName())
        .users(groupUserDtos)
        .modifyDate(group.getModifyDate())
        .createDate(group.getCreateDate())
        .build();
  }
}
