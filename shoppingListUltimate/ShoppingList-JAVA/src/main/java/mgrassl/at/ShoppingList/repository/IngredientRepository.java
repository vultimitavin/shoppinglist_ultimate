package mgrassl.at.ShoppingList.repository;

import mgrassl.at.ShoppingList.domain.entity.Ingredient;
import mgrassl.at.ShoppingList.domain.entity.Recipe;
import mgrassl.at.ShoppingList.domain.entity.Unit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;
import java.util.UUID;

@Repository
public interface IngredientRepository extends JpaRepository<Ingredient, UUID> {

  Set<Ingredient> findAllByRecipes(Recipe recipe);

  Ingredient findByNameAndUnitAndAmount(String name, Unit unit, double amount);
}
