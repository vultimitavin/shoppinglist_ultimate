package mgrassl.at.ShoppingList.repository;

import mgrassl.at.ShoppingList.domain.entity.Purchase;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface PurchaseRepository extends JpaRepository<Purchase, UUID> {

  List<Purchase> findAllByUser_Id(UUID uuid);

}
