package mgrassl.at.ShoppingList.model;

import java.io.Serializable;

public class JwtResponse implements Serializable {

  private static final long SERIAL_VERSION_UID = -8091879091924046844L;
  private final String jwttoken;

  public JwtResponse(final String jwttoken) {
    this.jwttoken = jwttoken;
  }

  public String getToken() {
    return this.jwttoken;
  }
}
