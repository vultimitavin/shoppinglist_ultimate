import {Component, OnInit} from '@angular/core';
import {Config, ConfigService} from './config.service';

@Component({
  selector: 'app-config',
  template: `<span *ngIf="config">
                <p>Heroes API URL is "{{config.articleUrl}}"</p>
             </span>`,
  providers: [ConfigService]
})

export class ConfigComponent implements OnInit {
  error: any;
  headers: string[];
  config: Config;

  constructor(private configService: ConfigService) {
  }

  ngOnInit() {
    this.showConfig();
  }

  showConfig() {
    this.configService.getConfig()
      .subscribe((data: Config) => this.config = { ...data });
  }
}
