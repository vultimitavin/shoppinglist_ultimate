import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AuthenticationService} from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class HttpInterceptorShoppingListService implements HttpInterceptor {
  constructor(private authService: AuthenticationService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    let authReq;
    console.log('request intercepted successfully!');

    if (this.authService.loggedIn) {
      authReq = req.clone({
        headers:
          req.headers.set('Authorization', this.authService.getAuthorizationHeader())
      });
    } else {
      authReq = req.clone();
    }

    return next.handle(authReq);
  }
}
