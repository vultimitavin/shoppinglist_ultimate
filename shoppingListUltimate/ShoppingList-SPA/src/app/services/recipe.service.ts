import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Recipe} from '../class/Recipe';

@Injectable({
  providedIn: 'root'
})
export class RecipeService {

  private baseUrl = 'http://localhost:8080/recipe';

  constructor(private http: HttpClient) {
  }

  getRecipeList(): Observable<any> {
    return this.http.get(this.baseUrl);
  }

  getRecipeById(uuid: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/${uuid}`);
  }

  createRecipe(recipe: Recipe): Observable<any> {
    return this.http.post(this.baseUrl, recipe);
  }

  updateRecipe(uuid: string, recipe: Recipe): Observable<any> {
    return this.http.put(`${this.baseUrl}/${uuid}`, recipe);
  }

  deleteRecipe(uuid: string): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${uuid}`);
  }
}
