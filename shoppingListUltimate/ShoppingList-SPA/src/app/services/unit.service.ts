import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Unit} from '../class/Unit';

@Injectable({
  providedIn: 'root'
})
export class UnitService {

  private baseUrl = 'http://localhost:8080/unit';

  constructor(private http: HttpClient) {
  }

  getUnitList(): Observable<any> {
    return this.http.get(this.baseUrl);
  }

  getUnitByUnitname(unitname: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/${unitname}`);
  }

  createUnit(unit: Unit): Observable<any> {
    return this.http.post(this.baseUrl, unit);
  }

  createUnitList(unit: Unit[]): Observable<any> {
    return this.http.post(this.baseUrl + '/list', unit);
  }
}
