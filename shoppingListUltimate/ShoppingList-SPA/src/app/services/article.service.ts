import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
import {Article} from '../class/Article';

@Injectable({
  providedIn: 'root'
})

export class ArticleService {
  shareArticleCreateDialogData$: Observable<any>;
  articlesTableData = new Subject<Article[]>();

  private baseUrl = 'http://localhost:8080/Artikel';

  constructor(private http: HttpClient) {
    this.shareArticleCreateDialogData$ = this.articlesTableData.asObservable();
  }


  setArticleTableData(data) {
    this.articlesTableData.next(data);
  }

  getArticleList(): Observable<any> {
    return this.http.get(this.baseUrl);
  }

  getArticle(uuid: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/${uuid}`);
  }

  createArticle(article: Article): Observable<any> {
    return this.http.post(this.baseUrl, article);
  }

  createArticleList(articles: Article[]): Observable<any> {
    return this.http.post(this.baseUrl + '/list', articles);
  }

  deleteArticle(uuid: string): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${uuid}`, {responseType: 'text'});
  }

  updateArticle(uuid: string, article: Article): Observable<any> {
    return this.http.put(`${this.baseUrl}/${uuid}`, article);
  }

  getCurrentDateIsoString(): string {
    const today = new Date();
    const dd = String(today.getDate()).padStart(2, '0');
    const mm = String(today.getMonth() + 1).padStart(2, '0'); // January is 0!
    const yyyy = today.getFullYear();

    return `${yyyy}-${mm}-${dd}`;
  }
}
