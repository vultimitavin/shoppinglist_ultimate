import {Injectable} from '@angular/core';
import {Article} from '../../class/Article';
import {UserService} from '../user.service';
import {User} from '../../class/User';

@Injectable({
  providedIn: 'root'
})
export class UtilService {
  weekDay = {
    0: 'Sunday',
    1: 'Monday',
    2: 'Tuesday',
    3: 'Wednesday',
    4: 'Thursday',
    5: 'Friday',
    6: 'Saturday'
  };

  constructor(private userService: UserService) {
  }

  public showLoadingScreen() {
    const loadingScreen = document.querySelector('.loadingSpinner');

    loadingScreen.classList.replace('loadingSpinner', 'loadingSpinner--show');
  }

  public hideLoadingScreen() {
    const loadingScreen = document.querySelector('.loadingSpinner--show');

    loadingScreen.classList.replace('loadingSpinner--show', 'loadingSpinner');
  }

  public createSnackBarItem(message: string, headerText: string) {
    // Get the snackbar DIV
    const snackBar = document.getElementById('snackbar');
    const snackBarMessage = document.getElementById('snackBarMessage');
    const snackBarHeaderText = document.getElementById('snackBarHeaderText');

    // Add the SnackBar Message
    snackBarMessage.textContent = message;
    // Add SnackBar Header Text
    snackBarHeaderText.textContent = headerText;

    // Add the "show" class to DIV
    snackBar.className = 'show';

    // After 3 seconds, remove the show class from DIV
    setTimeout(() => {
      snackBar.className = snackBar.className.replace('show', '');
    }, 10000);
  }

  getWeekDay(day: number): string {
    return this.weekDay[day];
  }

  prettyPrintDate(d: Date): string {
    const date = new Date(d);
    const dd = String(date.getDate()).padStart(2, '0');
    const mm = String(date.getMonth() + 1).padStart(2, '0'); // January is 0!
    const yyyy = date.getFullYear();

    return `${this.weekDay[date.getDay()]}, ${dd}.${mm}.${yyyy}`;
  }

  capitalizeString(text: string): string {
    return text.charAt(0).toUpperCase() + text.slice(1);
  }

  getDateIsoString(d: Date): string {
    const date = new Date(d);
    const dd = String(date.getDate()).padStart(2, '0');
    const mm = String(date.getMonth() + 1).padStart(2, '0'); // January is 0!
    const yyyy = date.getFullYear();

    return `${yyyy}-${mm}-${dd}`;
  }

  addDaysToDate(date: Date, days: number): Date {
    const newDate = new Date(date);
    newDate.setDate(newDate.getDate() + days);
    return newDate;
  }

  sortArticleAmountNumeric(articles: Article[], direction: string): Article[] {

    switch (direction) {
      case 'desc':
        // ------ Sort Descending ------
        articles.sort((a: Article, b: Article) => {
          if (a.amount > b.amount) {
            return -1;
          } else if (a.amount === b.amount) {
            return 0;
          }
          return 1;
        });
        break;
      case 'asc':
        // ------ Sort Ascending ------
        articles.sort((a: Article, b: Article) => {
          if (a.amount > b.amount) {
            return 1;
          } else if (a.amount === b.amount) {
            return 0;
          }
          return -1;
        });
        break;
      default:
        console.log('Wrong parameter, use: Article[], desc/asc');
        break;
    }
    return articles;
  }

  sortArticleNameLexigraphical(articles: Article[], direction: string): Article[] {

    switch (direction) {
      case 'desc':
        // ------ Sort Descending ------
        articles.sort((a: Article, b: Article) => {

          for (let i = 0; i < a.name.length; i++) {
            if (a.name.toLowerCase().charAt(i) < b.name.toLowerCase().charAt(i)) {
              return 1;
            } else if (a.name.toLowerCase().charAt(i) > b.name.toLowerCase().charAt(i)) {
              return -1;
            }
          }
          return 0;
        });
        break;
      case 'asc':
        // ------ Sort Ascending ------
        articles.sort((a: Article, b: Article) => {

          for (let i = 0; i < a.name.length; i++) {
            if (a.name.toLowerCase().charAt(i) > b.name.toLowerCase().charAt(i)) {
              return 1;
            } else if (a.name.toLowerCase().charAt(i) < b.name.toLowerCase().charAt(i)) {
              return -1;
            }
          }
          return 0;
        });
        break;
      default:
        console.log('Wrong parameter, use: Article[], desc/asc');
        break;
    }

    return articles;
  }

  sortArticleOwnerLexigraphical(articles: Article[], userMap: Map<string, User>, direction: string): Article[] {

    switch (direction) {
      case 'desc':
        // ------ Sort Descending ------
        articles.sort((a: Article, b: Article) => {

          const articleAownerName = userMap.get(a.userUuid).username;
          const articleBownerName = userMap.get(b.userUuid).username;

          for (let i = 0; i < articleAownerName.length; i++) {
            if (articleAownerName.toLowerCase().charAt(i) < articleBownerName.toLowerCase().charAt(i)) {
              return 1;
            } else if (articleAownerName.toLowerCase().charAt(i) > articleBownerName.toLowerCase().charAt(i)) {
              return -1;
            }
          }
          return 0;
        });
        break;
      case 'asc':
        // ------ Sort Ascending ------
        articles.sort((a: Article, b: Article) => {

          const articleAownerName = userMap.get(a.userUuid).username;
          const articleBownerName = userMap.get(b.userUuid).username;

          for (let i = 0; i < articleAownerName.length; i++) {
            if (articleAownerName.toLowerCase().charAt(i) > articleBownerName.toLowerCase().charAt(i)) {
              return 1;
            } else if (articleAownerName.toLowerCase().charAt(i) < articleBownerName.toLowerCase().charAt(i)) {
              return -1;
            }
          }
          return 0;
        });
        break;
      default:
        console.log('Wrong parameter, use: Article[], desc/asc');
        break;
    }

    return articles;
  }

  sortArticleCreateDate(articles: Article[], direction: string): Article[] {

    switch (direction) {
      case 'desc':
        // ------ Sort Descending ------
        articles.sort((a: Article, b: Article) => {

          if (a.createDate < b.createDate) {
            return 1;
          } else if (a.createDate > b.createDate) {
            return -1;
          }

          return 0;
        });
        break;
      case 'asc':
        // ------ Sort Ascending ------
        articles.sort((a: Article, b: Article) => {

          if (a.createDate < b.createDate) {
            return -1;
          } else if (a.createDate > b.createDate) {
            return 1;
          }

          return 0;
        });
        break;
    }

    return articles;
  }
}
