import { TestBed } from '@angular/core/testing';

import { HttpInterceptorShoppingListService } from './http-interceptor-shopping-list.service';

describe('HttpInterceptorShoppingListService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HttpInterceptorShoppingListService = TestBed.get(HttpInterceptorShoppingListService);
    expect(service).toBeTruthy();
  });
});
