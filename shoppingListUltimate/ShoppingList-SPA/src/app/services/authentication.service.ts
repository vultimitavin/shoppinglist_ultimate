import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {tap} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {UserService} from './user.service';
import {User} from '../class/User';


const BASE_URL = 'http://localhost:8080';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private http: HttpClient,
              private userService: UserService) {
  }

  public get currentUserName(): string {
    return sessionStorage.getItem('username');
  }

  public get currentUserAdmin(): boolean {
    return !!sessionStorage.getItem('isAdmin');
  }

  public get loggedIn(): boolean {
    return !!sessionStorage.getItem('access_token');
  }

  getAuthorizationHeader(): string {
    return `Baerer ${sessionStorage.getItem('access_token')}`;
  }

  login(username: string, password: string): Observable<any> {
    return this.http.post<{ token: string }>(
      `${BASE_URL}/authenticate`, {username, password})
      .pipe(
        tap(
          res => {
            sessionStorage.setItem('access_token', res.token);

            this.userService.getUserByUsername(username)
              .subscribe(
                (data: User) => {
                  sessionStorage.setItem('username', username);
                  sessionStorage.setItem('userUuid', data.uuid);
                  if (data.admin) {
                    sessionStorage.setItem('isAdmin', 'true');
                  }
                }, error => {
                  console.log('Couldnt fetch User by Username from Database: ' + error);
                }
              );
          }
        )
      );
  }

  logout() {
    sessionStorage.removeItem('access_token');
    sessionStorage.removeItem('username');
    if (sessionStorage.getItem('isAdmin')) {
      sessionStorage.removeItem('isAdmin');
    }
  }

  ping() {
    this.http
      .get(`${BASE_URL}/Artikel`)
      .subscribe(data => console.log(`Data: ${data}`), err => console.log(`Error: ${err}`));
  }


}
