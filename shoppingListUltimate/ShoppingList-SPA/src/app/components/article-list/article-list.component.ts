import {Component, OnInit} from '@angular/core';
import {ArticleService} from '../../services/article.service';
import {Article} from '../../class/Article';
import {User} from '../../class/User';
import {UserService} from '../../services/user.service';
import {UnitService} from '../../services/unit.service';
import {Unit} from '../../class/Unit';
import {UtilService} from '../../services/utility/util.service';
import {AuthenticationService} from '../../services/authentication.service';
import {Form, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-article-list',
  templateUrl: './article-list.component.html',
  styleUrls: ['./article-list.component.css']
})
export class ArticleListComponent implements OnInit {
  articles: Article[];
  articlesNew = new Array<Article>();
  articlesArchived = new Array<Article>();
  article: Article = new Article();
  userMap = new Map<string, User>();
  unitMap = new Map<string, Unit>();
  pArchived = 1;
  today = new Date();
  dates: Date[];
  dateToFilter: string;
  dateForm: FormGroup;

  constructor(private articleService: ArticleService,
              private userService: UserService,
              private unitService: UnitService,
              private utilService: UtilService,
              private formBuilder: FormBuilder,
              private authService: AuthenticationService) {
    // Listen to Data that comes from the article-create-dialog and append it to the articlesNew Array
    this.articleService.shareArticleCreateDialogData$.subscribe(
      (data: Article[]) => {
        data.forEach(article => {
          this.articlesNew.push(article);
        });
      }
    );

    // Creating a FormGroup with FormControls
    this.dateForm = this.formBuilder.group({
      floatLabel: 'auto',
      date: new FormControl('all')
    });
  }

  ngOnInit() {
    this.reloadData();
    // Get Values for the select field to filter the table
    this.dates = [
      this.today,
      this.utilService.addDaysToDate(this.today, 1),
      this.utilService.addDaysToDate(this.today, 2),
      this.utilService.addDaysToDate(this.today, 3),
      this.utilService.addDaysToDate(this.today, 4),
      this.utilService.addDaysToDate(this.today, 5)
    ];
    // Set the generic date to filter to today
    this.dateToFilter = 'all';
  }

  filterTable() {
    // get the form input
    const formInput = this.dateForm.controls.date.value;
    if (formInput !== 'all') {
      this.dateToFilter = this.utilService.getDateIsoString(new Date(formInput));
    } else {
      this.dateToFilter = formInput;
    }
    console.log(this.dateToFilter);
  }

  showAll() {
    this.reloadData();
  }

  reloadData() {
    this.articlesNew = [];
    this.articlesArchived = [];
    this.articleService.getArticleList().subscribe((data: Article[]) => {
      this.articles = data;
      this.articles.forEach(article => {
        if (!article.bought) {
          this.articlesNew.push(article);
        } else {
          this.articlesArchived.push(article);
        }
      });
    });

    this.getUserList();
    this.getUnitList();
  }

  getArticle(article: Article, event) {

    // Get selected Article for Data
    this.articleService.getArticle(article.uuid).subscribe((data: Article) => {
      this.article = data;
    });

    // Get articleDetailRow
    const articleDetailRow = event.target.parentElement.nextElementSibling;

    if (articleDetailRow.classList.contains('articleDetailRow--collapsed')) {
      articleDetailRow.classList.remove('articleDetailRow--collapsed');
      articleDetailRow.classList.add('articleDetailRow--show');
    } else {
      articleDetailRow.classList.remove('articleDetailRow--show');
      articleDetailRow.classList.add('articleDetailRow--collapsed');
    }
  }

  deleteArticle(article: Article) {
    this.articleService
      .deleteArticle(article.uuid)
      .subscribe(
        data => {
          this.articlesNew = this.articlesNew.filter(a => a.uuid !== article.uuid);
          this.articlesArchived = this.articlesArchived.filter(a => a.uuid !== article.uuid);
        },
        error => console.log(error));
  }

  setArticleBought(article: Article) {
    // Edit the Article to be bought
    article.bought = true;

    // Hand over the Article to be updated
    this.articleService
      .updateArticle(article.uuid, article)
      .subscribe(
        data => {
          // Remove Article from the ArticlesNew Array
          this.articlesNew = this.articlesNew.filter(a => a !== article);

          // Add Article to the ArticleArchived Array
          this.articlesArchived.push(article);
        }, err => console.log(`Error, couldnt update Article: ${err}`)
      );
  }

  getUserList() {
    this.userService.getUserList().subscribe(
      (data: User[]) => {
        data.forEach((user: User) => {
          this.userMap.set(user.uuid, user);
        });
      }
    );
  }

  getUnitList() {
    this.unitService.getUnitList().subscribe(
      (data: Unit[]) => {
        data.forEach((unit: Unit) => {
          this.unitMap.set(unit.uuid, unit);
        });
      }
    );
  }

  getArticleAmount(date: Date): number {
    const articleFromDate = this.articlesNew.filter(a => this.utilService.getDateIsoString(a.dateToBuy) ===
      this.utilService.getDateIsoString(date));
    return articleFromDate.length;
  }

}
