import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthenticationService} from '../../services/authentication.service';
import {ActivatedRoute, Router} from '@angular/router';
import {UtilService} from '../../services/utility/util.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  title = 'Log In!';
  returnUrl: string;

  constructor(private formBuilder: FormBuilder,
              private authenticationService: AuthenticationService,
              private utilService: UtilService,
              public router: Router,
              private route: ActivatedRoute) {

    // Creating a FormGroup with FormControls
    this.form = this.formBuilder.group({
      floatLabel: 'auto',
      username: new FormControl('', [Validators.required, Validators.maxLength(15)]),
      password: new FormControl('', [Validators.required, Validators.maxLength(15)])
    });
  }

  submit(form) {

/*    const logInSpinner = document.querySelector('.logInSpinner');
    logInSpinner.classList.replace('logInSpinner', 'logInSpinner--show');*/
    this.utilService.showLoadingScreen();

    this.authenticationService.login(form.get('username').value, form.get('password').value).subscribe(
      data => {

        // hide the loading spinner
        window.setTimeout(() => {
          /*logInSpinner.classList.replace('logInSpinner--show', 'logInSpinner');*/
          this.utilService.hideLoadingScreen();
          // Navigate to desired Site
          this.router.navigateByUrl(this.returnUrl);
          // Create Snackbar message
          this.utilService.createSnackBarItem(`Hi ${this.utilService.capitalizeString(form.get('username').value)},
         welcome back ;)`, 'Welcome');
        }, 1750);
      }, error => {
        window.setTimeout(() => {
          /*logInSpinner.classList.replace('logInSpinner--show', 'logInSpinner');*/
          this.utilService.hideLoadingScreen();
          this.form.reset();
          document.getElementById('errorMessage').style.display = 'unset';
        }, 1750);
      });
  }

  ngOnInit() {
    // get return url from route parameters or default to ''
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || '';
  }

}
