import {Component, OnInit, TemplateRef, ViewContainerRef} from '@angular/core';
import {AuthenticationService} from '../../services/authentication.service';
import {UtilService} from '../../services/utility/util.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(private authService: AuthenticationService,
              private viewContainerRef: ViewContainerRef,
              private utilService: UtilService) {
  }

  ngOnInit() {
  }

  logout() {
    this.utilService.createSnackBarItem(`Have a nice day ${this.utilService.capitalizeString(
      sessionStorage.getItem('username'))} :)`, 'Bye');
    this.authService.logout();
  }

  clearTemplates() {
    this.viewContainerRef.clear();
  }

  // Render the Template-Content and embed it into the view
  switchTemplate(content: TemplateRef<any>) {
    const portalOutlet = document.querySelector('#portalOutlet');
    this.viewContainerRef.clear();
    const embeddedView = this.viewContainerRef.createEmbeddedView(
      content);

    // DOM-Manipulation for HTML-Markup of template
    embeddedView.rootNodes.forEach(rootNode => portalOutlet.appendChild(rootNode));
  }

}
