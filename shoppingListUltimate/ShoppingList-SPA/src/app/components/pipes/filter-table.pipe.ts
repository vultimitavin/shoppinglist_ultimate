import {Pipe, PipeTransform} from '@angular/core';
import {Article} from '../../class/Article';
import {UtilService} from '../../services/utility/util.service';

@Pipe({
  name: 'filterTable'
})
export class FilterTablePipe implements PipeTransform {

  constructor(private utilService: UtilService) {
  }

  transform(articles: Article[], dateToFilter: string): any[] {
    if (!articles) {
      return [];
    }

    if (!dateToFilter || dateToFilter === 'all') {
      return articles;
    }

    // If date is today, also show all articles whose dateToBuy is prior today
    if (this.utilService.getDateIsoString(new Date()) === dateToFilter) {
      return articles.filter(article =>
        // Get the dateToBuy Attribute and set time to 00:00:00
        new Date(`${this.utilService.getDateIsoString(article.dateToBuy)}T00:00:00Z`) <=
        // Get the dateToFilter and set time to 00:00:00
        new Date(`${dateToFilter}T00:00:00Z`));
    }

    return articles.filter(article => dateToFilter === this.utilService.getDateIsoString(new Date(article.dateToBuy)));
  }

}
