import {Component, Input, OnInit, TemplateRef, ViewContainerRef} from '@angular/core';
import {Recipe} from '../../../class/Recipe';
import {ActivatedRoute} from '@angular/router';
import {RecipeService} from '../../../services/recipe.service';
import {UtilService} from '../../../services/utility/util.service';
import {Ingredient} from '../../../class/Ingredient';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {UnitService} from '../../../services/unit.service';
import {Unit} from '../../../class/Unit';
import {Location} from '@angular/common';

@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.css']
})
export class RecipeDetailComponent implements OnInit {
  recipe: Recipe;
  ingredientForm: FormGroup;
  unitList: Unit[];
  confirmMessage: string;

  constructor(private route: ActivatedRoute,
              private recipeService: RecipeService,
              private utilService: UtilService,
              private unitService: UnitService,
              private location: Location,
              private formBuilder: FormBuilder,
              private viewContainerRef: ViewContainerRef) {
    // Creating a FormGroup with FormControls
    this.ingredientForm = this.formBuilder.group({
      floatLabel: 'auto',
      name: new FormControl('', [Validators.required, Validators.maxLength(15)]),
      unit: new FormControl('', [Validators.required, Validators.maxLength(5)]),
      amount: new FormControl('', [Validators.required, Validators.maxLength(5)]),
    });
  }

  ngOnInit() {
    this.utilService.showLoadingScreen();

    this.unitService.getUnitList().subscribe(
      (data: Unit[]) => {
        this.unitList = data;
        this.ingredientForm.controls.unit.setValue(this.unitList[0].name);
      }, error => {
        console.log(`ERROR: ${error}`);
      }
    );

    window.setTimeout(() => {
      this.recipeService.getRecipeById(this.route.snapshot.params.uuid).subscribe(
        (data: Recipe) => {
          this.recipe = data;
          this.utilService.hideLoadingScreen();
        }, error => {
          this.utilService.hideLoadingScreen();
          console.log(`ERROR: ${error}`);
        }
      );
    }, 1500);
  }

  showRecipe() {
    console.log(this.recipe);
    console.log(document.querySelector('.ingredientDetails'));
  }

  addIngredientDto(formInput) {
    const newIngredient = new Ingredient();

    newIngredient.name = formInput.get('name').value;

    this.unitList.map(unit => {
      if (unit.name === formInput.get('unit').value) {
        newIngredient.unitDto = unit;
      }
    });

    newIngredient.amount = formInput.get('amount').value;

    this.recipe.ingredientDtos.push(newIngredient);
    this.ingredientForm.controls.name.reset();
    this.ingredientForm.controls.amount.reset();
  }

  resetForm() {
    this.ingredientForm.controls.name.reset();
    this.ingredientForm.controls.amount.reset();
    this.ingredientForm.controls.unit.setValue(this.unitList[0].name);
  }

  removeIngredientFromRecipe(ingredient: Ingredient) {
    this.recipe.ingredientDtos = this.recipe.ingredientDtos.filter(ing => (ing.name !== ingredient.name &&
      ing.amount !== ingredient.amount) || ing.unitDto.name !== ingredient.unitDto.name);
  }

  updateRecipe() {
    this.utilService.showLoadingScreen();
    this.recipeService.updateRecipe(this.recipe.uuid, this.recipe).subscribe(
      data => {
        window.setTimeout(() => {
          this.utilService.hideLoadingScreen();
          this.utilService.createSnackBarItem(`Saved Recipe ${this.recipe.name}`, 'Hurray');
        }, 1000);
        console.log(data);
      }, error => {
        window.setTimeout(() => {
          this.utilService.hideLoadingScreen();
          this.utilService.createSnackBarItem(`Something went wrong :(, try again later.`, 'Oops');
        }, 1000);
        console.log(`ERROR: ${error}`);
      }
    );
  }

  deleteRecipe() {
    this.utilService.showLoadingScreen();
    this.recipeService.deleteRecipe(this.recipe.uuid).subscribe(
      data => {
        window.setTimeout(() => {
          this.utilService.hideLoadingScreen();
          this.utilService.createSnackBarItem(`Deleted Recipe: ${this.recipe.name}`, 'Delete');
          this.goBack();
        }, 1250);
        console.log(data);
      }, error => {
        window.setTimeout(() => {
          this.utilService.hideLoadingScreen();
          this.utilService.createSnackBarItem(`Something went wrong :(, try again later.`, 'Oops');
        }, 1000);
        console.log(`ERROR: ${error}`);
      }
    );
  }

  goBack() {
    this.location.back();
  }

  clearTemplates() {
    this.viewContainerRef.clear();
  }

  // Render the Template-Content and embed it into the view
  switchTemplate(content: TemplateRef<any>) {
    this.confirmMessage = `Do you really want to delete the recipe: ${this.recipe.name}?`;
    const portalOutlet = document.querySelector('#portalOutlet');
    this.viewContainerRef.clear();
    const embeddedView = this.viewContainerRef.createEmbeddedView(
      content);

    // DOM-Manipulation for HTML-Markup of template
    embeddedView.rootNodes.forEach(rootNode => portalOutlet.appendChild(rootNode));
  }
}
