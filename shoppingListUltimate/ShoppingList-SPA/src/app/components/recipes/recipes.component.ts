import { Component, OnInit } from '@angular/core';
import {Recipe} from '../../class/Recipe';
import {UtilService} from '../../services/utility/util.service';
import {RecipeService} from '../../services/recipe.service';

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.css']
})
export class RecipesComponent implements OnInit {
  recipesMap = new Map<string, Recipe>();
  allRecipes: Recipe[];
  filteredRecipes: Recipe[];
  pArchived = 1;

  constructor(private utilService: UtilService,
              private recipeService: RecipeService) {
  }

  ngOnInit() {
    this.recipeService.getRecipeList().subscribe(
      (data: Recipe[]) => {
        this.allRecipes = data;
        this.filteredRecipes = data;
        this.allRecipes.forEach(recipe => {
          this.recipesMap.set(recipe.uuid, recipe);
        });
      }
    );
  }

  filterRecipes(event) {
    const filterValue = event.target.value;
    this.filteredRecipes = this.allRecipes.filter(recipe => recipe.name.toLowerCase().includes(filterValue.toLowerCase()));
  }

}
