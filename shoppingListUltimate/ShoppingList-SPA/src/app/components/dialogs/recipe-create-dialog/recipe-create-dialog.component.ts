import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Recipe} from '../../../class/Recipe';
import {RecipeService} from '../../../services/recipe.service';
import {UtilService} from '../../../services/utility/util.service';
import {User} from '../../../class/User';
import {UserService} from '../../../services/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-recipe-create-dialog',
  templateUrl: './recipe-create-dialog.component.html',
  styleUrls: ['./recipe-create-dialog.component.css']
})
export class RecipeCreateDialogComponent implements OnInit {
  @Output() closeRecipeCreateDialog = new EventEmitter();
  form: FormGroup;
  recipesByUser = new Array<Recipe>();
  currentUser: User;

  constructor(private formBuilder: FormBuilder,
              private recipeService: RecipeService,
              private utilService: UtilService,
              private userService: UserService,
              private router: Router) {
    // Creating a FormGroup with FormControls
    this.form = this.formBuilder.group({
      floatLabel: 'auto',
      name: new FormControl('', [Validators.required, Validators.maxLength(15)]),
    });
  }

  ngOnInit() {
    // Dim the page
    document.getElementById('dimmer').style.display = 'block';
    // prevent scrolling on main page
    document.body.style.overflow = 'hidden';
    this.recipeService.getRecipeList().subscribe(
      (data: Recipe[]) => {
        data.forEach((recipe: Recipe) => {
          if (recipe.userDto.username === sessionStorage.getItem('username')) {
            this.recipesByUser.push(recipe);
          }
        });
      }, error => {
        console.log(`ERROR: ${error}`);
        this.utilService.createSnackBarItem('Something went wrong :(. Try again later.', 'Oops');
      }
    );
    this.userService.getUserByUsername(sessionStorage.getItem('username')).subscribe(
      (data: User) => {
        this.currentUser = data;
      }
    );
  }

  submit(formInput) {
    const recipe = new Recipe();
    recipe.name = formInput.get('name').value;
    recipe.userDto = this.currentUser;

    this.utilService.showLoadingScreen();

    window.setTimeout(() => {
      this.recipeService.createRecipe(recipe).subscribe(
        res => {
          this.utilService.createSnackBarItem(`Recipe successfully created: ${recipe.name}`, 'Hurray');
          this.utilService.hideLoadingScreen();
          this.form.reset();
          this.closeModal();
        }, error => {
          this.utilService.hideLoadingScreen();
          console.log(`ERROR: ${error}`);
          this.form.reset();
          this.closeModal();
        });
    }, 1750);

  }

  closeModal() {
    document.body.style.overflow = 'auto';
    this.closeRecipeCreateDialog.next();
  }

}
