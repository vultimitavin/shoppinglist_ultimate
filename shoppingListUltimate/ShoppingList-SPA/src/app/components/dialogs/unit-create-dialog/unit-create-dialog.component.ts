import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Unit} from '../../../class/Unit';
import {UnitService} from '../../../services/unit.service';
import {ArticleListComponent} from '../../article-list/article-list.component';

@Component({
  selector: 'app-unit-create-dialog',
  templateUrl: './unit-create-dialog.component.html',
  styleUrls: ['./unit-create-dialog.component.css']
})
export class UnitCreateDialogComponent implements OnInit {
  @Output() closeUnitCreateDialog = new EventEmitter();
  form: FormGroup;
  unitList: Unit[];
  unitNewList = new Array<Unit>();

  constructor(private formBuilder: FormBuilder,
              private unitService: UnitService) {
    // Creating a FormGroup with FormControls
    this.form = this.formBuilder.group({
      floatLabel: 'auto',
      unitname: new FormControl('', [Validators.required, Validators.maxLength(15)]),
      abbreviation: new FormControl('', [Validators.required, Validators.maxLength(5)]),
    });
  }

  ngOnInit() {
    // Dim the page
    document.getElementById('dimmer').style.display = 'block';
    // prevent scrolling on main page
    document.body.style.overflow = 'hidden';
    this.reloadData();
  }

  submit(form) {

    const unit: Unit = {
      uuid: null,
      name: form.get('unitname').value,
      abbreviation: form.get('abbreviation').value,
      modifyDate: null,
      createDate: null
    };

    this.addUnitToArray(unit);

    this.form.reset();
  }

  reloadData() {
    this.unitService.getUnitList().subscribe(
      (data: Unit[]) => {
        this.unitList = data;
      }
    );
  }

  // TODO: Update Tabledata in view (Observable)
  createUnits() {
    this.unitService.createUnitList(this.unitNewList)
      .subscribe((data: Unit[]) => {
        console.log('Returned unitlist: ' + data);
      }, err => {
        console.log('Error posting Unitlist to Server: ' + err);
      });
    this.unitNewList = [];
  }

  addUnitToArray(unit: Unit) {
    if (this.unitNewList.find(u => (u.name === unit.name) || (u.abbreviation === unit.abbreviation)) ||
      this.unitList.find(u => (u.name === unit.name) || (u.abbreviation === unit.abbreviation))) {
      alert('Unit with name already present' + unit.name);
    } else {
      this.unitNewList.push(unit);
    }
  }

  removeUnitFromArray(unitToRemove: Unit) {
    const tmpUnitList = new Array<Unit>();
    this.unitList.map(unit => {
      if (unit !== unitToRemove) {
        tmpUnitList.push(unit);
      }
    });
    this.unitNewList = tmpUnitList;
  }

  save() {
    this.createUnits();
  }

  onDone() {
    this.save();
    this.closeModal();
  }

  closeModal() {
    document.body.style.overflow = 'auto';
    this.closeUnitCreateDialog.next();
  }

}
