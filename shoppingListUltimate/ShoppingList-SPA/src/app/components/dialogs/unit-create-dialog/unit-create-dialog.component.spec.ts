import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnitCreateDialogComponent } from './unit-create-dialog.component';

describe('UnitCreateDialogComponent', () => {
  let component: UnitCreateDialogComponent;
  let fixture: ComponentFixture<UnitCreateDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnitCreateDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnitCreateDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
