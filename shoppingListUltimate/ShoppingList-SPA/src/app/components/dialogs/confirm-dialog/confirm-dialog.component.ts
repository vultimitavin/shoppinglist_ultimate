import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {RecipeService} from '../../../services/recipe.service';
import {UtilService} from '../../../services/utility/util.service';
import {UserService} from '../../../services/user.service';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.css']
})
export class ConfirmDialogComponent implements OnInit {
  @Input('confirmMessage') confirmMessage: string;
  @Output() closeConfirmDialog = new EventEmitter();
  @Output() confirmAction = new EventEmitter();

  constructor(private recipeService: RecipeService,
              private utilService: UtilService,
              private userService: UserService) {
  }

  ngOnInit() {
    // Dim the page
    document.getElementById('dimmer').style.display = 'block';
    // prevent scrolling on main page
    document.body.style.overflow = 'hidden';
  }

  closeModal() {
    document.body.style.overflow = 'auto';
    this.closeConfirmDialog.next();
  }

  confirm() {
    document.body.style.overflow = 'auto';
    this.confirmAction.next();
  }

}
