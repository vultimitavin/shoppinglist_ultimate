import {Component, EventEmitter, OnInit, Output, TemplateRef, ViewContainerRef} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ArticleService} from '../../../services/article.service';
import {UserService} from '../../../services/user.service';
import {UnitService} from '../../../services/unit.service';
import {Article} from '../../../class/Article';
import {Unit} from '../../../class/Unit';
import {UtilService} from '../../../services/utility/util.service';
import {AuthenticationService} from '../../../services/authentication.service';

@Component({
  selector: 'app-article-create-dialog',
  templateUrl: './article-create-dialog.component.html',
  styleUrls: ['./article-create-dialog.component.css']
})
export class ArticleCreateDialogComponent implements OnInit {
  @Output() closeArticleCreateDialog = new EventEmitter();
  form: FormGroup;
  articles = new Array<Article>();
  unitList: Unit[];
  unitMap = new Map<string, Unit>();
  articlesAdded: string;

  constructor(private formBuilder: FormBuilder,
              private articleService: ArticleService,
              private authService: AuthenticationService,
              private userService: UserService,
              private unitService: UnitService,
              private utilService: UtilService) {
    // Creating a FormGroup with FormControls
    this.form = this.formBuilder.group({
      floatLabel: 'auto',
      articlename: new FormControl('', [Validators.required, Validators.maxLength(15)]),
      amount: new FormControl('', [Validators.required, Validators.maxLength(5)]),
      date: new FormControl(this.articleService.getCurrentDateIsoString(),
        [Validators.required, Validators.pattern('\d{2}\.\d{2}\.\d{4}')]),
      unit: new FormControl('', [Validators.required, Validators.maxLength(5)]),
    });
  }

  save() {
    this.createArticles();
  }

  closeModal() {
    document.body.style.overflow = 'auto';
    this.closeArticleCreateDialog.next();
  }

  onDone() {

    if (this.articles.length === 0) {
      // Show error message if articles are empty = user hasnt put any articles into basket
      this.articlesAdded = `${this.utilService.capitalizeString(this.authService.currentUserName)},
      you havent added an article to the basket yet.`;
      document.getElementById('articleBasket').style.border = '1px solid red';
    } else {
      this.save();
      this.closeModal();
    }
  }

  submit(form) {
    const dateToBuy = form.get('date').value;
    const unitName = form.get('unit').value;

    // Get unitUUID
    let currentUnitUuid = '';
    this.unitList.map((unit: Unit) => {
      if (unit.abbreviation === this.form.get('unit').value) {
        currentUnitUuid = unit.uuid;
      }
    });

    const article: Article = {
      uuid: null,
      name: form.get('articlename').value,
      amount: form.get('amount').value,
      unitUuid: currentUnitUuid,
      userUuid: sessionStorage.getItem('userUuid'),
      bought: false,
      modifyDate: null,
      createDate: null,
      dateToBuy: new Date(dateToBuy)
    };

    this.addArticleToArray(article);

    this.form.reset();
    this.form.controls.unit.setValue(unitName, {onlySelf: true});
    this.form.controls.date.setValue(dateToBuy, {onlySelf: true});
  }

  ngOnInit() {
    // Dim the page
    document.getElementById('dimmer').style.display = 'block';
    document.body.style.overflow = 'hidden';
    this.reloadData();
  }

  reloadData() {
    this.unitService.getUnitList().subscribe(
      (data: Unit[]) => {
        this.unitList = data;
        this.unitList.forEach((unit: Unit) => {
          this.unitMap.set(unit.uuid, unit);
        });

        // Change the default form value
        this.form.controls.unit.setValue(this.unitList[0].abbreviation, {onlySelf: true});
      }
    );
  }

  createArticles() {
    this.articleService.createArticleList(this.articles)
      .subscribe((data: Article[]) => {
        // Share the newly created articles with the article-table
        this.articleService.setArticleTableData(data);
      }, error => console.log(error));
    this.articles = [];
  }

  addArticleToArray(article: Article) {
    let isDuplicate = false;
    // Check whether the added Article already exists in the temporary article-Array
    this.articles.map(a => {
      if (a.name === article.name &&
        this.utilService.getDateIsoString(a.dateToBuy) === this.utilService.getDateIsoString(article.dateToBuy)) {
        isDuplicate = true;
      }
    });

    // If it already exists, create a Snackbar-Item. Else add it to the array
    if (isDuplicate) {
      // Create snackbaritem and message
      this.utilService.createSnackBarItem(`${this.utilService.capitalizeString(this.authService.currentUserName)},
       you already added ${article.name} for ${this.utilService.prettyPrintDate(article.dateToBuy)}`, 'Oops');
    } else {
      // reset the usermessage
      this.articlesAdded = null;
      this.articles.push(article);
    }

  }

  removeArticleFromArray(articleToRemove: Article) {
    const newArticleList = new Array<Article>();
    this.articles.map(article => {
      if (article !== articleToRemove) {
        newArticleList.push(article);
      }
    });
    this.articles = newArticleList;
  }
}
