import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticleCreateDialogComponent } from './article-create-dialog.component';

describe('ArticleCreateDialogComponent', () => {
  let component: ArticleCreateDialogComponent;
  let fixture: ComponentFixture<ArticleCreateDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArticleCreateDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticleCreateDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
