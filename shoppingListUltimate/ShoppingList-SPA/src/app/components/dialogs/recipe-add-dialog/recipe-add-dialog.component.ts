import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Recipe} from '../../../class/Recipe';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {RecipeService} from '../../../services/recipe.service';
import {UtilService} from '../../../services/utility/util.service';
import {UserService} from '../../../services/user.service';
import {Router} from '@angular/router';
import {User} from '../../../class/User';
import {Unit} from '../../../class/Unit';
import {Article} from '../../../class/Article';
import {ArticleService} from '../../../services/article.service';

@Component({
  selector: 'app-recipe-add-dialog',
  templateUrl: './recipe-add-dialog.component.html',
  styleUrls: ['./recipe-add-dialog.component.css']
})
export class RecipeAddDialogComponent implements OnInit {
  @Output() closeRecipeAddDialog = new EventEmitter();
  form: FormGroup;
  allRecipes: Recipe[];
  filteredRecipes: Recipe[];
  selectedRecipe: Recipe;
  currentUser: User;
  pArchived = 1;

  constructor(private formBuilder: FormBuilder,
              private recipeService: RecipeService,
              private utilService: UtilService,
              private articleService: ArticleService) {
    // Creating a FormGroup with FormControls
    this.form = this.formBuilder.group({
      floatLabel: 'auto',
      date: new FormControl(this.utilService.getDateIsoString(new Date()), [Validators.required, Validators.maxLength(15)]),
    });
  }

  submit(formInput) {
    const articles = new Array<Article>();
    const dateToBuy = formInput.get('date').value;

    this.selectedRecipe.ingredientDtos.forEach(ingredient => {
      const article: Article = {
        uuid: null,
        name: ingredient.name,
        amount: ingredient.amount,
        unitUuid: ingredient.unitDto.uuid,
        userUuid: sessionStorage.getItem('userUuid'),
        bought: false,
        modifyDate: null,
        createDate: null,
        dateToBuy: new Date(dateToBuy)
      };
      articles.push(article);
    });

    this.articleService.createArticleList(articles)
      .subscribe((data: Article[]) => {
        // Share the newly created articles with the article-table
        this.articleService.setArticleTableData(data);
      }, error => console.log(error));

    this.closeModal();
  }

  ngOnInit() {
    // Dim the page
    document.getElementById('dimmer').style.display = 'block';
    // prevent scrolling on main page
    document.body.style.overflow = 'hidden';
    this.recipeService.getRecipeList().subscribe(
      (data: Recipe[]) => {
        this.filteredRecipes = data;
        this.allRecipes = data;
        /*        data.forEach((recipe: Recipe) => {
                  if (recipe.userDto.username === sessionStorage.getItem('username')) {
                    this.recipesByUser.push(recipe);
                  }
                });*/
      }, error => {
        console.log(`ERROR: ${error}`);
        this.utilService.createSnackBarItem('Something went wrong :(. Try again later.', 'Oops');
      }
    );
    /*    this.userService.getUserByUsername(sessionStorage.getItem('username')).subscribe(
          (data: User) => {
            this.currentUser = data;
          }
        );*/
  }

  closeModal() {
    document.body.style.overflow = 'auto';
    this.closeRecipeAddDialog.next();
  }

  selectRecipe(recipe, event) {
    const row = event.target.parentElement;

    // If a recipe was already selected, remove the styling from the selected row
    Array.from(document.getElementsByClassName('recipeAddSelectedRow')).forEach(el => {
      console.log(el);
      el.classList.remove('recipeAddSelectedRow');
    });

    // Apply styling to the selected row
    row.classList.add('recipeAddSelectedRow');

    this.selectedRecipe = recipe;
    console.log(this.selectedRecipe);

    // enable the form submit button
    const submitButton = document.getElementById('submitButton');
    if (submitButton.hasAttribute('disabled')) {
      submitButton.removeAttribute('disabled');
    }
  }

  filterRecipes(event) {
    const filterValue = event.target.value;
    this.filteredRecipes = this.allRecipes.filter(recipe => recipe.name.toLowerCase().includes(filterValue.toLowerCase()));
  }

}
