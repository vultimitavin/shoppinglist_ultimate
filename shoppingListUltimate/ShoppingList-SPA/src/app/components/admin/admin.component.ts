import {Component, OnInit} from '@angular/core';
import {UserService} from '../../services/user.service';
import {User} from '../../class/User';
import {Unit} from '../../class/Unit';
import {UnitService} from '../../services/unit.service';

@Component({
  selector: 'app-home',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  userActiveList = new Array<User>();
  userAdminList = new Array<User>();
  unitList: Unit[];
  showDialog: boolean;

  constructor(private userService: UserService,
              private unitService: UnitService) {
  }

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.userActiveList = [];
    this.userAdminList = [];
    this.userService.getUserList().subscribe(
      (data: User[]) => {
        data.forEach((user: User) => {
          if (user.active) {
            this.userActiveList.push(user);
          }
          if (user.admin) {
            this.userAdminList.push(user);
          }
        });
      }, error => {
        console.log(`Couldnt fetch data from Server! Detailed Error Message: ${error}`);
      }
    );

    this.unitService.getUnitList().subscribe(
      (data: Unit[]) => {
        this.unitList = data;
        console.log(this.unitList);
      }
    );
  }

  getUser(id: string) {

  }

}
