import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticleDetailsUpdateComponent } from './article-details-update.component';

describe('ArticleDetailsUpdateComponent', () => {
  let component: ArticleDetailsUpdateComponent;
  let fixture: ComponentFixture<ArticleDetailsUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArticleDetailsUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticleDetailsUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
