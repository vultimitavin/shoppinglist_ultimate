import {Component, Input, OnInit} from '@angular/core';
import {Article} from '../../class/Article';
import {ArticleService} from '../../services/article.service';
import {ArticleListComponent} from '../article-list/article-list.component';

@Component({
  selector: 'app-article-details-update',
  templateUrl: './article-details-update.component.html',
  styleUrls: ['./article-details-update.component.css']
})
export class ArticleDetailsUpdateComponent implements OnInit {
  @Input('article') article: Article;
  id: number;

  constructor(private articleService: ArticleService,
              private articleList: ArticleListComponent) {
  }

  updateArticle() {
    this.articleService
      .updateArticle(this.article.uuid, this.article)
      .subscribe(
        data => {
          console.log(data)
          this.articleList.reloadData();
        }, error => console.log(error)
      );
  }

  ngOnInit() {
  }

  onSubmit() {
    this.updateArticle();
  }

}
