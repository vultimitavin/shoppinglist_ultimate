import {Component, OnInit, TemplateRef, ViewContainerRef} from '@angular/core';
import {UtilService} from '../../services/utility/util.service';
import {Recipe} from '../../class/Recipe';
import {RecipeService} from '../../services/recipe.service';
import {Form, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../../services/user.service';
import {User} from '../../class/User';
import {Ingredient} from '../../class/Ingredient';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {
  recipeForm: FormGroup;
  currentUser: User;

  constructor(private utilService: UtilService,
              private userService: UserService,
              private recipeService: RecipeService,
              private formBuilder: FormBuilder) {
    this.recipeForm = this.formBuilder.group({
      floatLabel: 'auto',
      recipename: new FormControl('', [Validators.required, Validators.maxLength(15)]),
      ingredient: new FormControl('', [Validators.required, Validators.maxLength(15)])
    });
  }

  ngOnInit() {
    this.userService.getUserByUsername(sessionStorage.getItem('username')).subscribe(
      (data: User) => {
        this.currentUser = data;
      }
    );
  }

  submit(formInput) {
    const recipe = new Recipe();
    const ingredient = new Ingredient();

    ingredient.name = formInput.get('ingredient').value;

    recipe.name = formInput.get('recipename').value;
    recipe.userDto = this.currentUser;

    this.recipeService.createRecipe(recipe).subscribe(
      res => console.log(`Data: ${res}`), error => console.log(`ERROR: ${error}`)
    );
    this.recipeForm.reset();
  }

}
