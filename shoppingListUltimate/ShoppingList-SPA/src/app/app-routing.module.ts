import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ArticleListComponent} from './components/article-list/article-list.component';
import {ConfigComponent} from './config/config.component';
import {AdminComponent} from './components/admin/admin.component';
import {LoginComponent} from './components/login/login.component';
import {TestCommand} from '@angular/cli/commands/test-impl';
import {TestComponent} from './components/test/test.component';
import {AuthGuardService} from './services/auth-guard.service';
import {RecipesComponent} from './components/recipes/recipes.component';
import {RecipeDetailComponent} from './components/recipes/recipe-detail/recipe-detail.component';

const routes: Routes = [
  {path: '', redirectTo: 'articles', pathMatch: 'full'},
  {path: 'admin', component: AdminComponent, canActivate: [AuthGuardService]},
  {path: 'login', component: LoginComponent},
  {path: 'articles', component: ArticleListComponent, canActivate: [AuthGuardService]},
  {path: 'recipes', component: RecipesComponent, canActivate: [AuthGuardService]},
  {path: 'recipe/:uuid', component: RecipeDetailComponent, canActivate: [AuthGuardService]},
  {path: 'config', component: ConfigComponent, canActivate: [AuthGuardService]},
  {path: 'logout', component: LoginComponent, canActivate: [AuthGuardService]},
  {path: 'test', component: TestComponent},
  {path: '**', redirectTo: '', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
