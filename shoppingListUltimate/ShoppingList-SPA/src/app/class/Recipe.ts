import {User} from './User';
import {Ingredient} from './Ingredient';

export class Recipe {
  uuid: string;
  name: string;
  userDto: User;
  ingredientDtos: Ingredient[];
  modifyDate: Date;
  createDate: Date;
}
