export class User {
  uuid: string;
  username: string;
  admin: boolean;
  active: boolean;
  createDate: Date;
  modifyDate: Date;
}
