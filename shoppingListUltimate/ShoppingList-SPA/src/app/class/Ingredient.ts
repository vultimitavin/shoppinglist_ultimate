import {Unit} from './Unit';

export class Ingredient {
  id: string;
  name: string;
  unitDto: Unit;
  amount: number;
  modifyDate: Date;
  createDate: Date;
}
