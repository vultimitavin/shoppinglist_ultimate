export class Article {
  uuid: string;
  name: string;
  userUuid: string;
  unitUuid: string;
  amount: number;
  bought: boolean;
  modifyDate: Date;
  createDate: Date;
  dateToBuy: Date;
}
