export class Unit {
  uuid: string;
  name: string;
  abbreviation: string;
  modifyDate: Date;
  createDate: Date;
}
