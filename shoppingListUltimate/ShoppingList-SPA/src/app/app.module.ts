import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {ArticleListComponent} from './components/article-list/article-list.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {ConfigComponent} from './config/config.component';
import {BsDropdownModule} from 'ngx-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ArticleDetailsUpdateComponent} from './components/article-details-update/article-details-update.component';
import {AdminComponent} from './components/admin/admin.component';
import {LoginComponent} from './components/login/login.component';
import {JwtModule} from '@auth0/angular-jwt';
import {HttpInterceptorShoppingListService} from './services/http-interceptor-shopping-list.service';
import { LogoutComponent } from './components/logout/logout.component';
import { TestComponent } from './components/test/test.component';
import { ArticleCreateDialogComponent } from './components/dialogs/article-create-dialog/article-create-dialog.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import {AuthGuardService} from './services/auth-guard.service';
import {AuthenticationService} from './services/authentication.service';
import { UnitCreateDialogComponent } from './components/dialogs/unit-create-dialog/unit-create-dialog.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { FilterTablePipe } from './components/pipes/filter-table.pipe';
import { RecipesComponent } from './components/recipes/recipes.component';
import { RecipeDetailComponent } from './components/recipes/recipe-detail/recipe-detail.component';
import { RecipeCreateDialogComponent } from './components/dialogs/recipe-create-dialog/recipe-create-dialog.component';
import { ConfirmDialogComponent } from './components/dialogs/confirm-dialog/confirm-dialog.component';
import { RecipeAddDialogComponent } from './components/dialogs/recipe-add-dialog/recipe-add-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    ArticleListComponent,
    ConfigComponent,
    ArticleDetailsUpdateComponent,
    AdminComponent,
    LoginComponent,
    LogoutComponent,
    TestComponent,
    ArticleCreateDialogComponent,
    NavbarComponent,
    UnitCreateDialogComponent,
    FilterTablePipe,
    RecipesComponent,
    RecipeDetailComponent,
    RecipeCreateDialogComponent,
    ConfirmDialogComponent,
    RecipeAddDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    BsDropdownModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: () => {
          return sessionStorage.getItem('access_token');
        },
        headerName: 'Authorization',
        whitelistedDomains: ['http://localhost:8080']
      }
    })
  ],
  providers: [AuthGuardService, AuthenticationService, ArticleListComponent,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorShoppingListService,
      multi: true
    }],
  bootstrap: [AppComponent]
})
export class AppModule {
}
