import {Component} from '@angular/core';
import {AuthenticationService} from './services/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'shopping list';
  clientHeight: number;

  constructor() {
    this.clientHeight = window.innerHeight;
  }

  loggedIn(): string {
    return sessionStorage.getItem('username') !== null ? sessionStorage.getItem('username') : ' -';
  }

  closeSnackBar() {
    // Get the snackbar DIV
    const snackBar = document.getElementById('snackbar');

    snackBar.classList.remove('show');
  }
}
